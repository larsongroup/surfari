# coding: utf-8
import signac
import resources as res
import os
from setup_li import setup_li
from smart_setup import smart_setup
from glob import glob
import time
import datetime

project = signac.contrib.get_project()

def test_setup_li_DM_hexadecane_water_c16e8():
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8'}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_DM_hexadecane_water_c16e8_smart_setup():
    sps = [{'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'surfactant':'c16e8','N':N} for N in (5, 10, 15)]
    smart_setup(sps,verbosity=1)
    N_time = {}
    for sp in sps:
        with project.open_job(sp) as job:
            N_time[sp['N']] = datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y")
    assert(N_time[15] < N_time[10])
    assert(N_time[10] < N_time[5])

    # test that jobs aren't re-done after they've been done
    smart_setup(sps,verbosity=1)
    for sp in sps:
        with project.open_job(sp) as job:
            assert(N_time[sp['N']] == datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y"))

def test_setup_li_DM_hexadecane_water_c16e8_smart_setup2():
    sp1 = {'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'surfactant':'c16e8','N':25}

    smart_setup(sp1,verbosity=1)
    with project.open_job(sp1) as job:
        sp1_time = datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y")

    sps = [{'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'surfactant':'c16e8_alt','N':N} for N in (5, 10, 25)]
    smart_setup(sps,verbosity=1)
    N_time = {}
    for sp in sps:
        with project.open_job(sp) as job:
            assert(os.path.isfile('li_dep.pdb'))
            assert(os.path.isfile('li_pre.pdb'))
            if sp['N'] == 25:
                assert(not os.path.isfile('li_dep.tpr'))
            else:
                assert(os.path.isfile('li_dep.tpr'))
            N_time[sp['N']] = datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y")
    assert(N_time[25] < N_time[10])
    assert(N_time[10] < N_time[5])

    # test that jobs aren't re-done after they've been done
    smart_setup(sps,verbosity=1)
    for sp in sps:
        with project.open_job(sp) as job:
            assert(N_time[sp['N']] == datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y"))

    smart_setup(sp1,verbosity=1)
    with project.open_job(sp1) as job:
        assert(sp1_time == datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y"))


def test_setup_li_DM_hexadecane_water_c16e8_solute_hexadecane1():
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[None,[['hexadecane'],[1]]]}
    with project.open_job(sp) as job:
        try:
            setup_li(job,verbosity=1)
            raise Exception('Should have failed b/c hexadecane solute in phase B is same as solventA')
        except AssertionError:
            pass

def test_setup_li_DM_hexadecane_water_c16e8_solute_hexadecane2():
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[[['hexadecane'],[1]],None]}
    with project.open_job(sp) as job:
        try:
            setup_li(job,verbosity=1)
            raise Exception('Should have failed b/c hexadecane solute in phase B is same as solventA')
        except AssertionError:
            pass

def test_setup_li_DM_hexadecane_water_c16e8_solute_dodecane1():
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[[['dodecane'],[1]],None]}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)

def test_setup_li_DM_hexadecane_water_c16e8_solute_dodecane12_octane8():
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[[['dodecane','octane'],[12,8]],None]}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_DM_hexadecane_water_c16e8_solute_dodecane8_octane12():
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[None,[['dodecane','octane'],[8,12]]]}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_dodecane_water_SPC_c12e8_solute_dodecane():
    sp ={'type':'li','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'solutes':[None,[['dodecane'],[1]]],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c12e8'}
    with project.open_job(sp) as job:
        try:
            setup_li(job,verbosity=1)
            raise Exception('Should have failed b/c hexadecane solute in phase B is same as solventA')
        except AssertionError:
            pass

def test_setup_li_dodecane_water_SPC_c12e8_solute_1NACL():
    sp ={'type':'li','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'solutes':[None,[['NA','CL'],[1,1]]],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c12e8'}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_dodecane_water_SPC_c12e8_solute_100NACL():
    sp ={'type':'li','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'solutes':[None,[['NA','CL'],[100,100]]],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c12e8'}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_dodecane_water_SPC_c12e8_solute_100NACL():
    sp ={'type':'li','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'solutes':[[['NA','CL'],[100,100]],None],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c12e8'}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_DM_hexadecane_water_c16e8_solutes_None():
    project = signac.contrib.get_project()
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[None,None]}
    with project.open_job(sp) as job:
        assert(not setup_li(job))
        assert(not os.path.isfile('signac_job_document.json'))
        assert(glob('*') == ['signac_statepoint.json'] or not glob('*'))

def test_setup_li_DM_hexadecane_water_c16e8_solutes_None2():
    project = signac.contrib.get_project()
    sp ={'type':'li','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':None}
    with project.open_job(sp) as job:
        assert(not setup_li(job))
        assert(not os.path.isfile('signac_job_document.json'))
        assert(glob('*') == ['signac_statepoint.json'] or not glob('*'))

def test_setup_li_dodecane_water_SPC_c12e8():
    sp ={'type':'li','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c12e8'}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_dodecane_water_SPC_SDS():
    sp ={'type':'li','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'SDS'}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_li_water_SPC_air_SDS():
    sp ={'type':'li','xy_len':6,'solvents':['water_SPC',None],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'SDS'}
    with project.open_job(sp) as job:
        setup_li(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')
