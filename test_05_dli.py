# coding: utf-8
import signac
import resources as res
import os
from glob import glob
from setup_dli import setup_dli
from smart_setup import smart_setup
import time
import datetime


project = signac.contrib.get_project()

def test_setup_dli_DM_hexadecane_water_c16e8_one_molecule():
    sp ={'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':1,'surfactant':'c16e8'}
    with project.open_job(sp) as job:
        setup_dli(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')


def test_setup_dli_DM_hexadecane_water_c16e8():
    sp ={'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8'}
    with project.open_job(sp) as job:
        setup_dli(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_dli_DM_hexadecane_water_c16e8_smart_setup():
    sps = [{'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'surfactant':'c16e8','N':N} for N in (5, 10, 15)]
    smart_setup(sps,verbosity=1)
    N_time = {}
    for sp in sps:
        with project.open_job(sp) as job:
            N_time[sp['N']] = datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y")
    assert(N_time[15] < N_time[10])
    assert(N_time[10] < N_time[5])

    # test that jobs aren't re-done after they've been done
    smart_setup(sps,verbosity=1)
    for sp in sps:
        with project.open_job(sp) as job:
            assert(N_time[sp['N']] == datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y"))

def test_setup_dli_DM_hexadecane_water_c16e8_smart_setup2():
    sp1 = {'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'surfactant':'c16e8','N':25}

    smart_setup(sp1,verbosity=1)
    with project.open_job(sp1) as job:
        sp1_time = datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y")

    sps = [{'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'surfactant':'c16e8_alt','N':N} for N in (5, 10, 25)]
    smart_setup(sps,verbosity=1)
    N_time = {}
    for sp in sps:
        with project.open_job(sp) as job:
            assert(os.path.isfile('li_dep.pdb'))
            assert(os.path.isfile('li_pre.pdb'))
            if sp['N'] == 25:
                assert(not os.path.isfile('li_dep.tpr'))
            else:
                assert(os.path.isfile('li_dep.tpr'))
            N_time[sp['N']] = datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y")
    assert(N_time[25] < N_time[10])
    assert(N_time[10] < N_time[5])

    # test that jobs aren't re-done after they've been done
    smart_setup(sps,verbosity=1)
    for sp in sps:
        with project.open_job(sp) as job:
            assert(N_time[sp['N']] == datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y"))

    smart_setup(sp1,verbosity=1)
    with project.open_job(sp1) as job:
        assert(sp1_time == datetime.datetime.strptime(job.document['time'], "%a %b %d %H:%M:%S %Y"))


def test_setup_dli_DM_hexadecane_water_c16e8_solutes_None():
    project = signac.contrib.get_project()
    from setup_dli import setup_dli
    sp ={'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[None,None]}
    with project.open_job(sp) as job:
        assert(not setup_dli(job))
        assert(not os.path.isfile('signac_job_document.json'))
        assert(glob('*') == ['signac_statepoint.json'] or not glob('*'))

def test_setup_dli_DM_hexadecane_water_c16e8_solutes_None2():
    project = signac.contrib.get_project()
    from setup_dli import setup_dli
    sp ={'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':None}
    with project.open_job(sp) as job:
        assert(not setup_dli(job))
        assert(not os.path.isfile('signac_job_document.json'))
        assert(glob('*') == ['signac_statepoint.json'] or not glob('*'))


def test_setup_dli_DM_hexadecane_water_c16e8_solutes():
    project = signac.contrib.get_project()
    from setup_dli import setup_dli
    sp ={'type':'dli','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'solutes':[[['dodecane'],[1]],[['octane'],[3]]]}
    with project.open_job(sp) as job:
        setup_dli(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_dli_dodecane_water_SPC_c12e8():
    sp ={'type':'dli','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'N':25,'surfactant':'c12e8'}
    with project.open_job(sp) as job:
        setup_dli(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')
