# coding: utf-8
import signac
from setup_config_array import setup_config_array, run_config_array
import os

project = signac.contrib.get_project()

def test_setup_config_array_DM_c16e8_hexadecane_water():
    sp = {'type':'config array','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini','slab_thicknesses':[10,10],'solvents':['hexadecane',None],'surfactant':'c16e8','xy_len':6}
    with project.open_job(sp) as job:
        setup_config_array(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'ready')
        run_config_array(job,verbosity=1)
        assert(job.document['stage'] == 'finished')

def test_setup_config_array_DM_c16e8_hexadecane_water_solutes():
    sp = {'type':'config array','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini','slab_thicknesses':[10,10],'solvents':['hexadecane',None],'surfactant':'c16e8','xy_len':6,\
            'solutes':[[['dodecane'],[1]],[['octane'],[3]]]}
    project = signac.contrib.get_project()
    with project.open_job(sp) as job:
        setup_config_array(job,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'ready')
        run_config_array(job,verbosity=1)
        assert(job.document['stage'] == 'finished')
