from subprocess import Popen, PIPE, call
from glob import glob
from gmx_wrapper import Gromacs
import os

gmx = Gromacs()

def xtc_movie(xtc):
    name = xtc.split('.xtc')[0]
    movie_frames(xtc)
    ffmpeg_input = '%s%5d.tga'%(name)
    output_path = frames_to_mp4(ffmpeg_input)
    for tga in glob('%s*tga'%(name)):
        os.remove(tga)
    return output_path

def movie_frames(xtc)
    assert(os.path.isfile(xtc))
    name = xtc.split('.xtc')[0]
    for tpr in glob('*tpr'):
        try:
            gmx.trjconv('-f %s.xtc -s %s -o %s.gro -sep'%(name,tpr))
            break
        except GromacsError:
            continue
    for gro in glob('%s*gro'%name):
        number = gro.split(name)[1].split('.gro')[0]
        tga = '%s%s.tga'%(name,str(number).zfill(5))
        p = Popen(['vmd',gro,'-e','t80aotvizwhite.tcl','-eofexit','-dispdev','text'],stdin=PIPE,stdout=PIPE,stderr=PIPE)
        p.communicate('exit\n')
    return name

def frames_to_mp4(INPUT):
    output_path = '%s.mp4'%(name)
    call(['ffmpeg','-i',INPUT,'-f','mp4','-vcodec','libx264','-pix_fmt','yuv420p',output_path])
    return output_path

def job_movie(job):
    with job:
        if 'full xtc' in job.document:
            movie_frames(job.document['full xtc'])
        else:
            try:
                movie_frames('full.xtc')
            except AssertionError:
                sys.exit('Can\'t find trajectory for job %s'%(job.get_id()))



# for each type in creation, create movie frames
# rename and renumber movie frames
# call frames_to_mp4
