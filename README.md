# Interfacial surfactant simulations on Gromacs with signac

Management of interfacial simulations of surfactants: setup, running, post-processing, and analysis.

- User writes Python scripts specifying the simulations to run
- Simulation engine: Gromacs
- Workspace and data management: signac
- Signac makes data uniquely addressable (findable) in terms of parameters, relieving user of managing the folder structure.
- Reduces manual steps to
  1. Adding and registering topology and structure files for new molecules in each forcefield
  2. Remote job submission (_very ugly right now_)
  3. Specifying the configuration array in addition to any desired umbrella samples, i.e. not yet smart enough to generate a configuration array automatically based on the requested umbrella samples.

**Warning: no support yet for remote job submission**

The hardest task for a new user to this project is probably job control: choosing whether to run a job locally or submit it to a cluster, running remotely, getting data back onto local machine for post-processing and analysis. The current remote workflow is somewhat tedious and error prone, but worse, this process is currently hard-coded to only work with flux.engin.umich.edu.
For more details, see [detailed instructions on how remote submission works](#remote).

<a name="usage"></a>
## Usage

To run a session script:

    python session_c12e8_tensions.py

Here's what a session script looks like that launches an array of simulations to compute interfacial tension.

```python
import signac
from smart_setup import smart_setup
from copy import copy

project = signac.contrib.get_project()

sp_base = {'type':'tension',
           'Pz':1.0,
           'T':300.0,
           'forcefield':'gromos53a6oxy+D',
           'slab_thicknesses':[10,10],
           'solvents':['branched_c11','water_SPC'],
           'surfactant':'c12e8',
           'xy_len':6,
           }

# create a list of statepoints
statepoint_list = []
for N in range(1, 68, 3):
    sp = copy(sp_base)
    sp['N'] = N
    statepoint_list.append(sp)

# set up the statepoints
smart_setup(statepoint_list, runtime=20000)
```

This equilibrates solvent slabs, pairs them, adds surfactants, pulls them to the interface, equilibrates the resulting double-loaded interface with surfactants, and pre-processes the production run to measure surface tension. All that's left to the user is [perform remote job submission](#remote) (_local runs not yet supported for surface tension_) before bringing it back to the local machine for post-processing and analysis.

## Contents
- [Usage](#usage)
- [Statepoints](#sp)
- [Dependencies](#deps)
- [Testing](#testing)
- [Important files and folders](#files)
- [Jobs and stages](#stages)
- [Scripts](#scripts)
- [Detailed instructions on how remote submission works](#remote)
- [Example statepoints](#examples)

<a name="sp"></a>
## Statepoints

### Statepoint rules

Solvents are alphabetically ordered, unless a solvent is `None`, in which case it comes second. Two unique solvents must be chosen.

*Valid examples:*
`['dodecane', 'water_SPC']`, `['water_SPC', 'xylene']`, `['hexane', None]`

*Invalid examples:*
`['water_SPC', 'dodecane']`, `[None, 'xylene']`, `[None, None]`, `['water_SPC', 'water_SPC']`

Surfactant structures are stretched, with tail pointing in -z direction. For a list of structure files, run `python resources/list_all_pdb.py` Structure is required at this point, can't just generate surfactant from SMILES string for example.

### Statepoint types

![Image of statepoint types and their dependencies](statepoints.png)

### List of all parameters and their descriptions
| parameter | description |
|-----------|-------------|
| `type`         | Type of simulation to be conducted  |
| `solvents`     | Two solvents                        |
| `surfactant`   | Surfactant molecule                 |
| `N`            | Number of surfactant molecules                 |
| `xy_len`       | Length of x,y sides of box (nm)         |
| `T`            | Temperature (K)
| `Pz`           | Pressure in z-direction (bar)         |
| `z`            | Distance to interface (nm)\*        |
| `k`            | Harmonic spring constant (kJ mol^-1 nm^-2) |

\* `z` is defined this way as a convenience. In practice, the center of mass of solvent A is used as the reference position, and the surfactant is restrained relative to that.

All statepoints have the attributes `type` and `forcefield`.

|`type`                  |`abbrev`|`slab_thickness(es)`|`solvent(s)`|`surfactant`|`N`   |`xy_len`|`T`|`Pz`|`z`|`k`|
|:----------------------:|:------:|:------------------:|:----------:|:--------- :|:----:|:------:|:-:|:--:|:-:|:-:|
|Slab                    | slab   |singular            |singular    |            |      |  X     |X  |X   |   |   |
|Pair                    | pair   |plural              |plural      |            |      |  X     |X  |X   |   |   |
|Single-loaded interface | sli    |plural              |plural      | X          | X    |  X     |X  |X   |   |   |
|Double-loaded interface | dli    |plural              |plural      | X          | X    |  X     |X  |X   |   |   |
|Surface tension         |tension |plural              |plural      | opt.       | opt. |  X     |X  |X   |   |   |
|Configuration array|config\_array|plural              |plural      | X          | X    |  X     |X  |X   |   |   |
|Umbrella sample         |umbrella|plural              |plural      | X          | X    |  X     |X  |X   |X  |X  |



## Dependencies
In these instructions I assume you use exclusively homebrew and python2.7/pip.

###Gromacs v5.1.1+
Go to [http://www.gromacs.org/Downloads](Gromacs website) and follow download/install instructions.

###packmol
Download from [http://www.ime.unicamp.br/~martinez/packmol/](http://www.ime.unicamp.br/~martinez/packmol/)

```bash
./configure
make
```

Install somewhere.

###python v2.7

```bash
brew install python
```

###numpy and scipy

```bash
pip install numpy
pip install scipy
```

###MDAnalysis
MDAnalysis will also install lots of other python modules as requirements.

```bash
pip install MDAnalysis
```

###signac
Signac is not public yet, but if you have access to the private repository, you can install with:

```bash
pip install git+https://$USER@bitbucket.org/glotzer/signac.git#egg=signac --user
```

<a name="testing"></a>
## Testing
After installing nose, if you don't already have it, you can run `nosetests` to check that everything is working. This will run simulations, so it may take several days.

<a name="files"></a>
## Important files and folders

**key\_sets.py**
Statepoint definitions

**setup\_(something).py**
Setup functions live here

**squeezing.py**
Some functions helpful for setup are located here.

**gmx\_wrapper.py, PackmolWrapper.py, PDBTools.py, topology.py, and mdp.py**
Useful functions for dealing with Gromacs

**test\_xx\_(something).py**
Nose test scripts to check everything is working as expected.

**ready.tar**
This is an archived set of run folders, ready to be moved to a remote cluster for submission.

**finished.tar**
This is an archived set of run folders, returned from a remote cluster ready to be unzipped.

### resources
Here is where molecular structures and topologies are kept and registered. 

**resources/\_\_init.py\_\_**
This file contains paths to resources, which are in subdirectories of the resources folder. This is also the file where topology and structure files are registered.

### workspace
This folder contains all the simulation job folders, named by their job id.

<a name="stages"></a>
## Jobs and stages

**Note:** Scripts should proceed with a job unless `stage` is `submitted` or `finished` or if job.document has `run_count` and `run_count` > 0.

There are three types of jobs:

1. setup - A relatively quick one-off job that can be done without significant computational resources.
2. setup+run - A one-off job that benefits that requires significant computational resources.
3. setup+run\_x - A multiple-off job that requires significant computational resources. For example, a simulation whose length may not be decided a priori, which may be run on multiple occasions to build up data.

And there are several stages available to these types of jobs:

- all: `requested`
- all: `processing` (not yet implemented)
- setup: `finished`
- setup+run: `ready`
- setup+run: `running`
- setup+run: `finished`
- setup+run\_x: `ready`\*
- setup+run\_x: `running`\*
- setup+run\_x: `finished`\*

\*setup+run\_x jobs have an additional stage variable in their job documents `run_count`, that indicates how many times job has finished.

**Requested. (not yet implemented)** The statepoint has been requested, but the work to compute it has not been started yet.

Requesting umbrellas is performed before and separately from pre-processing/running requested umbrellas, because if the script running the requested umbrellas fails for some reason, the user should not be expected to re-request the umbrellas. S/he must simply issue the command to run any requested umbrellas again, without having to remember which were requested.

**Processing.** Some initialization/operation is being performed on the statepoint, so it should not be touched. Note that if a job has an error, and the exception is not caught, then the stage will be stuck in `processing`. For this reason, when a setup script is called that marks the job as `processing`, exceptions should be caught, and stage should be reverted to its previous state.

**Ready.** The statepoint is ready to be either run locally or remotely.

**Running. (not yet implemented)** The statepoint job is running and has not finished yet. It should not be assumed a job has failed if this statepoint is observed. Some further check should be done.

**Finished.** The job finished and the statepoint should be usable for post-processing.

<a name="scripts"></a>
## Scripts

**package_remote_submit.py**

Prepare a remote submission package. 

    # check for submission tar file already in place, so I don't delete it
    with tarfile.TarFile(')
    # convert paths to relative paths with os.path

**remote_submit.py**

Execute an mdrun command or python script on a computer away from the signac projet.

<a name="remote"></a>
## Detailed instructions on how remote submission works
- Make sure all of the jobs are in state `ready`
    - If one isn't, then tell user it's not going to be archived.
- PBS/SBATCH jobscript is written for each job.
    - save cluster job ID to job document
    - mdrun or run python control script (TODO: verify that mdrun launched from python is no slower than mdrun launched from bash)
    - updates job document stage at end to finished and also increments `run_count` if job is of type setup+run\_x.
- Master submit script is written 
- Tar file `ready.tar` is opened in project root directory
- Make sure (again) all of the jobs are in state `ready`
    - If one isn't, then tell user it needs to start over.
- Job folders for submission are archived in tar file (relative to tar file).
- User moves tar file to project root directory on remote computer.
- User unpacks tar file. `tar --keep-newer-files -xvf ready.tar`
- Tar file unpacks to workspace/(jobs)/
- Tar file unpacks `remote_submit.py`
- User runs `python remote_submit.py`
    - If all jobs are ready (not finished) and no jobs have been submitted and none started, then submit all jobs.
    - Otherwise, present the user with the jobs categorized as the following:
        - (1) Not submitted, not started, not running, ready
        - (2) Submitted, not started, not running, ready
        - (3) Submitted, started, running, ready
        - (4) Submitted, started, not running, ready
        - (5) Submitted, started, not running, finished
        - (x) Exceptions
- ...
- User eagerly awaits jobs to finish
- ...
- User checks if all jobs have finished with `python remote_submit.py`
    - If all jobs have finished, tell user and package them up to be recieved at home.
    - If some jobs have finished, tell user his/her options:
        - `python remote_submit.py submit X Y Z`
            - Submit jobs in categories X Y Z
        - `python remote_submit.py package X Y Z`
            - tar jobs in categories X Y Z
- Tar file `finished.tar` (or `all.tar`) is opened in (remote) submit folder
- Finished job folders (or all job folders if `package_all` was used) are archived in tar file
- User moves tar file to (local) recieve folder
- User unpacks tar file. `tar --keep-newer-files -xvf finished.tar`

### User workflow
The above seems complicated. Fortunately, what the user actually does is a lot simpler.
It could be even simpler, but I don't have NFS.

```bash
(local)$ python submit_ready.py
... transfer ready.tar file to recieve folder in mirror-ish project folder on remote cluster ...
... user is in remote project root directory ...
(remote)$ scp .../ready.tar .
(remote)$ tar -xvf ready.tar
(remote)$ python remote_submit.py
... wait for sims to finish ...
(remote)$ python remote_submit.py
(remote)$ scp finished.tar ...
... transfer ready.tar file to recieve folder in local project folder ...
... user is in local project root directory ...
(local)$ tar -xvf `
```

## Troubleshooting
**I have more jobs than job ids**
I've encountered this problem previously, but I believe it shouldn't occur with an up-to-date verison of signac.
Check for empty folders, and avoid older versions of signac, because they would have problems in hashing floating points
inconsistently.

<a name="examples"></a>
## Example statepoints
### Slab (slab)

| key | typical value |
|-----|-------------|
|`Pz` | `1.0`|
|`T` | `300.0`|
|`forcefield` | `gromos53a6oxy+D`|
|`slab_thickness` | `10`|
|`solvent` | `water_SPC`|
|`type` | `slab`|
|`xy_len` | `6`|

### Pair (pair)

| key | typical value |
|-----|-------------|
|`Pz` | `1.0`|
|`T` | `300.0`|
|`forcefield` | `gromos53a6oxy+D`|
|`slab_thicknesses` | `[10,10]`|
|`solvents` | `[dodecane,water_SPC]`|
|`type` | `pair`|
|`xy_len` | `6`|

### Single-loaded interface (li)

| key | typical value |
|-----|-------------|
|`Pz` | `1.0`|
|`T` | `300.0`|
|`forcefield` | `gromos53a6oxy+D`|
|`slab_thicknesses` | `[10,10]`|
|`solvents` | `[dodecane,water_SPC]`|
|`surfactant` | `c12e8`|
|`N` | `25`|
|`type` | `li`|
|`xy_len` | `6`|

### Double-loaded interface (dli)

| key | typical value |
|-----|-------------|
|`Pz` | `1.0`|
|`T` | `300.0`|
|`forcefield` | `gromos53a6oxy+D`|
|`slab_thicknesses` | `[10,10]`|
|`solvents` | `[dodecane,water_SPC]`|
|`surfactant` | `c12e8`|
|`N` | `25`|
|`type` | `dli`|
|`xy_len` | `6`|

### Surface tension (tension)

| key | typical value |
|-----|-------------|
|`Pz` | `1.0`|
|`T` | `300.0`|
|`forcefield` | `gromos53a6oxy+D`|
|`slab_thicknesses` | `[10,10]`|
|`solvents` | `[dodecane,water_SPC]`|
|`surfactant` | `c12e8`|
|`N` | `25`|
|`type` | `tension`|
|`xy_len` | `6`|

### Configuration array (config array)

| key | typical value |
|-----|-------------|
|`Pz` | `1.0`|
|`T` | `300.0`|
|`forcefield` | `gromos53a6oxy+D`|
|`slab_thicknesses` | `[10,10]`|
|`solvents` | `[dodecane,water_SPC]`|
|`surfactant` | `c12e8`|
|`N` | `25`|
|`type` | `config array`|
|`xy_len` | `6`|

### Umbrella sample (umbrella)

| key | typical value |
|-----|-------------|
|`Pz` | `1.0`|
|`T` | `300.0`|
|`forcefield` | `gromos53a6oxy+D`|
|`slab_thicknesses` | `[10,10]`|
|`solvents` | `[dodecane,water_SPC]`|
|`surfactant` | `c12e8`|
|`N` | `25`|
|`type` | `umbrella`|
|`xy_len` | `6`|
|`z` | `10.0`|
|`k` | `6000`|

