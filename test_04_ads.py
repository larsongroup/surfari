from setup_ads import setup_ads
project = signac.contrib.get_project()
import os

def test_ads_DM_hexadecane_water_c16e8():
    sp = {'type':'ads','xy_len':6,'solvents':('hexadecane',None),
            'forcefield':'dry martini','slab_thicknesses':(10,10),
            'scale':'cg','T':300.0,'Pz':1.0,'N':25,'surfactant':'c16e8',
            'inner_dist':2,'outer_dist':2}
    with project.open_job(sp) as ads_job:
        setup_ads(ads_job)
