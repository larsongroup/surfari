import pandas as pd

def parse_colvar(tar, colvar_path):
    """Read data from colvar file.
    
    Parameters
    ----------
    colvar : str or file handle
        String or file handle to read csv data from.
    """
    try:
        colvar = tar.extractfile(colvar_path)
    except KeyError:
        colvar = colvar_path
    data = pd.read_csv(colvar, comment='#', sep=' ',
                       skipinitialspace=True,
                       names=['t', 'z', 'C'])
    return data

class JobTable(list):
    """ Overridden list class which takes a list of signac jobs 
        and renders an HTML table in IPython Notebook with the id
        and statepoint attributes. """

    def __init__(self, listarg, user_attribs=None):
        if user_attribs is None:
            self.user_attribs = None
        elif type(user_attribs) is str:
            self.user_attribs = [user_attribs]
        else:
            self.user_attribs = user_attribs
        super(JobTable, self).__init__(listarg)


    def _repr_html_(self):
        """Return html table representing myself, the list of jobs.

        Operations
        ----------
        1. Determine what the attributes are.
        2. Add header row with name of each attribute.
        3. Add row to table for each job, with job id in first column.
        """
        if self.user_attribs is None:
            attribs = set(sum((job.statepoint().keys() for job in self), []))
        else:
            attribs = self.user_attribs
        html = ["<table>"]
        html.append("<td><b>{0}</b></td>".format('id'))
        for attrib in attribs:
            html.append("<td><b>{0}</b></td>".format(attrib))

        for job in self:
            html.append("<tr>")
            html.append("<td><b>{0}</b></td>".format(job.get_id()))

            for attrib in attribs:
                try:
                    html.append("<td>{0}</td>".format(job.statepoint()[attrib]))
                except KeyError:
                    html.append("<td></td>")

            html.append("</tr>")
        html.append("</table>")
        return ''.join(html)
