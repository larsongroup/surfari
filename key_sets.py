statepoint_keys = {}
statepoint_keys['slab'] = set(['solvent', 'slab_thickness', 'forcefield', 'Pz', 'type', 'T', 'xy_len'])
statepoint_keys['surfactant slab'] = set(['solvent', 'surfactant', 'N', 'slab_thickness', 'forcefield', 'Pz', 'type', 'T', 'xy_len'])
statepoint_keys['solution'] = set(['solvent', 'surfactant', 'N', 'slab_thickness', 'forcefield', 'Pz', 'type', 'T', 'xy_len'])
statepoint_keys['pair'] = set(['solvents', 'slab_thicknesses', 'forcefield', 'Pz', 'type', 'T', 'xy_len'])
statepoint_keys['li'] = set(['solvents', 'slab_thicknesses', 'forcefield', 'Pz', 'type', 'T', 'xy_len','N','surfactant'])
statepoint_keys['dli'] = set(['solvents', 'slab_thicknesses', 'forcefield', 'Pz', 'type', 'T', 'xy_len','N','surfactant'])
statepoint_keys['tension'] = set(['solvents', 'slab_thicknesses', 'forcefield', 'Pz', 'type', 'T', 'xy_len','N','surfactant'])
statepoint_keys['config array'] = set(['solvents', 'slab_thicknesses', 'forcefield', 'Pz', 'type', 'T', 'xy_len','N','surfactant'])
statepoint_keys['ads'] = set(['solvents', 'slab_thicknesses', 'forcefield', 'Pz', 'type', 'T', 'xy_len','N','surfactant','z1','z2','start_z','k'])
statepoint_keys['umbrella'] = set(['solvents', 'slab_thicknesses', 'forcefield', 'Pz', 'type', 'T', 'xy_len','N','surfactant','z','k'])
statepoint_keys['2surf config array'] = set(['solvent', 'slab_thickness', 'forcefield', 'Pz', 'type', 'T', 'xy_len','surfactant'])
statepoint_keys['2surf umbrella'] = set(['solvent', 'slab_thickness', 'forcefield', 'Pz', 'type', 'T', 'xy_len','surfactant','r','k'])


def check_statepoint(statepoint,type):
    assert(statepoint['type'] == type)
    assert(set(statepoint.keys()).issuperset(statepoint_keys[type]))
