import signac

project = signac.contrib.get_project()

def make_jobscript(job,computer='flux',account='rlarson_flux'):
    run_type = job.document['run_type']
    # TODO handle MPI needs
    if run_type == 'mdrun':
        run_statement = 'gmx mdrun '+job.document['mdrun_args']
    elif run_type == 'python':
        run_statement = 'python '+job.document['python_args']
    else:
        raise NotImplementedError('Don\'t know how to handle job type {}'.format(run_type))

    if job.statepoint()['forcefield'] == 'dry martini':
        ppn = 1
        wallh = 24
    elif job.statepoint()['forcefield'] == 'martini':
        ppn = 4
        wallh = 24
    elif job.statepoint()['forcefield'] == 'gromos53a6oxy+D':
        ppn = 12
        wallh = 52

    if computer.lower().strip() == 'flux':
        pbs_opts = {}
        pbs_opts['-S'] = '/bin/sh'
        pbs_opts['-N'] = job.get_id()
        pbs_opts['-M'] = project.config['email']
        pbs_opts['-q'] = 'flux'
        pbs_opts['-A'] = account
        pbs_opts['-j'] = 'oe'
        pbs_opts['-l'] = 'nodes={n:d}:ppn={p:d},mem=1gb,walltime={h:d}:00:00'.format(
                         n=1,
                         p=ppn,
                         h=wallh)
        pbs_opts['-V'] = ''

        #pbs_opts['-d'] = '' NOTE this must be set by the remote_submit script!

        maxh = max(0.99*wallh, wallh-0.0008)
        if run_type == 'mdrun':
            run_statement = run_statement + ' -maxh {maxh:f}'.format(
                            maxh=maxh)
            words = run_statement.strip().split()
            deffnm = words[words.index('-deffnm')+1]
        elif run_type == 'python':
            run_statement = run_statement.replace('$MAXH', str(maxh))
            if job.statepoint()['type'] == 'config array':
                deffnm = 'pull_out'
        else:
            raise NotImplementedError('Don\'t know how to handle job type {}'.format(run_type))

        # If a log file is present, look for line with "time exceeded"
        # and "will terminate" in some line of the log file. Take Step
        # from that line and make stopped_step. Continue doing for the
        # whole file.
        # Note this will error if the log file isn't present, but that
        # shouldn't stop the batch script.
        prerun = ('python -c "import json\n'
                  'log = \'{0:s}.log\'\n'
                  'stopped_step = None\n'
                  'try:\n'
                  '    with open(log) as f:\n'
                  '        for line in f.readlines():\n'
                  '            if \'time exceeded\' in line and \'will terminate\' in line:\n'
                  '                words = line.strip().split()\n'
                  '                stopped_step = int(words[1].strip(\':\'))\n'
                  'except IOError:\n'
                  '    stopped_step = None'
                  'if stopped_step is not None:\n'
                  '    job_document = json.load(open(\'signac_job_document.json\'))\n'
                  '    job_document[\'stopped_step\'] = stopped_step\n'
                  '    json.dump(job_document, open(\'signac_job_document.json\', \'w\'))\n"'.format(deffnm))

        # If "time exceeded" and "will terminate" in some line of the
        # log file and step is greater than stopped_step, set stage =
        # stopped
        # Note this will error if the log file isn't present, but that
        # shouldn't stop the batch script. The simulation will then be
        # categorized as an eXception, since it was submitted and started
        # but not running.
        postrun = ('python -c "import json\n'
                   'log = \'{0:s}.log\'\n'
                   'stopped_step = None\n'
                   'try:\n'
                   '    with open(log) as f:\n'
                   '        for line in f.readlines():\n'
                   '            if \'time exceeded\' in line and \'will terminate\' in line:\n'
                   '                words = line.strip().split()\n'
                   '                stopped_step = int(words[1].strip(\':\'))\n'
                   'except IOError:\n'
                   '    stopped_step = None\n'
                   'job_document = json.load(open(\'signac_job_document.json\'))\n'
                   'old_stopped_step = job_document.pop(\'stopped_step\', -1)\n'
                   'if stopped_step is not None and stopped_step > old_stopped_step:\n'
                   '        job_document[\'stage\'] = \'stopped\'\n'
                   'else:\n'
                   '        job_document[\'stage\'] = \'finished\'\n'
                   'json.dump(job_document, open(\'signac_job_document.json\', \'w\'))\n"'.format(deffnm))

        lines = []
        for key in pbs_opts:
            lines.append('#PBS {flag} {opts}\n'.format(flag=key,
                                                       opts=pbs_opts[key]))
 
        lines.append('echo "My job id is $PBS_JOBID"\n'
                      '\n'
                      'if [ -s "$PBS_NODEFILE" ] ; then\n'
                      '    echo "Running on"\n'
                      '    cat $PBS_NODEFILE\n'
                      'fi\n'
                      '\n'
                      'if [ -d "$PBS_O_WORKDIR" ] ; then\n'
                      '    cd $PBS_O_WORKDIR\n'
                      '    echo "Running from $PBS_O_WORKDIR"\n'
                      'fi\n'
                      '\n'
                      'echo "I am in $PWD"\n'
                      'echo "Saving job.document[\"run_count started\"] ='
                      ' job.document[\"run_count\"]" and setting submitted'
                      ' flag, perhaps redundantly, in case user manually'
                      ' submitted me.\n'
                      'python -c "import json; job_document ='
                      ' json.load(open(\'signac_job_document.json\'));'
                      ' job_document[\'run_count started\'] ='
                      ' job_document[\'run_count\'];'
                      ' json.dump(job_document,open(\'signac_job_document.json\',\'w\'))"\n'
                      'python -c "import json; job_document ='
                      ' json.load(open(\'signac_job_document.json\'));'
                      ' job_document[\'run_count submitted\'] ='
                      ' job_document[\'run_count\'];'
                      ' json.dump(job_document,open(\'signac_job_document.json\',\'w\'))"\n'
                      #'echo "activate conda env: source activate surf"\n'
                      #'source activate surf\n'
                      '\n'
                      '{prerun}\n'
                      '{run}\n'
                      'ERROR_CODE=$?\n'
                      'echo "Updating job document stage"\n'
                      'echo "Run program exited with code $ERROR_CODE"\n'
                      '{postrun}\n'
                      'if [[ $ERROR_CODE != 0 ]]; then\n'
                      '  echo "Run had an error"\n'
                      '  python -c "import json; job_document ='
                      ' json.load(open(\'signac_job_document.json\'));'
                      ' job_document[\'stage\'] = \'error\';'
                      ' json.dump(job_document,open(\'signac_job_document.json\',\'w\'))"\n'
                      'fi\n'.format(
                          run=run_statement,
                          prerun=prerun,
                          postrun=postrun))
 
        with job:
            with open('run.pbs','w') as pbsfile:
                pbsfile.writelines(lines)

