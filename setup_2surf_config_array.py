"""
    Configuration array:
    Surfactant is pulled from a single-loaded interface both inward and outward with respect to solvent slab A. These pulling trajectories are then concatenatd into a single trajectory `config_array.xtc` which can be used for configurations of surfactant at different distances from the reference slab A. 
"""
import signac
import resources as res
import time
import gmx_wrapper
import MDAnalysis as mda
from math import sqrt
import numpy as np
from identify_slab import identify_slab
from shutil import copyfile

project = signac.contrib.get_project()

def setup_2surf_config_array(this_job,force=False,verbosity=0):
    """
        This sets up a configuration array if dependencies are available.
        This function should be called /after/ verifying that the job is actually requested.
    """
    from setup_base import match, subdict
    from solution import setup_solution, run_solution
    from key_sets import check_statepoint
    from PDBTools import pdbcat, NDX
    from topology import gmx_make_top
    from mdp import Gromacs_mdp

    print 'Setting up job %s'%(this_job.get_id())
    statepoint = this_job.statepoint()
    check_statepoint(statepoint,'2surf config array')
    # Probably redundant check that job hasn't already been computed
    if not force and 'stage' in this_job.document and this_job.document['stage'] == 'finished':
        print 'job %s is already finished. skipping setup.'%(this_job.get_id())
        return False

    if 'solutes' in statepoint:
        solutes = statepoint['solutes']
        if statepoint['solutes'] is None or (solutes[0] is None and solutes[1] is None):
            print 'Statepoint cannot have None solutes... leave solutes key out entirely!'
            return False
    
    inherited_keys = ('xy_len','solvent','forcefield',\
                     'slab_thickness','surfactant','T','Pz')
    opt_keys = ('solutes',)

    # Check, create (if lacking), and load dependencies
    solutions = [solution for solution in project.find_jobs()\
            if 'type' in solution.statepoint()\
            and solution.statepoint()['type'] == 'solution'\
            and solution.statepoint()['N'] == 2\
            and 'stage' in solution.document\
            and solution.document['stage'] == 'finished'\
            and match(solution.statepoint(), statepoint, inherited_keys, opt_keys)]

    if len(solutions) == 0:
        solution_sp = subdict(statepoint, inherited_keys, opt_keys=opt_keys)
        solution_sp['type'] = 'solution'
        solution_sp['N'] = 2
        check_statepoint(solution_sp,'solution')
        with project.open_job(solution_sp) as solution_job:
            setup_solution(solution_job, 500, verbosity=verbosity)
            run_solution(solution_job, verbosity=verbosity)
    elif len(solutions) > 1:
        print 'Multiple loaded interfaces are usable for job %s'%(this_job.get_id())
        print 'Using the first returned by project.find_jobs()'
        solution_job = solutions[0]
    else:
        solution_job = solutions[0]

    #########################################
    # Dependencies exist, so set up the configuration array
    eq_conf = solution_job.workspace()+'/'+'NPT_0.gro'
    surfactant = statepoint['surfactant']
    ff = statepoint['forcefield']
    T = statepoint['T']
    Pz = statepoint['Pz']

    surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]

    copyfile(eq_conf,'NPT_0.gro')
    copyfile(eq_conf.replace('.gro', '.tpr'),'NPT_0.tpr')

    gmx = gmx_wrapper.Gromacs(verbosity=verbosity,ignore_pme_warning=True)

    # Reload translated box and choose low z, hi z
    u = mda.Universe('NPT_0.tpr', 'NPT_0.gro')
    surf = u.select_atoms(surf_sel_str)
    resid1 = list(set(surf.resids))[0]
    resid2 = list(set(surf.resids))[1]
    surf1_sel_str = surf_sel_str+' and resid %d'%resid1
    surf2_sel_str = surf_sel_str+' and resid %d'%resid2
    surf1 = u.select_atoms(surf1_sel_str)
    surf2 = u.select_atoms(surf2_sel_str)

    low_r = 0.1
    hi_r = 0.48*min(u.dimensions[:3])
    mda.lib.mdamath.make_whole(surf1)
    mda.lib.mdamath.make_whole(surf2)
    dp = ((surf1.center_of_mass() - surf2.center_of_mass())/u.dimensions[:3]) % 1.
    dsq = ((-1.*(dp > 0.5) + 1.*(dp < -0.5) + dp) * u.dimensions[:3])**2.
    surf_r = sqrt(np.sum(dsq))

    u = mda.Universe('NPT_0.gro')
    surf = u.select_atoms(surf_sel_str)
    resid1 = list(set(surf.resids))[0]
    resid2 = list(set(surf.resids))[1]
    surf1_sel_str = surf_sel_str+' and resid %d'%resid1
    surf2_sel_str = surf_sel_str+' and resid %d'%resid2

    # Create ndx file
    ndx = NDX('NPT_0.gro','ca.ndx')
    ndx.AppendMDASelectionStr(surf1_sel_str,'surf1')
    ndx.AppendMDASelectionStr(surf2_sel_str,'surf2')
    with open('ca.top','w') as top:
        top.write(gmx_make_top(ff, 'NPT_0.gro'))
    pull_in_rate = -0.002
    pull_out_rate = 0.002
    pull_k = 6000

    # Write mdp file from template
    mdp = Gromacs_mdp(ff=ff,type='pull')
    if Pz != 1.0 or T != 300.0:
        raise NotImplementedError('Pz != 1.0 and T !=300.0 not yet supported')
    mdp['nsteps'] = abs((low_r - surf_r)/10.)/abs(pull_in_rate*mdp['dt'])
    mdp['pull_ngroups'] = 2
    mdp['pull_ncoords'] = 1
    mdp['pull_group1_name'] = 'surf1'
    mdp['pull_group2_name'] = 'surf2'
    mdp['pull_coord1_type'] = 'umbrella'
    mdp['pull_coord1_geometry'] = 'distance'
    mdp['pull_coord1_dim'] = 'Y Y Y'
    mdp['pull_coord1_start'] = 'yes'
    mdp['pull_coord1_groups'] = '1 2'
    mdp['pull_coord1_rate'] = pull_in_rate
    mdp['pull_coord1_k'] = pull_k
    mdp.write('pull_in.mdp')
    mdp['nsteps'] = abs((hi_r - surf_r)/10.)/abs(pull_in_rate*mdp['dt'])
    mdp['pull_coord1_rate'] = pull_out_rate
    mdp.write('pull_out.mdp')

    gmx.grompp('-f pull_in.mdp -c NPT_0.gro -p ca.top'+\
                ' -n ca.ndx -o pull_in.tpr')
    gmx.grompp('-f pull_out.mdp -c NPT_0.gro -p ca.top'+\
                ' -n ca.ndx -o pull_out.tpr')
    #
    #################################################

    # Clean up
    this_job.document['stage'] = 'ready'
    this_job.document['time'] = time.ctime()
    this_job.document['pull_in_rate'] = pull_in_rate
    this_job.document['pull_out_rate'] = pull_out_rate
    this_job.document['pull_k'] = pull_k
    this_job.document['run_count'] = 0
    this_job.document['run_type'] = 'python'
    this_job.document['python_args'] = '-c "import signac; project = signac.contrib.get_project(); import sys; sys.path.append(\'../../\'); from setup_config_array import run_config_array; run_config_array(project.open_job(id=\'%s\'), maxh=$MAXH)"'%(this_job.get_id())

def run_2surf_config_array(this_job, num_threads=None, verbosity=0,  maxh=None):
    statepoint = this_job.statepoint()
    surfactant = statepoint['surfactant']
    surf_sel_str = 'resname '+res.resname_of_molecule[surfactant]

    if this_job.document['stage'] != 'ready':
        print('Job is not in stage ready. Will not run.')
        return False

    gmx = gmx_wrapper.Gromacs(ignore_pme_warning=True, verbosity=verbosity)

    if num_threads is not None:
        nt = '-nt {:d}'.format(num_threads)
    else:
        nt = ''
    if maxh is not None:
        maxh = '-maxh {:f}'.format(0.98*maxh/2.)
    else:
        maxh = ''

    with this_job:
        gmx.mdrun('-deffnm pull_in {nt} {maxh} -cpi'.format(
                    nt=nt,
                    maxh=maxh))
        try:
            gmx.mdrun('-deffnm pull_out {nt} {maxh} -cpi'.format(
                        nt=nt,
                        maxh=maxh))
        except gmx_wrapper.GromacsError as gmx_error:
            if 'Distance between pull groups' in gmx_error.message:
                print 'Error occurred due to pulling too far. Trying to finish...'
            else:
                raise gmx_error

        gmx.trjcat('-f pull_in.xtc pull_out.xtc -o config_array.xtc -cat')

        u = mda.Universe('pull_in.tpr','config_array.xtc')

        surf = u.select_atoms(surf_sel_str)
        resid1 = list(set(surf.resids))[0]
        resid2 = list(set(surf.resids))[1]
        surf1_sel_str = surf_sel_str+' and resid %d'%resid1
        surf2_sel_str = surf_sel_str+' and resid %d'%resid2
        surf1 = u.select_atoms(surf1_sel_str)
        surf2 = u.select_atoms(surf2_sel_str)
        rmin = 1e9
        rmax = -1e9
        for ts in u.trajectory:
            mda.lib.mdamath.make_whole(surf1)
            mda.lib.mdamath.make_whole(surf2)
            dp = ((surf1.center_of_mass() - surf2.center_of_mass())/u.dimensions[:3]) % 1.
            dsq = ((-1.*(dp > 0.5) + 1.*(dp < -0.5) + dp) * u.dimensions[:3])**2.
            r = sqrt(np.sum(dsq))
            if r < rmin:
                rmin = r
            elif r > rmax:
                rmax = r
    #
    #########################################

    # Clean up
    this_job.document['stage'] = 'finished'
    this_job.document['time'] = time.ctime()
    this_job.document['r_min'] = rmin/10.
    this_job.document['r_max'] = rmax/10.
