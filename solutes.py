import resources as res
import MDAnalysis as mda
from gmx_wrapper import Gromacs
from PackmolWrapper import PackmolInput
from identify_slab import identify_slab
import os
from PDBTools import pdbcat

def validate_solutes(solutes):
    """
        Check to make sure solutes is valid solute definition.
        I don't like this solute definition, I think there's a cleaner way that's
        less confusing, but I'm sticking with this for now because it works.
    """
    assert(type(solutes) == list)
    if len(solutes) == 2:
        try:
            A_none_flag = False
            B_none_flag = False
            solutesA = solutes[0]
            solutesB = solutes[1]
            assert(solutesA is not None or solutesB is not None)
            if solutesA is not None:
                assert(type(solutesA) == list and len(solutesA) == 2)
                assert(type(solutesA[0]) == list and type(solutesA[1]) == list)
                for molecule, number in zip(solutesA[0],solutesA[1]):
                    assert(type(molecule) in (str, unicode))
                    assert(type(number) == int)
            if solutesB is not None:
                assert(type(solutesB) == list and len(solutesB) == 2)
                assert(type(solutesB[0]) == list and type(solutesB[1]) == list)
                for molecule, number in zip(solutesB[0],solutesB[1]):
                    assert(type(molecule) in (str, unicode))
                    assert(type(number) == int)
        except AssertionError:
            raise NotImplementedError('solutes validation failed... possibly'
                                      ' a new test is needed for single-phase'
                                      ' solutes')

def process_statepoint_solutes(statepoint):
    """
        Get `solutes` from statepoint and validate
        If everything goes okay, return `solutes`=None
    """
    if not 'solutes' in statepoint:
        solutes = None
    else:
        solutes = statepoint['solutes']
        validate_solutes(solutes)
    return solutes
    

def add_solutes(start_conf,ff,solvents,solutes,gmx=None):
    """
        Add solutes to `start_conf`
        solutesA = solutes[0]
        solutesB = solutes[1]
        solutes A are 
        
        - TODO extend to a single slab
    """
    solventA = solvents[0]
    solventB = solvents[1]
    solutesA = solutes[0]
    solutesB = solutes[1]
    packmol_tol = res.packmol_tols[res.ff_scale[ff]]

    A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
    if solventB:
        B_sel_str = 'resname %s'%res.resname_of_molecule[solventB]
    if gmx is None:
        gmx = Gromacs()

    # TODO this belongs somewhere else

    # Read box dimensions, put all atoms inside box, and identify slab A
    u = mda.Universe(start_conf)
    Lx,Ly,Lz,_,_,_ = u.dimensions
    gmx.editconf('-f %s -o %s -pbc yes'%(start_conf,start_conf))
    z1,z2,w,_ = identify_slab(start_conf,start_conf,A_sel_str)
    confs_to_cat = []
    # Add solutes with packmol
    if solutesA is not None:
        # remove A temporarily so solute can be placed over it
        packmol = PackmolInput(packmol_tol,'pdb','A_solutes_plus.pdb',packmol_tol)
        not_A = u.select_atoms('not %s'%(A_sel_str))
        if not_A:
            with mda.Writer('noA.pdb') as W:
                W.write(not_A)
            packmol.addStructure('noA.pdb',1)
            packmol.addConstraintFixed(0.,0.,0.,0.,0.,0.)
        for molecule,number in zip(solutes[0][0],solutes[0][1]):
            packmol.addStructure(os.path.relpath(res.abspath)+'/'+res.ff_molecule_pdb[ff][molecule],number)
            if z1 < z2:
                packmol.addConstraintInsideBox(packmol_tol/2.,packmol_tol/2.,z1+w+packmol_tol/2.,\
                        Lx-packmol_tol/2.,Ly-packmol_tol/2.,z2-w-packmol_tol/2.)
            else:
                packmol.addConstraintOutsideBox(packmol_tol/2.,packmol_tol/2.,z2-w-packmol_tol/2.,\
                        Lx-packmol_tol/2.,Ly-packmol_tol/2.,z1+packmol_tol/2.)
                packmol.addConstraintInsideBox(packmol_tol/2.,packmol_tol/2.,packmol_tol/2.,\
                        Lx-packmol_tol/2.,Ly-packmol_tol/2.,Lz-w-packmol_tol/2.)
        packmol.run()
        solutesA_sel_str = 'resname '+' or resname '.join([res.resname_of_molecule[mol] for mol in solutesA[0]])
        keep_solutes_A = mda.Universe('A_solutes_plus.pdb').select_atoms(solutesA_sel_str)
        with mda.Writer('A_solutes.pdb') as W:
            W.write(keep_solutes_A)
        gmx.editconf('-f A_solutes.pdb -box %f %f %f -o A_solutes.pdb'%(Lx/10.,Ly/10.,Lz/10.))
        confs_to_cat.append('A_solutes.pdb')
    if solutesB:
        # remove B temporarily so solute can be placed over it
        packmol = PackmolInput(packmol_tol,'pdb','B_solutes_plus.pdb',packmol_tol)
        if solventB:
            with mda.Writer('noB.pdb') as W:
                W.write(u.select_atoms('not %s'%(B_sel_str)))
        else:
            with mda.Writer('noB.pdb') as W:
                W.write(u.atoms)
        packmol.addStructure('noB.pdb',1)
        packmol.addConstraintFixed(0.,0.,0.,0.,0.,0.)
        for molecule,number in zip(solutes[1][0],solutes[1][1]):
            packmol.addStructure(os.path.relpath(res.abspath)+'/'+res.ff_molecule_pdb[ff][molecule],number)
            if z1 < z2:
                packmol.addConstraintOutsideBox(packmol_tol/2.,packmol_tol/2.,z1-w-packmol_tol/2.,\
                        Lx-packmol_tol/2.,Ly-packmol_tol/2.,z2+w+packmol_tol/2.)
                packmol.addConstraintInsideBox(packmol_tol/2.,packmol_tol/2.,packmol_tol/2.,\
                        Lx-packmol_tol/2.,Ly-packmol_tol/2.,Lz-w-packmol_tol/2.)
            else:
                packmol.addConstraintInsideBox(packmol_tol/2.,packmol_tol/2.,z2+w+packmol_tol/2.,\
                        Lx-packmol_tol/2.,Ly-packmol_tol/2.,z1-w-packmol_tol/2.)
        packmol.run()
        solutesB_sel_str = 'resname '+' or resname '.join([res.resname_of_molecule[mol] for mol in solutesB[0]])
        keep_solutes_B = mda.Universe('B_solutes_plus.pdb').select_atoms(solutesB_sel_str)
        with mda.Writer('B_solutes.pdb') as W:
            W.write(keep_solutes_B)
        gmx.editconf('-f B_solutes.pdb -box %f %f %f -o B_solutes.pdb'%(Lx/10.,Ly/10.,Lz/10.))
        confs_to_cat.append('B_solutes.pdb')

    confs_to_cat.append(start_conf)
    pdbcat('solvent_overlap_solutes.pdb',*confs_to_cat)
    gmx.editconf('-f solvent_overlap_solutes.pdb -o solvent_overlap_solutes.pdb -resnr 1')
    u = mda.Universe('solvent_overlap_solutes.pdb')
    if solventB:
        overlap = u.select_atoms('(%s or %s) and around %f not (%s or %s)'%(B_sel_str,A_sel_str,packmol_tol,B_sel_str,A_sel_str))
    else:
        overlap = u.select_atoms('(%s) and around %f not (%s)'%(A_sel_str,packmol_tol,A_sel_str))
    delete = overlap.residues.atoms
    save = mda.core.AtomGroup.AtomGroup(list(set(u.atoms._atoms)-set(delete._atoms)))
    with mda.Writer(start_conf) as W:
        W.write(save)
