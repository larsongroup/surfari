import signac
import resources as res
import MDAnalysis as mda
import os

project = signac.contrib.get_project()

def mda_get_resname(path):
    """
        If there is only one molecule type (i.e. one resame), return the resname
        Otherwise, raise an exception, because I don't know which resname is wanted
    """
    resnames = set(mda.Universe(path).atoms.resnames)
    if len(resnames) != 1:
        raise Exception
    else:
        return list(resnames)[0]

def resnames_from_pdb(pdb):
    """
        Takes template top file and adds molecules that are in
        pdb to output top file `outtop`
    """
    recordname = ''
    lastresid = ''
    reslist = []
    resnumlist = []
    with open(pdb,'r') as pdbfile:
        for line in pdbfile.readlines():
            recordname = line[0:6].strip()
            if recordname == 'ATOM' or recordname == 'HETATM':
                resname = line[17:20].strip()
                resid = line[22:26].strip()
                if lastresid == resid and lastresname == resname:
                    pass
                else:
                    lastresid = resid
                    if reslist and reslist[-1] == resname:
                        resnumlist[-1] = resnumlist[-1] + 1
                    else:
                        reslist.append(resname)
                        resnumlist.append(1)
                lastresname = resname
    return reslist,resnumlist


def resnames_from_gro(gro):
    """
        Takes template top file and adds molecules that are in
        gro to output top file `outtop`
    """
    recordname = ''
    lastresid = ''
    reslist = []
    resnumlist = []
    with open(gro,'r') as grofile:
        for line in grofile.readlines()[2:-1]:
            resname = line[5:10].strip()
            resid = int(line[:5].strip())
            if lastresid == resid and lastresname == resname:
                pass
            else:
                lastresid = resid
                if reslist and reslist[-1] == resname:
                    resnumlist[-1] = resnumlist[-1] + 1
                else:
                    reslist.append(resname)
                    resnumlist.append(1)
            lastresname = resname
    return reslist,resnumlist


def gmx_make_top(ff,*args):
    """
        Make topology file given ff and path to structure file or list of (resname,N) pairs.

        - Can take either a path string to a structure file for which topology file
        should be created.
        - Or can takes list of (resname,N) pairs.
        - Either way, the first argument ff must identify a forcefield itp
        via res.ff_itp
    """
    itps = []

    if len(args) == 1 and type(args[0]) == str:
        # Assume I'm given the path to a structure file
        path = args[0]
        ext = path.split('.')[-1]
        if ext == 'pdb':
            resnames,Ns = resnames_from_pdb(path)
        elif ext == 'gro':
            resnames,Ns = resnames_from_gro(path)
        else:
            raise IOError('Don\'t know how to read file of type %s'%(ext))
        molecules = [res.molecule_of_resname[resname] for resname in resnames]
    elif type(args[0]) == tuple:
        # Assume I'm given a list of (mol,N) tuples
        molecules = [arg[0] for arg in args]
        Ns        = [arg[1] for arg in args]
    else:
        raise TypeError()

    res_path = project.root_directory()+'/resources/'
    itp_path = os.path.relpath(res_path + res.ff_itp[ff])
    if itp_path not in itps:
        itps.append(itp_path)
    for molecule in molecules:
        itp_path = os.path.relpath(res_path + res.ff_molecule_itp[ff][molecule])
        if itp_path not in itps:
            itps.append(itp_path)

    resnames = [res.resname_of_molecule[molecule] for molecule in molecules]
    return _gmx_compile_top(itps,resnames,Ns)

def _gmx_compile_top(itps,resnames,Ns):
    # Write ff itp(s) (embedded in gmx ff)
    # Write mol itp(s) (derived from gmx ff)
    # System section (no info needed)
    # Molecules section (number of mols and order they appear in struct file needed)
    top_text = ''
    if type(itps) != str:
        itps_no_dupes = []
        for itp in itps:
            if itp not in itps_no_dupes:
                itps_no_dupes.append(itp)
        for itp in itps_no_dupes:
            top_text += '#include "'+itp+'"\n'
    else:
        top_text += '#include "'+itps+'"\n'
    top_text += '[ system ]\n\n'
    top_text += '[ molecules ]\n'
    for resname,N in zip(resnames,Ns):
        top_text += '%s %d\n'%(resname,N)
    return top_text
