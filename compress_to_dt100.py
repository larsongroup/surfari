# Open every trajectory, and if it has more frames than dt=100, trjconv it and delete the old trajectory
import os
import signac
import MDAnalysis as mda
from gmx_wrapper import Gromacs

project = signac.contrib.get_project()
gmx = Gromacs()

def thin(tpr, xtc, dt):
    xtc_dt100 = xtc.split('.xtc')[0]+'_dt100.xtc'
    gmx.trjconv('-s %s -f %s -o %s -dt %d'%(tpr, xtc, xtc_dt100, dt), stdin_text='System\n')
    os.rename(xtc_dt100, xtc)

for sp in project.find_statepoints({'type':'tension'}):
    with project.open_job(sp) as job:
        if 'run_count' in job.document:
            num = job.document['run_count'] + (job.document['stage'] == 'finished')
            for i in range(num+1):
                tpr = 'NAPzT_%d.tpr'%i
                xtc = 'NAPzT_%d.xtc'%i
                try:
                    u = mda.Universe(tpr, xtc)
                    if u.trajectory.dt < 100:
                        thin(tpr, xtc, 100)
                    else:
                        pass
                except IOError as ioe:
                    print(ioe)

for sp in project.find_statepoints({'type':'umbrella'}):
    with project.open_job(sp) as job:
        if 'run_count' in job.document:
            num = job.document['run_count'] + (job.document['stage'] == 'finished')
            for i in range(num):
                if os.path.isfile('pullx-umbrella_%d.xvg'%i):
                    tpr = 'umbrella_%d.tpr'%i
                    xtc = 'umbrella_%d.xtc'%i
                    u = mda.Universe(tpr, xtc)
                    if u.trajectory.dt < 100:
                        thin(tpr, xtc, 100)
                    else:
                        pass
