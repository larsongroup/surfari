"""
    Interface loading:
    Surfactants are arranged into a lattice which is placed adjacent to the interface.
    Overlapping solvent atoms are deleted, and broken molecules are then removed.
    The box is squeezed and then equilibrated.
"""
import signac
import resources as res
import time
import gmx_wrapper
from setup_base import match, subdict
from setup_pair import setup_pair
from key_sets import check_statepoint
from PDBTools import pdbcat, NDX
import MDAnalysis as mda
import numpy as np
from topology import gmx_make_top
from mdp import Gromacs_mdp
from glob import glob
import os
from identify_slab import identify_slab
from math import floor, ceil, sqrt
from shutil import copyfile
from solutes import add_solutes, process_statepoint_solutes
from squeezing import squeeze

project = signac.contrib.get_project()

def setup_dli_from_dli(this_job, dli_job, force=False, num_threads=None, verbosity=0):
    """
        Set up a loaded interface from a more-loaded interface
    """
    statepoint = this_job.statepoint()
    match_keys = [key for key in statepoint if key not in ("N","surfactant")]
    for key in match_keys:
        assert(statepoint[key] == dli_job.statepoint()[key])

    gmx = gmx_wrapper.Gromacs(verbosity=verbosity,\
            ignore_pme_warning=True,\
            ignore_cg_radii_warning=True)

    eq_time = 500

    ff = statepoint['forcefield']
    T0 = statepoint['T']
    Pz0 = statepoint['Pz']
    solvents = statepoint['solvents']
    solventA = solvents[0]
    surfactant = statepoint['surfactant']

    gmx.editconf('-f %s -o %s'%(dli_job.workspace()+'/li_eq.gro', 'li_dep.pdb'))

    if statepoint['N'] < dli_job.statepoint()['N']:
        copyfile(dli_job.workspace()+'/li_eq.tpr', 'li_dep.tpr')
        u = mda.Universe('li_dep.tpr', 'li_dep.pdb')

        A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
        surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]

        surf = u.select_atoms(surf_sel_str)
        surf1 = surf[:len(surf)/2]
        surf2 = surf[len(surf)/2:]
        keep_surf1 = surf1.residues[:-(dli_job.statepoint()['N']-statepoint['N'])].atoms
        keep_surf2 = surf2.residues[:-(dli_job.statepoint()['N']-statepoint['N'])].atoms
        not_surf = u.select_atoms('not %s'%surf_sel_str)

        with mda.Writer('li_pre.pdb') as W:
            W.write(not_surf+keep_surf1+keep_surf2)
    elif statepoint['N'] == dli_job.statepoint()['N']:
        copyfile('li_dep.pdb', 'li_pre.pdb')
    else:
        raise Exception('this_job N must be less than or equal to li_job N')

    with open('li.top','w') as top:
        top.write(gmx_make_top(ff,'li_pre.pdb'))

    if not None in solvents:
        mdp = Gromacs_mdp(ff=ff,type='eq')
        mdp.add_params({'ref-p':(1.0,Pz0)})
        mdp.add_params({'ref-t':T0})
        mdp.add_params({'nsteps':eq_time/mdp['dt']})
        mdp.write('eq.mdp')

    elif None in solvents:
        mdp = Gromacs_mdp(ff=ff,type='nvt')
        mdp.add_params({'gen-vel':'yes'})
        mdp.add_params({'gen-temp':T0})
        mdp.add_params({'ref-t':T0})
        mdp.add_params({'nsteps':eq_time/mdp['dt']})
        mdp.write('eq.mdp')

    gmx.grompp('-f eq.mdp -c li_pre.pdb -p li.top -o li_eq.tpr')

    this_job.document['eq_time'] = eq_time

def setup_dli_from_pair(this_job,pair_job,force=False,num_threads=None,verbosity=0):
    """
        Set up a loaded interface from a solvent pair
    """
    statepoint = this_job.statepoint()
    pair_tpr = '%s/pair_eq.tpr'%(pair_job.workspace())
    pair_gro = '%s/pair_eq.gro'%(pair_job.workspace())
    Lz = mda.Universe(pair_gro).dimensions[2]/10.

    gmx = gmx_wrapper.Gromacs(verbosity=verbosity,\
            ignore_pme_warning=True,\
            ignore_cg_radii_warning=True)


    # Check for adequate metadata and resources as best I can (misses existence of itp files,
    # but I'll take care of this when I make itp paths relative, by copying itp files to job dir)
    ff = statepoint['forcefield']
    packmol_tol = res.packmol_tols[res.ff_scale[ff]]
    xy_len = statepoint['xy_len']
    N = statepoint['N']
    lat_dim_x = floor(sqrt(N))
    lat_dim_y = ceil(N/lat_dim_x)

    T0 = statepoint['T']
    Pz0 = statepoint['Pz']

    res_path = project.root_directory()+'/resources/'

    eq_time = 500

    solvents = statepoint['solvents']
    solventA = solvents[0]
    solventB = solvents[1]
    surfactant = statepoint['surfactant']

    solutes = process_statepoint_solutes(statepoint)

    A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
    surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]

    copyfile(res_path+'/'+res.ff_molecule_pdb[ff][surfactant],'surf.pdb')
    copyfile(res_path+'/'+res.ff_molecule_itp[ff][surfactant],'surf.itp')

    if res.hydrophobicity[solventA] < res.hydrophobicity[solventB]:
        gmx.editconf('-f surf.pdb -o surf.pdb -rotate 180 0 0')

    if solventB is not None:
        B_resname = res.resname_of_molecule[solventB]

    assert(Gromacs_mdp(ff=ff,type='em')), 'no mdp definition for ff %s or type em'%ff
    assert(Gromacs_mdp(ff=ff,type='pull')), 'no mdp definition for ff %s or type pull'%ff
    if None in solvents:
        assert(Gromacs_mdp(ff=ff,type='nvt')), 'no mdp definition for ff %s or type nvt'%ff
    else:
        assert(Gromacs_mdp(ff=ff,type='squeeze')), 'no mdp definition for ff %s or type squeeze'%ff
        assert(Gromacs_mdp(ff=ff,type='eq')), 'no mdp definition for ff %s or type eq'%ff

    gmx.editconf('-f %s -box %f %f 1 -o %s'%('surf.pdb',xy_len/lat_dim_x,xy_len/lat_dim_y,'surf.pdb'))
    gmx.genconf('-f %s -nbox %d %d 1 -o %s'%('surf.pdb',lat_dim_x,lat_dim_y,'lat.pdb'))
    u = mda.Universe('lat.pdb')
    keep = u.residues[:N].atoms
    with mda.Writer('keep.pdb') as W:
        W.write(keep)
    assert(len(set(keep.residues)) == N)
    gmx.editconf('-f keep.pdb -o keep2.pdb -resnr %d'%(N+1))
    gmx.editconf('-f %s -o pair_eq.pdb'%(pair_gro))
    pdbcat('catted.pdb','keep.pdb','keep2.pdb','pair_eq.pdb')
    gmx.editconf('-f catted.pdb -box %f %f %f -o catted.pdb -resnr 1'%(xy_len,xy_len,Lz))
    u = mda.Universe('catted.pdb')
    surf = u.select_atoms(surf_sel_str)
    surf1 = surf[:len(surf)/2]
    surf2 = surf[len(surf)/2:]
    z1,z2,w,_ = identify_slab('catted.pdb','catted.pdb',A_sel_str)

    surf1.positions = np.vstack([surf1.positions[:,0],\
                                surf1.positions[:,1],\
                                -surf1.positions[:,2]]).T

    surf1.positions = np.vstack([surf1.positions[:,0],\
                                surf1.positions[:,1],\
                                surf1.positions[:,2]+(z1-w-packmol_tol)-max(surf1.positions[:,2])]).T

    surf2.positions = np.vstack([surf2.positions[:,0],\
                                surf2.positions[:,1],\
                                surf2.positions[:,2]+(z2+w+packmol_tol)-min(surf2.positions[:,2])]).T
    if solventB is not None:
        B_sel_str = 'resname %s'%(B_resname)
        not_B = u.select_atoms('not (%s)'%B_sel_str)
        B = u.select_atoms('%s'%B_sel_str)
        B_close = u.select_atoms('(%s) and around %f not (%s)'%(B_sel_str,packmol_tol,B_sel_str))
        B_delete = B_close.residues.atoms
        B_save = mda.core.groups.AtomGroup(list(set(B.indices)-set(B_delete.indices)), u)
        with mda.Writer('fixed.pdb') as W:
            W.write(not_B+B_save)
    else:
        with mda.Writer('fixed.pdb') as W:
            W.write(u.atoms)

    # Add solutes with packmol
    if solutes is not None:
        add_solutes('fixed.pdb',ff,solvents,solutes)

    u = mda.Universe('fixed.pdb')
    surf = u.select_atoms(surf_sel_str)
    surf1 = surf[:len(surf)/2]
    surf2 = surf[len(surf)/2:]
    try:
        align_group_sel_str = res.ff_molecule_interface_align_group[ff][surfactant]
        align_group = surf2.select_atoms(align_group_sel_str)
    except KeyError:
        align_group = surf2
    with open('li.top','w') as top:
        top.write(gmx_make_top(ff,'fixed.pdb'))
    mdp = Gromacs_mdp(ff=ff,type='em')
    mdp.write('em.mdp')
    gmx.grompp('-f em.mdp -c fixed.pdb -p li.top -o em.tpr')
    if num_threads:
        gmx.mdrun('-deffnm em -nt %d'%(num_threads))
    else:
        gmx.mdrun('-deffnm em')
    ndx = NDX('em.gro','li.ndx')
    ndx.AppendMDASelectionStr(A_sel_str,'pull_ref')
    for i,resid in zip(range(1,2*N+1),list(set(surf.resids))):
        ndx.AppendMDASelectionStr(surf_sel_str+' and resid %d'%resid,'pull_surf%d'%i)
    pull_lat_rate = -0.0005
    mdp = Gromacs_mdp(ff=ff,type='pull')
    mdp.add_params({'nsteps':abs((z2-align_group.center_of_mass()[2])/(10.*pull_lat_rate*mdp['dt']))})
    mdp.add_params({'pull_ngroups':2*N+1})
    mdp.add_params({'pull_ncoords':2*N})
    mdp.add_params({'pull_group1_name':'pull_ref'})
    for i in range(1,2*N+1):
        mdp.add_params({'pull_group%d_name'%(i+1):'pull_surf%d'%i})
        mdp.add_params({'pull_coord%d_type'%i:'umbrella'})
        mdp.add_params({'pull_coord%d_geometry'%i:'distance'})
        mdp.add_params({'pull_coord%d_dim'%i:'N N Y'})
        mdp.add_params({'pull_coord%d_start'%i:'yes'})
        mdp.add_params({'pull_coord%d_groups'%i:'1 %d'%(i+1)})
        mdp.add_params({'pull_coord%d_rate'%i:'%f'%(pull_lat_rate)})
        mdp.add_params({'pull_coord%d_k'%i:250})
    mdp.write('pull_lat.mdp')
    gmx.grompp('-f pull_lat.mdp -c em.gro -p li.top -n li.ndx -o pull_lat.tpr')
    if num_threads:
        gmx.mdrun('-deffnm pull_lat -c li_squeeze0.gro -nt %d'%(num_threads))
    else:
        gmx.mdrun('-deffnm pull_lat -c li_squeeze0.gro')

    # Squeeze unless there's supposed to be empty space
    if not None in solvents:
        squeeze('li', statepoint, verbosity=verbosity, num_threads=num_threads)

        # Combine squeezes into one trajectory
        gmx.trjcat('-f {} -cat -o full_squeeze.xtc'.format(' '.join(glob('li_squeeze*xtc'))))

        mdp = Gromacs_mdp(ff=ff,type='eq')
        mdp.add_params({'ref-p':(1.0,Pz0)})
        mdp.add_params({'ref-t':T0})
        mdp.add_params({'nsteps':eq_time/mdp['dt']})
        mdp.write('eq.mdp')

    elif None in solvents:
        mdp = Gromacs_mdp(ff=ff,type='nvt')
        mdp.add_params({'gen-vel':'yes'})
        mdp.add_params({'gen-temp':T0})
        mdp.add_params({'ref-t':T0})
        mdp.add_params({'nsteps':eq_time/mdp['dt']})
        mdp.write('eq.mdp')

    gmx.grompp('-f eq.mdp -c li_squeeze{:d}.gro -p li.top -o li_eq.tpr'.format(
                                                    len(glob('li_squeeze*gro'))-1))

    this_job.document['eq_time'] = eq_time
    this_job.document['pull_lat_rate'] = pull_lat_rate


def setup_dli(this_job,force=False,num_threads=None,verbosity=0):
    """
        This sets up a loaded interface if dependencies are available.
        This function should be called /after/ verifying that the job is actually requested.
    """
    starttime = time.time()
    print 'Setting up job %s'%(this_job.get_id())
    statepoint = this_job.statepoint()
    check_statepoint(statepoint,'dli')
    # Probably redundant check that job hasn't already been computed
    if not force and 'stage' in this_job.document and this_job.document['stage'] == 'finished':
        print 'job %s is already finished. skipping setup.'%(this_job.get_id())
        return False

    solvents = statepoint['solvents']
    solventA = solvents[0]
    solventB = solvents[1]
    surfactant = statepoint['surfactant']
    N = statepoint['N']
    surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]

    try:
        solutes = process_statepoint_solutes(statepoint)
    except AssertionError:
        print 'Invalid solutes definition'
        return False

    if solutes is not None:
        if solutes[0] is not None:
            for molecule in solutes[0][0]:
                assert(molecule != solventB), 'solute cannot be same as solvent'
                assert(molecule != solventA), 'solute cannot be same as solvent'
                assert(molecule != surfactant), 'solute cannot be same as surfactant'
        if solutes[1] is not None:
            for molecule in solutes[1][0]:
                assert(molecule != solventB), 'solute cannot be same as solvent'
                assert(molecule != solventA), 'solute cannot be same as solvent'
                assert(molecule != surfactant), 'solute cannot be same as surfactant'
        if solutes[0] is None and solutes[1] is None:
            print 'Statepoint cannot have None solutes... leave solutes key out entirely!'
            return False

    # Check, create (if lacking), and load job dependencies
    # 1. Check for another dli which is identical except for having more surfactants N
    # 2. Check for another dli which is identical except for having equal or more surfactants N
    #    and although `surfactant` is different, res.ff_molecule_pdb[ff][surfactant] is
    #    the same. Then copy that except use my itp file and equilibrate.
    # 3. Check for a pair to make dli from scratch. If a pair doesn't exist, set it up.

    inherited_keys = [key for key in statepoint if key != 'N']
    similar_li = [job for job in project.find_jobs()\
                  if match(job.statepoint(), statepoint, inherited_keys)\
                  and 'stage' in job.document\
                  and job.document['stage'] == 'finished'
                  and job.statepoint()['N'] > this_job.statepoint()['N']]

    if similar_li:
        # Make loaded interface by cutting out some surfactant from a more-loaded interface
        setup_dli_from_dli(this_job, similar_li[0], force, num_threads, verbosity)
    else:
        inherited_keys = [key for key in statepoint if key not in ('N', 'surfactant')]
        similar_li = [job for job in project.find_jobs()\
                      if match(job.statepoint(), statepoint, inherited_keys)\
                      and 'stage' in job.document\
                      and job.document['stage'] == 'finished'\
                      and job.statepoint()['N'] >= this_job.statepoint()['N']\
                      and res.ff_molecule_pdb[job.statepoint()['forcefield']][job.statepoint()['surfactant']] ==\
                      res.ff_molecule_pdb[this_job.statepoint()['forcefield']][this_job.statepoint()['surfactant']]]
        if similar_li:
            # Make loaded interface by swapping itp file and possibly cutting out some surfactant
            # from a more-loaded interface
            setup_dli_from_dli(this_job, similar_li[0], force, num_threads, verbosity)
        else:
            # Make loaded interface from pair
            inherited_keys = ('xy_len','forcefield','T','Pz','solvents','slab_thicknesses')

            pairs = [job for job in project.find_jobs()\
                    if 'type' in job.statepoint()\
                    and job.statepoint()['type'] == 'pair'\
                    and 'stage' in job.document\
                    and job.document['stage'] == 'finished'
                    and match(job.statepoint(), statepoint, inherited_keys)]

            if len(pairs) == 0:
                pair_sp = subdict(statepoint, inherited_keys)
                pair_sp['type'] = 'pair'
                check_statepoint(pair_sp,'pair')
                with project.open_job(pair_sp) as pair_job:
                    setup_pair(pair_job,num_threads=num_threads,verbosity=verbosity)
            elif len(pairs) > 1:
                print 'Multiple pairs are usable for job %s'%(this_job.get_id())
                print 'Using the first returned by project.find_jobs()'
                pair_job = pairs[0]
            else:
                pair_job = pairs[0]

            setup_dli_from_pair(this_job, pair_job, force, num_threads, verbosity)

    #########################################
    # Dependencies exist, so set up the loaded interface
    gmx = gmx_wrapper.Gromacs(verbosity=verbosity,\
            ignore_pme_warning=True,\
            ignore_cg_radii_warning=True)

    if num_threads:
        gmx.mdrun('-deffnm li_eq -nt %d'%(num_threads))
    else:
        gmx.mdrun('-deffnm li_eq')

    assert(len(mda.Universe('li_eq.tpr','li_eq.gro').select_atoms(surf_sel_str).residues) == 2*N)

    # Combine all setup into one trajectory
    if not similar_li:
        if not None in solvents:
            gmx.trjcat('-f pull_lat.xtc {} li_eq.xtc -cat -o full.xtc'.format(' '.join(glob('li_squeeze*xtc'))))
        elif None in solvents:
            gmx.trjcat('-f pull_lat.xtc li_eq.xtc -cat -o full.xtc')
    # Discard extra trajectories
    for xtc in glob('slab_squeeze*xtc'):
        os.remove(xtc)
    if os.path.isfile('pull_lat.xtc'):
        os.remove('pull_lat.xtc')
    os.remove('li_eq.xtc')
    #
    #########################################

    # Clean up
    this_job.document['stage'] = 'finished'
    this_job.document['time'] = time.ctime()
    this_job.document['wall time'] = time.time() - starttime
