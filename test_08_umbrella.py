# coding: utf-8
import signac
from umbrella import setup_umbrella, run_umbrella
import os

project = signac.contrib.get_project()

sp = {'type':'umbrella','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini',
        'slab_thicknesses':[10,10],'solvents':['hexadecane',None],
        'surfactant':'c16e8','xy_len':6,'z':-2.,'k':6000}

# NOTE: First four tests assume that job doesn't exist yet
def test_setup_umbrella_DM_c16e8_hexadecane_water():
    with project.open_job(sp) as job:
        setup_umbrella(job,1000,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'ready')
        assert(job.document['run_count'] == 0)

def test_run_umbrella_DM_c16e8_hexadecane_water():
    with project.open_job(sp) as job:
        run_umbrella(job)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')
        assert(job.document['run_count'] == 0)

def test_setup2_umbrella_DM_c16e8_hexadecane_water():
    with project.open_job(sp) as job:
        setup_umbrella(job,1000,verbosity=1,resume=True)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'ready')
        assert(job.document['run_count'] == 1)

def test_run2_umbrella_DM_c16e8_hexadecane_water():
    with project.open_job(sp) as job:
        run_umbrella(job)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')
        assert(job.document['run_count'] == 1)

def test_setup_umbrella_DM_c16e8_hexadecane_water_solutes():
    sp = {'type':'umbrella','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini',\
            'slab_thicknesses':[10,10],'solvents':['hexadecane',None],\
            'surfactant':'c16e8','xy_len':6,'z':-2.,'k':6000,\
            'solutes':[[['dodecane'],[1]],[['octane'],[3]]]}
    project = signac.contrib.get_project()
    with project.open_job(sp) as job:
        setup_umbrella(job,1000,verbosity=1)
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'ready')
