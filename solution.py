import signac
import resources as res
import time
from setup_slab import setup_slab
from setup_surfactant_slab import setup_surfactant_slab
from setup_base import match, subdict
import MDAnalysis as mda
from key_sets import check_statepoint
from mdp import Gromacs_mdp
from gmx_wrapper import Gromacs
from solutes import process_statepoint_solutes
from shutil import copyfile
from umbrella import run_umbrella
import numpy as np

project = signac.contrib.get_project()


def setup_solution(this_job,runtime,resume=False,verbosity=0):
    """
        This sets up a solution calculation if dependencies are avaialble.
        This function should be called /after/ verifying that the job is
        actually requested.
    """
    if ('stage' in this_job.document and this_job.document['stage'] == 'submitted'):
        print('Job %s has stage submitted... won\'t continue'%this_job.get_id())
        return False

    print('Setting up job %s'%(this_job.get_id()))
    statepoint = this_job.statepoint()
    check_statepoint(statepoint,'solution')
    solvent = statepoint['solvent']
    surfactant = statepoint['surfactant']

    try:
        solutes = process_statepoint_solutes(statepoint)
    except AssertionError:
        print('Invalid solutes definition')
        return False

    if solutes is not None:
        if solutes is not None:
            for molecule in solutes[0]:
                assert(molecule != solvent), 'solute cannot be same as solvent'
        if solutes is None:
            print('Statepoint cannot have None solutes... leave solutes key out entirely!')
            return False

    gmx = Gromacs(verbosity=verbosity)

    # Check, create (if lacking), and load dependencies
    if surfactant is not None:
        seek_type = 'surfactant slab'
        inherited_keys = ('N','xy_len','solvent','forcefield',
                         'slab_thickness', 'surfactant','T','Pz')
    else:
        assert(statepoint['N'] == 0)
        seek_type = 'slab'
        inherited_keys = ('xy_len', 'solvent', 'forcefield',
                         'slab_thickness', 'T', 'Pz')

    seek_jobs = [seek_job for seek_job in project.find_jobs()
                 if 'type' in seek_job.statepoint()
                 and seek_job.statepoint()['type'] == seek_type
                 and 'stage' in seek_job.document
                 and seek_job.document['stage'] == 'finished'
                 and match(seek_job.statepoint(), statepoint, inherited_keys)]

    if len(seek_jobs) == 0:
        seek_sp = subdict(statepoint, inherited_keys, opt_keys=('solutes',))
        seek_sp['type'] = seek_type
        with project.open_job(seek_sp) as seek_job:
            print('First need to set up job %s'%(seek_job.get_id()))
            if seek_type == 'surfactant slab':
                setup_surfactant_slab(seek_job)
            elif seek_type == 'slab':
                setup_slab(seek_job)
            else:
                raise ValueError('Unknown job type {}'.format(seek_type))
    elif len(seek_jobs) > 1:
        print('Multiple {0} are usable for job {1}'.format(seek_type, this_job.get_id()))
        print('Using the first returned by project.find_jobs()')
        seek_job = seek_jobs[0]
    else:
        seek_job = seek_jobs[0]

    #########################################
    # Dependencies exist, so set up the pair
    ff = statepoint['forcefield']

    mdp = Gromacs_mdp(ff=ff, type='NPT')
    mdp.add_params({'nsteps':int(runtime/mdp.params['dt'])})

    if ('stage' in this_job.document and this_job.document['stage'] == 'finished') or ('run_count' in this_job.document and this_job.document['run_count'] > 0):
        if resume:
            if this_job.document['stage'] == 'finished':
                run_count = this_job.document['run_count'] + 1
            elif this_job.document['stage'] in ('processing','ready'):
                run_count = this_job.document['run_count']
            else:
                raise Exception
            this_job.document['run_count'] = run_count
            this_job.document['stage'] = 'processing'

            total_t = 0.
            for i in range(run_count):
                tinit = float(gmx.dump_key('NPT_%d.tpr'%(i),'tinit')[0])
                dt = float(gmx.dump_key('NPT_%d.tpr'%(i),'dt')[0])
                nsteps = float(gmx.dump_key('NPT_%d.tpr'%(i),'nsteps')[0])
                assert(np.isclose(total_t, tinit))
                total_t += nsteps*dt
            mdp['tinit'] = total_t
            mdp.write('NPT_%d.mdp'%run_count)
            gmx.grompp('-f NPT_%d.mdp -c NPT_%d.tpr -t NPT_%d.cpt -p slab.top -o NPT_%d.tpr'%(run_count,run_count-1,run_count-1,run_count))
            mdrun_args = '-deffnm NPT_%d -cpi'%(run_count)
        else:
            print('job %s is already finished or its run_count is greater than 0, and resume==False. skipping setup.'%this_job.get_id())
            return False
    else:
        run_count = 0
        this_job.document['stage'] = 'processing'

        if seek_type in ('surfactant slab', 'slab'):
            copyfile('%s/slab_eq.tpr'%(seek_job.workspace()),'slab_eq.tpr')
            copyfile('%s/slab_eq.cpt'%(seek_job.workspace()),'slab_eq.cpt')
            copyfile('%s/slab.top'%(seek_job.workspace()),'slab.top')
            mdp.write('NPT_%d.mdp'%run_count)
            gmx.grompp('-f NPT_%d.mdp -c slab_eq.tpr -t slab_eq.cpt -p slab.top -o NPT_%d.tpr'%(run_count,run_count))
        else:
            raise ValueError('Unknown job type {}'.format(seek_type))
        mdrun_args = '-deffnm NPT_%d -cpi'%(run_count)

    # Clean up
    this_job.document['grompp time %d'%run_count] = time.ctime()
    this_job.document['job_type'] = 'setup_run_x'
    this_job.document['run_count'] = run_count
    this_job.document['run_type'] = 'mdrun'
    this_job.document['mdrun_args'] = mdrun_args
    try:
        del(this_job.document['job id'])
    except:
        pass
    this_job.document['stage'] = 'ready'

def run_solution(job,force=False,num_threads=None, verbosity=0):
    run_umbrella(job, force, num_threads, verbosity)
