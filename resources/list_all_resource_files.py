from __init__ import ff_molecule_pdb, ff_molecule_itp

print 'User should check that surfactant tails are pointed in -z direction'

print 'All pdb files listed in resources/__init__.py:'
for ff in ff_molecule_pdb:
    for mol in ff_molecule_pdb[ff]:
        print ff_molecule_pdb[ff][mol]

print 'All itp files listed in resources/__init__.py:'
for ff in ff_molecule_itp:
    for mol in ff_molecule_itp[ff]:
        print ff_molecule_itp[ff][mol]
