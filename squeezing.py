import MDAnalysis as mda
import numpy as np
from mdp import Gromacs_mdp
from gmx_wrapper import Gromacs
import resources as res
from glob import glob
import os


def empty_xyz_bins(tpr,gro):
    """
        Bin atoms into roughly cubic bins sized to have 20 molecules per bin
    """
    u = mda.Universe(tpr,gro)
    Lx,Ly,Lz,_,_,_ = u.dimensions
    u.atoms.pack_into_box()
    vbin = (Lx*Ly*Lz/len(u.atoms)*20)
    lbin = vbin**(1./3.)
    nx = ny = int(round(Lx/lbin))
    lx = Lx/nx
    lz = vbin/lx**2
    nz = int(round(Lz/lbin))
    hist,bin_edges = np.histogramdd(u.atoms.positions,bins=(nx,ny,nz))
    return (hist == 0).any()

def eliminate_empty_z_bins(tpr,gro,tol):
    """
        TODO find longest contiguous hole in z profile. move right side of hole
        to right side of box. reduce box length by width of hole minus packmol_tol.
        change dimensions altogether, can't just do z [2]
    """
    u = mda.Universe(tpr,gro)
    Lx,Ly,Lz,_,_,_ = u.dimensions
    u.atoms.pack_into_box()
    # choose bin size to have at least 60 atoms per bin
    bins = len(u.atoms)/60
    hist,bin_edges = np.histogram(u.atoms.positions[:,2],weights=u.atoms.masses,bins=bins)

    biggest_left = None
    biggest_right = None
    left = None
    right = None
    zero_flag = False
    if 0 in hist:
        # find zero
        for p,l,r in zip(hist,bin_edges[:-1],bin_edges[1:]):
            if not zero_flag and p == 0:
                zero_flag = True
                left = l
                right = r
                if not biggest_left:
                    biggest_left = left
                    biggest_right = right
            elif zero_flag and p == 0:
                right = r
                if right-left > biggest_right-biggest_left:
                    biggest_left = left
                    biggest_right = right
            elif zero_flag and p != 0:
                zero_flag = False

        if biggest_right-biggest_left <= tol:
            return False

        u.atoms.positions += np.array([[0,0,Lz - biggest_right]]*len(u.atoms))
        u.atoms.pack_into_box()
        u.dimensions = u.dimensions - np.array([0,0,(biggest_right-biggest_left)-tol,0,0,0])
        with mda.Writer(gro) as W:
            W.write(u)
        return True
    else:
        return False

def density_z_profile(tpr,gro):
    u = mda.Universe(tpr,gro)
    Lx,Ly,Lz,_,_,_ = u.dimensions
    u.atoms.pack_into_box()
    # choose bin size to have at least 60 atoms per bin
    bins = len(u.atoms)/60
    hist,bin_edges = np.histogram(u.atoms.positions[:,2],weights=u.atoms.masses,bins=bins)
    bin_centers = (bin_edges[:-1]+bin_edges[1:])/2.
    return bin_centers, hist

def density_correlation(tpr,gro):
    """
        Bin atoms into density profile and return correlation of adjacent
        bins where bins are sized to have at least 60 atoms per bin on average.
    """
    z,rho = density_z_profile(tpr,gro)
    rho -= np.mean(rho)
    corr = np.correlate(rho,rho,mode='full')
    corr = corr[len(corr)/2:]/max(corr)
    return corr[1]

def squeeze(name, statepoint, check_corr=False, verbosity=0, num_threads=None):
    for xtc in glob('slab_squeeze*xtc'):
        os.remove(xtc)

    prefix = '{}_squeeze'.format(name)

    T0 = statepoint['T']
    Pz0 = statepoint['Pz']
    ff = statepoint['forcefield']
    packmol_tol = res.packmol_tols[res.ff_scale[ff]]

    final_squeeze = False
    min_squeezes = 3
    Lz = 1e999
    corr = 1e999
    Pz = Pz0*1.
    T = T0*1.
    kT = 0.3
    kP = 0.3
    P_bump_factor = 3.
    T_bump = 1.2
    i = 0

    gmx = Gromacs(ignore_pme_warning=True, ignore_cg_radii_warning=True)
    mdp = Gromacs_mdp(ff=ff,type='squeeze')
    while i < min_squeezes or not final_squeeze:
        if verbosity > 0:
            print('Pz: {:f}'.format(Pz))
            print('T: {:f}'.format(T))
        T  -= kT*(T - T0)
        Pz -= kP*(Pz - Pz0)

        mdp.add_params({'ref-t':T})
        mdp.add_params({'ref-p':(1.0,Pz)})
        mdp.write('squeeze.mdp')
        gmx.grompp('-f squeeze.mdp -c {p:s}{0:d}.gro -p {n:s}.top -o'
                   ' {p:s}{1:d}.tpr'.format(i, i+1, p=prefix, n=name))
        if num_threads:
            gmx.mdrun('-deffnm {p:s}{0:d} -nt {nt:d}'.format(i+1,
                                                             nt=num_threads,
                                                             p=prefix))
        else:
            gmx.mdrun('-deffnm {p:s}{0:d}'.format(i+1, p=prefix))
        i += 1
        prev_Lz = Lz
        Lz = mda.Universe('{p:s}{0:d}.gro'.format(i, p=prefix)).dimensions[2]
        if Lz > prev_Lz*1.05:
            Pz *= P_bump_factor

        if check_corr:
            prev_corr = corr
            corr = density_correlation('{p:s}{0:d}.tpr'.format(i, p=prefix),
                                       '{p:s}{0:d}.gro'.format(i, p=prefix))

        if eliminate_empty_z_bins('{p:s}{0:d}.tpr'.format(i, p=prefix),
                                  '{p:s}{0:d}.gro'.format(i, p=prefix),
                                  packmol_tol):
            mdp.add_params({'gen_vel':'yes','gen_temp':T})
        else:
            mdp.add_params({'gen_vel':'no'})
            if empty_xyz_bins('{p:s}{0:d}.tpr'.format(i, p=prefix),
                              '{p:s}{0:d}.gro'.format(i, p=prefix)):
                Pz *= P_bump_factor
                T += T_bump
            elif check_corr and corr >= 0.3 and corr > prev_corr*1.05:
                Pz *= P_bump_factor
                T += T_bump
            elif (Lz-prev_Lz)/prev_Lz < 0.01 and (not check_corr or corr < 0.3)\
                    and i >= min_squeezes:
                # if there was no empty z space to eliminate,
                # and if there are no empty xyz bins
                # and if density correlation was less than 0.3
                # and if the box shrank (or only grew 1%)
                # and if i >= min_squeezes
                # then we're done
               final_squeeze = True
            for backup in glob('*#*'):
                os.remove(backup)
