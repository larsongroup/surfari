import signac
import tarfile
import os
import sys
from make_jobscript import make_jobscript
project = signac.contrib.get_project()

def write_remote_submit_py(path):
    rspl = remote_submit_py_lines = []
    rspl.append('from subprocess import call\n')
    rspl.append('import tarfile\n')
    rspl.append('import signac\n')
    rspl.append('import sys\n')
    rspl.append('import os\n')
    rspl.append('import shutil\n')
    rspl.append('from glob import glob\n')
    rspl.append('from collections import defaultdict\n')
    rspl.append('\n')
    rspl.append('cat_description = {}\n')
    rspl.append('cat_description["1"] = "Not submitted, not started, not running, ready"\n')
    rspl.append('cat_description["2"] = "Submitted, not started, not running, ready"\n')
    rspl.append('cat_description["3"] = "Submitted, started, running, ready"\n')
    rspl.append('cat_description["4"] = "Submitted, started, not running, ready"\n')
    rspl.append('cat_description["5"] = "Submitted, started, not running, finished"\n')
    rspl.append('cat_description["E"] = "Simulation errors"\n')
    rspl.append('cat_description["S"] = "User Stopped"\n')
    rspl.append('cat_description["P"] = "Packaged"\n')
    rspl.append('cat_description["x"] = "Exceptions"\n')
    rspl.append('\n')
    rspl.append('# Check for command line arguments\n')
    rspl.append('if len(sys.argv) > 1:\n')
    rspl.append('    command = sys.argv[1]\n')
    rspl.append('    assert(command in ("submit","package","help","show","delete"))\n')
    rspl.append('else:\n')
    rspl.append('    command = None\n')
    rspl.append('\n')
    rspl.append('if len(sys.argv) > 2:\n')
    rspl.append('    arg_cats = sys.argv[2:]\n')
    rspl.append('    for cat in arg_cats:\n')
    rspl.append('        if cat not in cat_description:\n')
    rspl.append('            sys.exit("Error: unrecognized category label %s"%cat)\n')
    rspl.append('else:\n')
    rspl.append('    arg_cats = None\n')
    rspl.append('\n')
    rspl.append('if command == "submit" and arg_cats is None:\n')
    rspl.append('    sys.exit("Error: Cannot submit if category label is not given.")\n')
    rspl.append('\n')
    rspl.append('if command == "package" and arg_cats is None:\n')
    rspl.append('    sys.exit("Error: Cannot package if category label is not given.")\n')
    rspl.append('\n')
    rspl.append('# Characterize jobs\n')
    rspl.append('project = signac.contrib.get_project()\n')
    rspl.append('jobs = set([job for job in project.find_jobs()])\n')
    rspl.append('ready_jobs = set([job for job in jobs if "stage" in job.document and job.document["stage"]=="ready"])\n')
    rspl.append('finished_jobs = set([job for job in jobs if "stage" in job.document and job.document["stage"]=="finished"])\n')
    rspl.append('error_jobs = set([job for job in jobs if "stage" in job.document and job.document["stage"]=="error"])\n')
    rspl.append('stopped_jobs = set([job for job in jobs if "stage" in job.document and job.document["stage"]=="stopped"])\n')
    rspl.append('packaged_jobs = set([job for job in jobs if "stage" in job.document and job.document["stage"]=="packaged"])\n')
    rspl.append('started_jobs = set([job for job in jobs if "run_count started" in job.document and job.document["run_count started"] == job.document["run_count"]])\n')
    rspl.append('running_jobs = set([])\n')
    rspl.append('submitted_jobs = set([])\n')
    rspl.append('queued_jobs = set([])\n')
    rspl.append('completed_jobs = set([])\n')
    rspl.append('held_jobs = set([])\n')
    rspl.append('for job in jobs:\n')
    rspl.append('    if "run_count submitted" in job.document and job.document["run_count submitted"] == job.document["run_count"]:\n')
    rspl.append('        submitted_jobs.add(job)\n')
    rspl.append('    elif "run_count submitted" in job.document:\n')
    rspl.append('        assert(int(job.document["run_count submitted"]) < int(job.document["run_count"]))\n')
    rspl.append('with open("qstat.stderr","w") as stderr:\n')
    rspl.append('    with open("qstat.stdout","w") as stdout:\n')
    rspl.append('        call(["qstat"], stdout=stdout, stderr=stderr)\n')
    rspl.append('\n')
    rspl.append('job_status_dict = defaultdict(lambda: None)\n')
    rspl.append('\n')
    rspl.append('with open("qstat.stdout", "r") as stdout:\n')
    rspl.append('    for line in stdout:\n')
    rspl.append('        if "." in line:\n')
    rspl.append('            words = line.split()\n')
    rspl.append('            compare_id = words[0].split(".")[0]\n')
    rspl.append('            status = words[4]\n')
    rspl.append('            job_status_dict[compare_id] = status\n')
    rspl.append('\n')
    rspl.append('for job in submitted_jobs:\n')
    rspl.append('    status = job_status_dict[job.document["pbs id"]]\n')
    rspl.append('    if status is not None:\n')
    rspl.append('        if status == "R":\n')
    rspl.append('            running_jobs.add(job)\n')
    rspl.append('        if status == "Q":\n')
    rspl.append('            queued_jobs.add(job)\n')
    rspl.append('        if status == "C":\n')
    rspl.append('            completed_jobs.add(job)\n')
    rspl.append('        if status == "H":\n')
    rspl.append('            held_jobs.add(job)\n')
    rspl.append('catP = packaged_jobs\n')
    rspl.append('catE = error_jobs - catP\n')
    rspl.append('catS = stopped_jobs - catP\n')
    rspl.append('cat1 = ready_jobs - submitted_jobs - started_jobs - running_jobs - catS - catE - catP\n')
    rspl.append('cat2 = ready_jobs & submitted_jobs - started_jobs - running_jobs - catS - catE - catP\n')
    rspl.append('cat3 = ready_jobs & submitted_jobs & started_jobs & running_jobs - catS - catE - catP\n')
    rspl.append('cat4 = ready_jobs & submitted_jobs & started_jobs - running_jobs - catS - catE - catP\n')
    rspl.append('cat5 = finished_jobs & submitted_jobs & started_jobs - running_jobs - catS - catE - catP\n')
    rspl.append('other = jobs - cat1 - cat2 - cat3 - cat4 - cat5 - catS - catE - catP\n')
    rspl.append('\n')
    rspl.append('cat_dict = {}\n')
    rspl.append('cat_dict["1"] = cat1\n')
    rspl.append('cat_dict["2"] = cat2\n')
    rspl.append('cat_dict["3"] = cat3\n')
    rspl.append('cat_dict["4"] = cat4\n')
    rspl.append('cat_dict["5"] = cat5\n')
    rspl.append('cat_dict["S"] = catS\n')
    rspl.append('cat_dict["E"] = catE\n')
    rspl.append('cat_dict["P"] = catP\n')
    rspl.append('cat_dict["x"] = other\n')
    rspl.append('\n')
    rspl.append('arg_jobs = set([])\n')
    rspl.append('if arg_cats is not None:\n')
    rspl.append('    for cat in arg_cats:\n')
    rspl.append('        arg_jobs |= cat_dict[cat]\n')
    rspl.append('\n')
    rspl.append('if command == "help":\n')
    rspl.append('    print "Usage"\n')
    rspl.append('    print "---------------"\n')
    rspl.append('    print "python remote_submit.py show"\n')
    rspl.append('    print "python remote_submit.py submit X [Y] [Z] ..."\n')
    rspl.append('    print "python remote_submit.py package X [Y] [Z] ..."\n')
    rspl.append('    print "python remote_submit.py delete X [Y] [Z] ..."\n')
    rspl.append('    print "  where X [Y] [Z] are category labels as given below:"\n')
    rspl.append('elif command is None or command == "show":\n')
    rspl.append('    print "Summary of jobs"\n')
    rspl.append('    print "---------------"\n')
    rspl.append('    for cat_label in cat_dict:\n')
    rspl.append('        if cat_dict[cat_label]:\n')
    rspl.append('            print "(%s) %s:"%(cat_label, cat_description[cat_label])\n')
    rspl.append('            for job in cat_dict[cat_label]:\n')
    rspl.append('                if job in queued_jobs:\n')
    rspl.append('                   pbs_state = "Q"\n')
    rspl.append('                elif job in running_jobs:\n')
    rspl.append('                   pbs_state = "R"\n')
    rspl.append('                elif job in completed_jobs:\n')
    rspl.append('                   pbs_state = "C"\n')
    rspl.append('                elif job in held_jobs:\n')
    rspl.append('                   pbs_state = "H"\n')
    rspl.append('                else:\n')
    rspl.append('                   pbs_state = ""\n')
    rspl.append('                print "%s %s"%(job,pbs_state)\n')
    rspl.append('elif command == "submit":\n')
    rspl.append('    for job in arg_jobs:\n')
    rspl.append('        with job:\n')
    rspl.append('            with open("qsub.stdout","w") as stdout:\n')
    rspl.append('                call(["qsub","-d",job.workspace(),"run.pbs"],stdout=stdout)\n')
    rspl.append('            with open("qsub.stdout","r") as stdout:\n')
    rspl.append('                job.document["pbs id"] = str(int(stdout.readline().split(".")[0]))\n')
    rspl.append('                job.document["run_count submitted"] = job.document["run_count"]\n')
    rspl.append('elif command == "package":\n')
    rspl.append('    with tarfile.TarFile("finished.tar","w") as tar:\n')
    rspl.append('        for job in arg_jobs:\n')
    rspl.append('            tar.add(os.path.relpath(job.workspace()))\n')
    rspl.append('            job.document["stage"] = "packaged"\n')
    rspl.append('elif command == "delete":\n')
    rspl.append('    for job in arg_jobs:\n')
    rspl.append('        shutil.rmtree(job.workspace())\n')

    with open(path,'w') as file:
        file.writelines(rspl)

# check for submission tar file already in place
if len(sys.argv) > 1:
    if sys.argv[1] == 'help':
        print 'Description:'
        print '  Package all ready jobs for submission'
        print 'Usage:'
        print '  To package all ready jobs into ready.tar:'
        print '    `python package_remote_submit.py`'
        print '  To take jobs in stage submitted back to stage `ready`:'
        print '    `python package_remote_submit.py unsubmit-all`'
        sys.exit()
    elif sys.argv[1] == 'unsubmit-all':
        revert_jobs = [job for job in project.find_jobs() if 'stage' in job.document and job.document['stage'] == 'submitted']
        if revert_jobs:
            print 'Reverting jobs from submitted to ready:'
            for job in revert_jobs:
                job.document['stage'] = 'ready'
                print job.get_id()
        else:
            print 'No jobs to revert'
        sys.exit()
    else:
        sys.exit('Unrecognized argument %s'%sys.argv[1])

if os.path.isfile('remote_submit.py'):
    print 'I need remote_submit.py deleted but won\'t delete it myself'
    sys.exit()



jobs_to_package = [job for job in project.find_jobs() if 'stage' in job.document and job.document['stage'] == 'ready']
jobs_were_packaged = [job for job in project.find_jobs() if 'stage' in job.document and job.document['stage'] == 'submitted']

if jobs_were_packaged:
    print 'Found %d job(s) marked as packaged and submitted (but not finished):'%(len(jobs_were_packaged))
for job in jobs_were_packaged:
    print '{0}: {type}'.format(job.get_id(), **job.statepoint())

if os.path.isfile('ready.tar'):
    print 'ready.tar already exists. I won\'t overwrite or rename it'
    sys.exit()

if not jobs_to_package:
    print 'Found 0 jobs to package. Exiting...'
    sys.exit()
elif len(jobs_to_package) == 1:
    print 'Found 1 job to package:'
else:
    print 'Found %d jobs to package:'%(len(jobs_to_package))
for job in jobs_to_package:
    print '{0}: {type}'.format(job.get_id(), **job.statepoint())


with tarfile.TarFile('ready.tar','w') as tar:
    tar.add('signac.rc')
    write_remote_submit_py('remote_submit.py')
    tar.add('remote_submit.py')
    tar.add('setup_config_array.py')
    tar.add('identify_slab.py')
    tar.add('resources/')
    for job in jobs_to_package:
        make_jobscript(job)
        tar.add(os.path.relpath(job.workspace()))
        # change stage to submitted after archiving
for job in jobs_to_package:
    job.document['stage'] = 'submitted'
os.remove('remote_submit.py')
