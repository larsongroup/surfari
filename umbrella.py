import signac
import resources as res
import time
from setup_base import match, subdict
from setup_li import setup_li
from key_sets import check_statepoint
from PDBTools import pdbcat, NDX
import MDAnalysis as mda
import numpy as np
from topology import gmx_make_top
from mdp import Gromacs_mdp
from gmx_wrapper import Gromacs
from shutil import copyfile
from identify_slab import identify_slab
from solutes import process_statepoint_solutes
import os
import sys

project = signac.contrib.get_project()

def setup_umbrella(this_job,runtime,force=False,verbosity=1,num_threads=None,resume=False):
    """
        This sets up an umbrella sampling if dependencies are available.
        This function should be called /after/ verifying that the job is actually requested.
    """
    if ('stage' in this_job.document and this_job.document['stage'] == 'submitted'):
        print 'Job %s has stage submitted... won\'t continue'%this_job.get_id()
        return False

    print 'Setting up job %s'%(this_job.get_id())
    statepoint       = this_job.statepoint()
    check_statepoint(statepoint,'umbrella')
    gmx = Gromacs(verbosity=verbosity, ignore_pr_warning=True)
    z                = statepoint['z']

    try:
        solutes = process_statepoint_solutes(statepoint)
    except AssertionError:
        print 'Invalid solutes definition'
        return False

    # Check, create (if lacking), and load dependencies
    inherited_keys = ('N','xy_len','solvents','forcefield',\
                     'slab_thicknesses','surfactant')
    opt_keys = ('solutes',)

    cas = [ca_job for ca_job in project.find_jobs({'type': 'config array'})
            if 'stage' in ca_job.document
            and ca_job.document['stage'] == 'finished'
            and match(ca_job.statepoint(), statepoint, inherited_keys, opt_keys=opt_keys)
            and ca_job.document['z_min'] <= z
            and ca_job.document['z_max'] >= z]

    if len(cas) == 0:
        #ca_job = project.open_job(subdict(statepoint, inherited_keys))
        #with ca_job:
        #    setup_config_array(ca_job)
        print('No config array available for z=%f.'%(z))
        return False
    elif len(cas) > 1:
        print 'Multiple configuration arrays are usable for job %s'%(this_job.get_id())
        print 'Using the first returned by project.find_jobs()'
        ca_job = cas[0]
    else:
        ca_job = cas[0]

    # Dependencies exist, so set up the umbrella sampling
    solvents = statepoint['solvents']
    solventA = solvents[0]
    surfactant = statepoint['surfactant']
    T = statepoint['T']
    Pz = statepoint['Pz']
    ff = statepoint['forcefield']
    pull_k = statepoint['k']

    A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
    surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]

    z_A_COM = ca_job.document['z_A_COM']

    # Write mdp file from template
    mdp = Gromacs_mdp(ff=ff, type='umbrella')
    mdp['gen-temp'] = T
    mdp['ref-t'] = T
    mdp['ref-p'] = (1.0,Pz)
    mdp['nsteps'] = runtime/mdp['dt']
    mdp['pull_ngroups'] = 2
    mdp['pull_ncoords'] = 1
    mdp['pull_group1_name'] = 'pull_ref'
    mdp['pull_group2_name'] = 'pull_surf'
    mdp['pull_coord1_type'] = 'umbrella'
    mdp['pull_coord1_geometry'] = 'distance'
    mdp['pull_coord1_dim'] = 'N N Y'
    mdp['pull_coord1_start'] = 'no'
    mdp['pull_coord1_groups'] = '1 2'
    mdp['pull_coord1_rate'] = 0.
    mdp['pull_coord1_k'] = pull_k

    if ('stage' in this_job.document and this_job.document['stage'] == 'finished') or ('run_count' in this_job.document and this_job.document['run_count'] > 0):
        if resume:
            if this_job.document['stage'] == 'finished':
                run_count = this_job.document['run_count'] + 1
            elif this_job.document['stage'] in ('processing','ready'):
                run_count = this_job.document['run_count']
            else:
                raise Exception
            this_job.document['run_count'] = run_count
            this_job.document['stage'] = 'processing'

            total_t = 0.
            for i in range(run_count):
                tinit = float(gmx.dump_key('umbrella_%d.tpr'%(i),'tinit')[0])
                dt = float(gmx.dump_key('umbrella_%d.tpr'%(i),'dt')[0])
                nsteps = float(gmx.dump_key('umbrella_%d.tpr'%(i),'nsteps')[0])
                assert(np.isclose(total_t, tinit))
                total_t += nsteps*dt

            mdp['tinit'] = total_t
            mdp.write('umbrella_%d.mdp'%(run_count))

            gmx.grompp(('-f umbrella_%d.mdp -c umbrella_%d.tpr -t'
                        ' umbrella_%d.cpt -p umbrella.top -o umbrella_%d.tpr'
                        ' -n umbrella.ndx')%(run_count,run_count-1,run_count-1,run_count))
            mdrun_args = '-deffnm umbrella_%d -px pullx-umbrella_%d.xvg -cpi'%(run_count, run_count)

        else:
            print 'job %s is already finished or its run_count is greater than 0, and resume==False. skipping setup.'%this_job.get_id()
            return False
    else:
        # Get my bearings, load configuration array, select frame
        # (this is slow! would be better to break conf. array into frames
        # just once)
        run_count = 0
        this_job.document['stage'] = 'processing'
        z1,z2,w,_ = identify_slab(ca_job.workspace()+'/pull_in.tpr',ca_job.workspace()+'/config_array.xtc',A_sel_str)
        u = mda.Universe(ca_job.workspace()+'/pull_in.tpr',
                         ca_job.workspace()+'/config_array.xtc')
        surf = u.select_atoms(surf_sel_str)
        ndx = NDX(ca_job.workspace()+'/pull_in.tpr','umbrella.ndx')
        assert(os.path.isfile('umbrella.ndx')), 'umbrella.ndx doesn\'t exist'
        ndx.AppendMDASelectionStr(A_sel_str,'pull_ref')
        resid = surf.resids[0]
        pull_surf_sel_str = surf_sel_str+' and resid %d'%resid
        ndx.AppendMDASelectionStr(pull_surf_sel_str,'pull_surf')
        pull_surf = u.select_atoms(pull_surf_sel_str)
        pull_ref = u.select_atoms(A_sel_str)
        closest_i = None
        smallest_diff = 1e999
        for ts in u.trajectory:
            mda.lib.mdamath.make_whole(pull_surf)
            z_compare = pull_surf.center_of_mass()[2] - z2
            if abs(z_compare - 10.*z) < smallest_diff:
                smallest_diff = abs(z_compare - 10.*z) 
                closest_frame = ts.frame
        for ts in u.trajectory:
            if ts.frame < closest_frame:
                continue
            else:
                break
        with mda.Writer('start.pdb') as W:
            W.write(u.atoms)

        mdp['pull_coord1_init'] = z-z_A_COM
        mdp.write('umbrella_%d.mdp'%(run_count))

        # Write top file for this ff and box
        with open('umbrella.top','w') as top:
            top.write(gmx_make_top(ff,'start.pdb'))

        gmx.grompp('-f umbrella_%d.mdp -c start.pdb -p umbrella.top -o umbrella_%d.tpr -n umbrella.ndx'%(run_count,run_count))
        mdrun_args = '-deffnm umbrella_%d -px pullx-umbrella_%d.xvg -cpi'%(run_count, run_count)
    #
    #########################################

    # Clean up
    this_job.document['grompp time %d'%run_count] = time.ctime()
    this_job.document['job_type'] = 'setup_run_x'
    this_job.document['run_count'] = run_count
    this_job.document['run_type'] = 'mdrun'
    this_job.document['mdrun_args'] = mdrun_args
    try:
        del(this_job.document['job id'])
    except:
        pass
    this_job.document['stage'] = 'ready'

def run_umbrella(job,force=False,num_threads=None, verbosity=0):
    """
        Run an umbrella job
    """
    # Probably redundant check that job hasn't already been computed
    if not force and 'stage' in job.document and job.document['stage'] == 'finished':
        print 'job {} is already finished. skipping setup.'.format(job.get_id())
        return False

    print 'Running job %s'%(job.get_id())
    
    gmx = Gromacs(verbosity=verbosity, ignore_pr_warning=True)
    run_count = job.document['run_count']
    mdrun_args = job.document['mdrun_args']

    # Run umbrella
    if num_threads is not None:
        print(mdrun_args)
        gmx.mdrun(mdrun_args+' -nt {:d}'.format(num_threads))
    else:
        print(mdrun_args)
        gmx.mdrun(mdrun_args)

    # Wait for gromacs to finish running and return error code
    while gmx.poll() is None:
        time.sleep(1)

    if gmx.poll() != 0:
        raise Exception('Gromacs exited with code {}'.format(gmx.poll()))

    job.document['stage'] = 'finished'
