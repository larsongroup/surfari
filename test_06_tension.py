# coding: utf-8
import signac
from tension import setup_tension
import os
from glob import glob

project = signac.contrib.get_project()

def test_setup_tension_DM_c16e8_hexadecane_water():
    sp = {'type':'tension','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini','slab_thicknesses':[10,10],'solvents':['hexadecane',None],'surfactant':'c16e8','xy_len':6}
    with project.open_job(sp) as job:
        try:
            setup_tension(job,1000,verbosity=1)
            assert(False)
        except AssertionError:
            pass
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'ready')

def test_setup_tension_DM_c16e8_hexadecane_water_solutes():
    sp = {'type':'tension','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini',\
            'slab_thicknesses':[10,10],'solvents':['hexadecane',None],\
            'surfactant':'c16e8','xy_len':6,\
            'solutes':[[['dodecane'],[1]],[['octane'],[3]]]}
    project = signac.contrib.get_project()
    with project.open_job(sp) as job:
        try:
            setup_tension(job,1000,verbosity=1)
            assert(False)
        except AssertionError:
            pass
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'ready')

def test_setup_tension_DM_c16e8_hexadecane_water_solutes_None():
    sp = {'type':'tension','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini',\
            'slab_thicknesses':[10,10],'solvents':['hexadecane',None],\
            'surfactant':'c16e8','xy_len':6,\
            'solutes':None}
    project = signac.contrib.get_project()
    with project.open_job(sp) as job:
        assert(not setup_tension(job,100))
        assert(not os.path.isfile('signac_job_document.json'))
        assert(glob('*') == ['signac_statepoint.json'] or not glob('*'))

def test_setup_tension_DM_c16e8_hexadecane_water_solutes_None2():
    sp = {'type':'tension','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini',\
            'slab_thicknesses':[10,10],'solvents':['hexadecane',None],\
            'surfactant':'c16e8','xy_len':6,\
            'solutes':[None,None]}
    project = signac.contrib.get_project()
    with project.open_job(sp) as job:
        assert(not setup_tension(job,100))
        assert(not os.path.isfile('signac_job_document.json'))
        assert(glob('*') == ['signac_statepoint.json'] or not glob('*'))

def test_setup_tension_DM_c16e8_hexadecane_water_solutes_None3():
    sp = {'type':'tension','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini',\
            'slab_thicknesses':[10,10],'solvents':['hexadecane',None],\
            'surfactant':'c16e8','xy_len':6,\
            'solutes':['dodecane',None,None]}
    project = signac.contrib.get_project()
    with project.open_job(sp) as job:
        assert(not setup_tension(job,100))
        assert(not os.path.isfile('signac_job_document.json'))
        assert(glob('*') == ['signac_statepoint.json'] or not glob('*'))
