"""
    Slab squeezing:
        There are at least two useful pieces of information for judging
        the progress of a slab squeezing. One is the density profile and
        another is the Lz of the box. The simplest scheme for controlling
        slab squeezing would be to only heed Lz and use no other information.
        Unfortunately, this is not sufficient, because non-uniform density
        profiles can develop which show relatively little change in Lz.

        Look for correlated density fluctuations, with bin size so that at least
        60 atoms are in each bin (on average). Checks for correlation just by
        looking at adjacent bin and seeing if correlation > 0.3.

        If not correlated, and if Lz is not changing by much, then we can
        stop squeezing.

        TODO
        Also look at xyz bins. If any cubic bin sized such that on average
        there should be 6 molecules inside happens to have zero molecules inside,
        that's a problem.

        TODO
        If there is a z slice without any atoms, cut it out, minus a tol sliver
"""
from PackmolWrapper import PackmolInput
import gmx_wrapper
from topology import gmx_make_top
from mdp import Gromacs_mdp
import resources as res
from key_sets import check_statepoint
from subprocess import Popen, PIPE
from shutil import copyfile
import time
import MDAnalysis as mda
import numpy as np
from glob import glob
import os
import signac
from squeezing import squeeze
import time
project = signac.contrib.get_project()

def setup_surfactant_slab(this_job,force=False,num_threads=None,verbosity=0):
    """
        This sets up a slab if dependencies are available.
        This function should be called /after/ verifying that the job is actually requested.
        Full squeezing trajectory is saved to full_squeeze.xtc
        TODO add/remove molecules to get Lz precisely right
    """
    starttime = time.time()
    print 'Setting up job %s'%(this_job.get_id())
    statepoint = this_job.statepoint()
    check_statepoint(statepoint,'surfactant slab')
    # Probably redundant check that job hasn't already been computed
    if not force and 'stage' in this_job.document and this_job.document['stage'] == 'finished':
        print 'job %s is already finished. skipping setup.'%(this_job.get_id())
        return False

    #########################################
    # Dependencies exist, so set up the slab
    ff             = statepoint['forcefield']
    molecule       = statepoint['solvent']
    surfactant     = statepoint['surfactant']
    N              = statepoint['N']
    xy_len         = statepoint['xy_len']
    packmol_tol    = res.packmol_tols[res.ff_scale[ff]]
    box_buffer     = packmol_tol/2.
    number_density = res.density[ff][statepoint['solvent']]
    molecule_path  = project.root_directory()+'/resources/' + res.ff_molecule_pdb[ff][molecule]
    if surfactant is not None:
        surfactant_path  = project.root_directory()+'/resources/' + res.ff_molecule_pdb[ff][surfactant]
    slab_thickness = statepoint['slab_thickness']
    T0             = statepoint['T']
    Pz0            = statepoint['Pz']
    sparse_factor  = 1.5

    molecule_pdb = molecule_path.strip('/').split('/')[-1]
    if surfactant is not None:
        surfactant_pdb = surfactant_path.strip('/').split('/')[-1]
    box_size = [xy_len,xy_len,slab_thickness]

    assert(Gromacs_mdp(ff=ff,type='em'))
    assert(Gromacs_mdp(ff=ff,type='squeeze'))
    assert(Gromacs_mdp(ff=ff,type='eq'))

    gmx = gmx_wrapper.Gromacs(verbosity=verbosity,
                              ignore_pme_warning=True,
                              ignore_cg_radii_warning=True)

    copyfile(molecule_path,molecule_pdb)
    copyfile(surfactant_path,surfactant_pdb)

    if surfactant is None:
        assert(N is None)
    elif N is None:
        assert(surfactant is None)
    elif N is 0:
        raise ValueError('N cannot be 0')
    elif surfactant is not None:
        assert(N > 0)
    elif N > 0:
        assert(surfactant is not None)

    # pack a box with molecules sparsely, using half the estimated
    # number density (estimate comes from resources file)
    packmol = PackmolInput(packmol_tol,'pdb','sparse_slab.pdb',2*box_buffer)
    number = (box_size[0]*box_size[1]*slab_thickness)*(number_density)
    packmol.addStructure(molecule_pdb, number)
    packmol.addConstraintInsideBox(box_buffer, box_buffer, box_buffer,
                                   10.*box_size[0]-box_buffer,
                                   10.*box_size[1]-box_buffer,
                                   10.*sparse_factor*slab_thickness-box_buffer)
    if surfactant is not None:
        packmol.addStructure(surfactant_pdb, N)
        packmol.addConstraintInsideBox(box_buffer, box_buffer, box_buffer,
                                       10.*box_size[0]-box_buffer,
                                       10.*box_size[1]-box_buffer,
                                       10.*sparse_factor*slab_thickness-box_buffer)
    packmol.run()

    with open('slab.top','w') as top:
        top.write(gmx_make_top(ff,'sparse_slab.pdb'))

    # Energy minimize
    mdp = Gromacs_mdp(ff=ff,type='em')
    mdp.write('em.mdp')

    gmx.grompp('-f em.mdp -c sparse_slab.pdb -p slab.top -o slab_em.tpr')
    if num_threads:
        gmx.mdrun('-deffnm slab_em -c slab_squeeze0.gro -nt %d'%(num_threads))
    else:
        gmx.mdrun('-deffnm slab_em -c slab_squeeze0.gro')

    # Squeeze the slab
    squeeze('slab', statepoint, check_corr=True, verbosity=verbosity,
            num_threads=num_threads)

    # Combine squeezes into one trajectory
    gmx.trjcat('-f {} -cat -o full_squeeze.xtc'.format(' '.join(glob('slab_squeeze*xtc'))))

    # Equilibrate (Berendsen)
    # TODO: consider relaxation of internal molecular distances
    # is autocorrelation time increasing with time, or has it become steady?
    # Note that this won't work for glasses, so there should be an option
    # to disable this, or probably choose different slab setup methods.
    # this would be noted in the job document.
    # Rename T to T_squeeze and Pz to Pz_squeeze and reserve T,Pz for the
    # statepoint quantities
    Pz = Pz0*1.
    T = T0*1.
    mdp = Gromacs_mdp(ff=ff,type='eq')
    mdp.add_params({'ref-p':(1.0,Pz)})
    mdp.add_params({'ref-t':T})
    mdp.add_params({'nsteps':50000})
    mdp.write('eq.mdp')

    gmx.grompp('-f eq.mdp -c slab_squeeze{:d}.gro -p slab.top -o slab_eq.tpr'.format(
                                                    len(glob('slab_squeeze*gro'))-1))
    if num_threads:
        gmx.mdrun('-deffnm slab_eq -nt {:d}'.format(num_threads))
    else:
        gmx.mdrun('-deffnm slab_eq')

    # Combine all setup into one trajectory
    gmx.trjcat('-f {} slab_eq.xtc -cat -o full.xtc'.format(' '.join(glob('slab_squeeze*xtc'))))
    # Discard extra trajectories
    for xtc in glob('slab_squeeze*xtc'):
        os.remove(xtc)
    os.remove('slab_eq.xtc')
    #
    #########################################

    # Clean up
    this_job.document['stage'] = 'finished'
    this_job.document['time'] = time.ctime()
    this_job.document['sparse_factor'] = sparse_factor
    this_job.document['packmol_tol'] = packmol_tol
    this_job.document['wall time'] = time.time() - starttime
