import signac
from gmx_wrapper import Gromacs
from math import ceil, floor, sqrt, exp
import numpy as np
from scipy import signal, stats
import pandas as pd
from identify_slab import identify_slab, TanhProfile
import resources as res
import MDAnalysis as mda
import os
from glob import glob
from setup_base import subdict
from shutil import copyfile
import pickle

project = signac.contrib.get_project()

tension_compute_attrs = ['st_avg', 'st_stderr']


gmx = Gromacs(verbosity=1)

def autocorr(t,x):
    """
        Calculates normalized autocorrelation and returns twice
        the first time at which the normalized autocorrelation drops below
        e^-1.
    """
    xd = x-np.mean(x)
    data_length = len(xd)
    b = np.zeros(data_length * 2)
    b[data_length/2:data_length/2+data_length] = xd
    ac = signal.fftconvolve(b, xd[::-1], mode='valid')
    acn = ac[len(ac)/2::]/np.max(ac)
    t = t - min(t)
    if acn[1] < 0.135:
        return t[1], acn
    return 2*t[acn<0.367879][0], acn


def block_avg(t,x):
    lagts = []
    stderrs = []
    for i in range(1,len(x)+1):
        floor(len(x)/i)
        len_y = int(floor(len(x)/i))
        if len_y < 3:
            break
        if (len(x)-i*len_y) > 0:
            blocks = x[:-(len(x)-i*len_y)].reshape(i,len_y)
            t_blocks = t[:-(len(x)-i*len_y)].reshape(i,len_y)
        else:
            blocks = x.reshape(i,len_y)
            t_blocks = t.reshape(i,len_y)
        avgs = np.mean(blocks,axis=1)
        stderrs.append(np.std(avgs)/sqrt(len(avgs)))
        lagts.append(max(t_blocks[0])-min(t_blocks[0]))
    return lagts,stderrs


def surf_is_adsorbed(top, traj, A_sel_str, surf_sel_str):
    u = mda.Universe(top, traj)
    surf = u.select_atoms(surf_sel_str)
    z1, z2, w, _ = identify_slab(top, traj, A_sel_str)
    print('z1: {}, z2: {}, w: {}'.format(z1, z2, w))
    for ts in u.trajectory:
        surf.pack_into_box()
        ads1 = surf.select_atoms('prop z > %f and prop z < %f'%(z1-3*w,z1+3*w)).residues
        ads2 = surf.select_atoms('prop z > %f and prop z < %f'%(z2-3*w,z2+3*w)).residues
        if len(ads1 + ads2) < len(surf.residues):
            return False
    return True


def outer_slab_discrepancy(top, traj, slab_sel_str, frame=None):
    """Normalized measure of integrated discrepancy between slab density
    profile and fitted model tanh profile. The discrepancy is normalized by
    the density, so it has units of Angstroms (mdanalysis default).
    """
    u = mda.Universe(top, traj)
    slab = u.select_atoms(slab_sel_str)
    bins = 200
    if frame is not None:
        u.trajectory[frame]
    Lx, Ly, Lz, _, _, _ = u.dimensions
    hist, z_edges = np.histogram(slab.positions[:,2],
                                 weights=slab.masses/(Lx*Ly*Lz/bins),
                                 bins=bins,
                                 range=(0, Lz))
    z = (z_edges[:-1] + z_edges[1:])/2.
    z1, z2, w, rho = identify_slab(top, traj, 'resname SOL', frame=frame)
    diff = hist/rho - TanhProfile(z1, z2-z1, w, rho, z_edges)/rho

    m = (z1+z2)/2.

    masks = []
    if z2 < z1:
        masks.append((z >= m) & (z <= z1))
        masks.append( (z >= z2) & (z <= m))
    elif m > Lz/2.:
        # NOTE can't use | operator to get mask for integration
        masks.append((z >= m - Lz/2.) & (z <= z1))
        masks.append((z >= z2) & (z <= Lz))
        masks.append((z >= 0) & (z <= m - Lz/2.))
    elif m <= Lz/2.:
        masks.append((z >= z2) & (z <= m + Lz/2.))
        masks.append((z >= m + Lz/2.) & (z <= Lz))
        masks.append((z >= 0.) & (z <= z1))
    else:
        raise Exception('Unexpected z1, z2')

    return np.sum(np.trapz(diff[mask], z[mask]) for mask in masks)


def solvent_is_leaking(top, traj, slab_sel_str, frame=None):
    discrepancy = outer_slab_discrepancy(top, traj, slab_sel_str, frame)
    if discrepancy > 1.:
        return True
    else:
        return False


def compute_tension(job):
    print 'Computing tension for job %s:'%job.get_id()
    print job.statepoint()
    statepoint = job.statepoint()
    solventA = statepoint['solvents'][0]
    A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
    if 'surfactant' in statepoint and statepoint['surfactant'] is not None:
        surfactant = statepoint['surfactant']
        surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]
    else:
        surfactant = None
    with job:
        if 'begt' in job.document:
            #begt = job.document['begt']
            begt = 0
        else:
            begt = 0
        run_count = job.document['run_count']
        run_count += (job.document['stage'] == 'finished' or job.document['stage'] == 'stopped')
        thinned = []
        for i in range(run_count):
            # Verifying surfactant adsorption
            if 'surfactant' in statepoint:
                print 'Checking for desorbed surfactant in job %s'%(job.get_id())
                if(surfactant is not None
                   and not surf_is_adsorbed('NAPzT_%d.tpr'%i, 'NAPzT_%d.xtc'%i,
                                             A_sel_str,surf_sel_str)):
                    print 'Surfactant desorbed in job %s'%(job.get_id())
                    job.document['desorbed']=True
                else:
                    job.document['desorbed']=False

            gmx.energy('-f NAPzT_%d.edr -b %f -o tension%d -xvg none'%(i,begt,i),
                    stdin_text='#\n')
            raw = pd.read_csv('tension%d.xvg'%(i),names=['t','st_raw'],
                                sep=' ',skipinitialspace=True)
            tau,acn = autocorr(raw.t.values,raw.st_raw.values)
            lagt,stderr = block_avg(raw.t.values,raw.st_raw.values)
            dt = raw.t[1]-raw.t[0]
            if tau == dt:
                print 'WARNING: autocorrelation time appears to be less than 1 frame'
            thinned.append(raw.iloc()[::ceil(tau/dt)])

        if 'st_avg' in job.document:
            del job.document['st_avg']
        if 'st_stderr' in job.document:
            del job.document['st_stderr']

        if not thinned:
            return
        for i in range(len(thinned)):
            try:
                y = y.append(thinned[i])
            except NameError:
                y = thinned[i]
        begi = int(np.arange(len(y.t))[y.t.values>=begt][0])
        y = y.iloc()[begi:] 
        y['st'] = y['st_raw']/20.
        doc = job.document
        with open('t_st.csv', 'w') as pck:
            pickle.dump((y.t.values, y.st.values), pck)
        doc['st_avg'] = np.mean(y.st.values)
        doc['st_stderr'] = np.std(y.st.values)/np.sqrt(len(y.st))
        for attr in tension_compute_attrs:
            assert(attr in doc)

    job.document.update(doc)


def read_bootstraps(path):
    profiles = []
    profile = []
    with open(path) as f:
        for line in f.readlines():
            if not line.strip():
                profiles.append(profile)
                profile = []
            elif line.strip()[0] == '&':
                profiles.append(profile)
                profile = []
            elif line.strip()[0] == '#' or line.strip()[0] == '@':
                pass
            else:
                words = line.strip().split()
                profile.append(float(words[1]))
    return np.array(profiles)


def compute_tensions(compute_all=False):
    # TODO verify that it works correctly for run_count > 0
    # TODO check for interfacial rupture ... poor fit w/ identify_slab?
    # TODO check if already computed... add job doc flag. allow force recompute with `redo` keyword arg = True
    # for each tension job, compute surface pressure over time

    # find finished tension jobs
    if compute_all:
        jobs = [job for job in project.find_jobs({'type':'tension'}) if
                'stage' in job.document and (job.document['stage'] == 'finished' or
                job.document['stage'] == 'stopped' or job.document['run_count'] > 0)]
    else:
        jobs = [job for job in project.find_jobs({'type':'tension'}) if
                'stage' in job.document and (job.document['stage'] == 'finished' or
                job.document['stage'] == 'stopped' or job.document['run_count'] > 0)
                and any(attr not in job.document for attr in tension_compute_attrs)]
    for job in jobs:
        compute_tension(job)


def xvg_spring_iter(xvgs):
    """ Given the list of xvg files from the data set, read the spring
    constants and positions from the corresponding tpr files and yield back
    xvg, k, x0.
    """
    for xvg in xvgs:
        tpr = xvg.replace('pullx-', '').replace('.xvg', '.tpr')
        k = float(gmx.dump_key(tpr, 'k')[0])/2.49434
        x0 = float(gmx.dump_key(tpr, 'init')[0])
        yield xvg, k, x0


def inconsistency_ZhuHummer2012(xvgs):
    """Compute inconsistency parameter for each pair of consecutive windows
    and return the r* and y(r*) where r* is the midpoint of the consecutive
    windows, and y(r*) is the inconsistency parameter at that midpoint.

    Parameters
    ----------
    xvgs : list
        List of xvg filepaths

    Returns
    -------
    x0m : np.array
        Array of midpoints of consecutive windows

    y : np.array
        Array of inconsistency parameters
    
    Operations
    ----------
    Order the windows by x0. Take midpoints of consecutive windows as
    r*, take highest-biased Boltzmann sample 100 times, all others multiply
    by 100xBoltzmann factor difference between their energy and the ref.
    (highest sample) energy.

    """
    k = []
    x0 = []
    N = []
    dfs = []
    for xvg, k_, x0_ in xvg_spring_iter(xvgs):
        # remove lines starting with # or @
        data = pd.read_csv(xvg, sep='\t', names=['t', 'x'], comment='#')
        try:
            data = data[~data['t'].str.contains('@')]
        except AttributeError:
            pass
        k.append(k_)
        x0.append(x0_)
        N.append(len(data['x']))
        data['V'] = k_/2.*(data['x'] - x0_)**2.
        dfs.append(data)
    df = pd.concat(dfs, keys=xvgs)

    sorted_indices = np.argsort(x0)
    k = np.array(k)[sorted_indices]
    x0 = np.array(x0)[sorted_indices]
    N = np.array(N)[sorted_indices]
    xvgs = np.array(xvgs)[sorted_indices]

    y = []
    x0m_list = []
    for i in range(len(xvgs)-1):
        k1 = k[i]
        k2 = k[i+1]
        km = (k1+k2)/2.
        x01 = x0[i]
        x02 = x0[i+1]
        x0m = (x01+x02)/2.
        x0m_list.append(x0m)
        x1 = df.xs(xvgs[i])['x']
        x2 = df.xs(xvgs[i+1])['x']
        v1 = df.xs(xvgs[i])['V']
        v2 = df.xs(xvgs[i+1])['V']
        n1 = N[i]
        n2 = N[i+1]
        x1new = []
        x2new = []
        mults1 = np.array(np.round(100.*np.exp(-km/2.*(x1-x0m)**2. + v1)), dtype=np.int64)
        mults2 = np.array(np.round(100.*np.exp(-km/2.*(x2-x0m)**2. + v2)), dtype=np.int64)
        for x, mult in zip(x1, mults1):
            x1new.extend([x]*mult)
        for x, mult in zip(x2, mults2):
            x2new.extend([x]*mult)
        n1 = N[i]
        n2 = N[i+1]
        y.append(sqrt(n1*n2/(n1+n2))*stats.ks_2samp(x1new, x2new).statistic)

    return np.array(x0m_list), np.array(y)


def compute_pmf(job):
    with job:
        begt = 1000
        if os.path.isfile('pullx-files.dat'):
            os.remove('pullx-files.dat')
        if os.path.isfile('tpr-files.dat'):
            os.remove('tpr-files.dat')
        xvgs = glob('pullx-umbrella*xvg')
        for pullx in xvgs:
            tpr = pullx.replace('.xvg','.tpr').replace('pullx-','')
            if not os.path.isfile(tpr):
                print 'Couldn\'t find tpr file %s for pullx file %s'%(tpr,pullx)
                continue
            with open(pullx) as f:
                data_count = 0
                for line in f.readlines():
                    if line[0] != '#' and line[0] != '@':
                        data_count += 1
                    if data_count > 30:
                        break
                if data_count <= 30:
                    print('Insufficient data to wham file {}'.format(pullx))
                    continue

            with open('pullx-files.dat','a') as file:
                file.write('%s\n'%(pullx))
            with open('tpr-files.dat','a') as file:
                file.write('%s\n'%(tpr))

        if os.path.isfile('pullx-files.dat'):
            gmx.wham('-it tpr-files.dat -ix pullx-files.dat -xvg none -o'
                     ' -hist -unit kCal -b {begt:f} -nBootstrap {n:d} -ac'
                     ' -bsprof bsProfs.xvg -bs-method traj'.format(
                         begt=begt,
                         n=100))
            profile = pd.read_csv('profile.xvg',skipinitialspace=True,sep='\t',names=['z','W'])
            bootstraps = read_bootstraps('bsProfs.xvg')
            offsets = bootstraps[:, -10]
            bootstraps = (bootstraps.T - offsets)
            p5 = np.percentile(bootstraps, 5, axis=1)
            p95 = np.percentile(bootstraps, 95, axis=1)
            p50 = np.percentile(bootstraps, 50, axis=1)
            job.document['z'] = profile.z.values
            kT = 0.0019872036*job.statepoint()['T']
            job.document['W'] = p50/kT
            job.document['dW5'] = (p5-p50)/kT
            job.document['dW95'] = (p95-p50)/kT
        job.document['stage'] = 'computed'


def compute_Hummer(job):
    lag = job.statepoint()['lag']
    nbins = job.statepoint()['nbins']
    zmin = job.statepoint()['zmin']
    zmax = job.statepoint()['zmax']
    with job:
        # based on pullx data, calculate transition matrices
        # calculate dV for bins
        for pullx in glob('pullx-umbrella*xvg'):
            jid = pullx.split('_')[2].replace('.tpr', '')
            umb_job = project.open_job(id=jid)
            k = umb_job.statepoint()['k']
            z = umb_job.statepoint()['z']


def compute_Hummers():
    # find finished umbrella jobs
    finished_umb = [job for job in project.find_jobs({'type':'umbrella'})
                    if 'stage' in job.document and
                    (job.document['stage'] == 'finished' or
                     ('run_count' in job.document and
                       job.document['run_count'] > 0))]

    for job in finished_umb:
        k = job.statepoint()['k']
        z = job.statepoint()['z']
        jid = job.get_id()

        pmf_keys = list(set(job.statepoint().keys()) - set(['k','z']))
        pmf_statepoint = subdict(job.statepoint(), pmf_keys)
        pmf_statepoint['type'] = 'hummer'
        with project.open_job(pmf_statepoint) as pmf_job:
            consts = pmf_job.document['consts']
            pass


        surfactant = statepoint['surfactant']
        surf_sel_str = 'resname '+res.resname_of_molecule[surfactant]
        surf = u.select_atoms(surf_sel_str)
        resid1 = list(set(surf.resids))[0]
        resid2 = list(set(surf.resids))[1]
        surf1_sel_str = surf_sel_str+' and resid %d'%resid1
        surf2_sel_str = surf_sel_str+' and resid %d'%resid2
        surf1 = u.select_atoms(surf1_sel_str)
        surf2 = u.select_atoms(surf2_sel_str)
        times = []
        rs = []
        for ts in u.trajectory:
            mda.lib.mdamath.make_whole(surf1)
            mda.lib.mdamath.make_whole(surf2)
            dp = ((surf1.center_of_mass() - surf2.center_of_mass())/u.dimensions[:3]) % 1.
            dsq = ((-1.*(dp > 0.5) + 1.*(dp < -0.5) + dp) * u.dimensions[:3])**2.
            r = sqrt(np.sum(dsq))
            times.append(ts.time)
            rs.append(r)


def generate_xvg_from_xtc(tpr, xtc, xvg, job):
    statepoint = job.statepoint()
    u = mda.Universe(tpr, xtc)
    surfactant = statepoint['surfactant']
    rs = []
    times = []
    if statepoint['type'] == 'umbrella':
        solventA = statepoint['solvents'][0]
        A_sel_str = 'resname {}'.format(res.resname_of_molecule[solventA])
        surf = u.select_atoms('resname {}'.format(res.resname_of_molecule[surfactant]))
        pull_surf = surf.select_atoms('resid {}'.format(surf.resids[0]))
        pull_ref = u.select_atoms(A_sel_str)
        for ts in u.trajectory:
            times.append(ts.time)
            rs.append((pull_surf.center_of_mass()[2] - pull_ref.center_of_mass()[2])/10.)
    elif statepoint['type'] == '2surf umbrella':
        surf_sel_str = 'resname '+res.resname_of_molecule[surfactant]
        surf = u.select_atoms(surf_sel_str)
        resid1 = list(set(surf.resids))[0]
        resid2 = list(set(surf.resids))[1]
        surf1_sel_str = surf_sel_str+' and resid %d'%resid1
        surf2_sel_str = surf_sel_str+' and resid %d'%resid2
        surf1 = u.select_atoms(surf1_sel_str)
        surf2 = u.select_atoms(surf2_sel_str)
        for ts in u.trajectory:
            mda.lib.mdamath.make_whole(surf1)
            mda.lib.mdamath.make_whole(surf2)
            dp = ((surf1.center_of_mass() - surf2.center_of_mass())/u.dimensions[:3]) % 1.
            dsq = ((-1.*(dp > 0.5) + 1.*(dp < -0.5) + dp) * u.dimensions[:3])**2.
            r = sqrt(np.sum(dsq))
            times.append(ts.time)
            rs.append(r)

    with open('pullx-umbrella_{}.xvg'.format(i),'w') as file:
        for t, r in zip(times, rs):
            file.write('%.4f\t%.4f\n'%(t, r))


def compute_pmfs(umb_filter=None):
    # TODO check adsorption
    # TODO error analysis
    raw_input('A friendly reminder to set OMP_NUM_THREADS to a sufficiently '
              'high number for gmx wham to exploit. Press ENTER to continue.')

    # find finished umbrella jobs
    if umb_filter is None:
        umb_filter = {}
    finished_umbrella_jobs = [job for job in project.find_jobs(umb_filter) if
            job.statepoint()['type'] in ('umbrella', '2surf umbrella') and
            'stage' in job.document and (job.document['stage'] == 'finished' or
            ('run_count' in job.document and job.document['run_count'] > 0))]

    # collect umbrellas into one pmf statepoint
    print('Scanning finished umbrella jobs.')
    for job in finished_umbrella_jobs:
        pmf_keys = list(set(job.statepoint().keys()) - set(['k','z','r']))
        pmf_statepoint = subdict(job.statepoint(), pmf_keys)
        pmf_statepoint['type'] = 'pmf'
        with project.open_job(pmf_statepoint) as pmf_job:
            pass
        umbrella_folder = job.workspace()+'/'
        pmf_folder = pmf_job.workspace()+'/'
        with job:
            for i in range(job.document['run_count'] + (job.document['stage'] == 'finished')):
                tpr = 'umbrella_{}.tpr'.format(i)
                if not os.path.isfile(tpr):
                    raise IOError('File {0} does not exist for job {1}'.format(
                                    tpr, job.get_id()))
                       
                xvg = 'pullx-umbrella_{}.xvg'.format(i)
                xtc = 'umbrella_{}.xtc'.format(i)
                if not os.path.isfile(xvg):
                    if os.path.isfile(xtc):
                        generate_xvg_from_xtc(tpr, xtc, xvg, job)

                copyfile(xvg, pmf_job.workspace()+'/pullx-umbrella_%d_%s.xvg'%(i, job.get_id()))
                copyfile(tpr, pmf_job.workspace()+'/umbrella_%d_%s.tpr'%(i, job.get_id()))

    # find pmf jobs
    umb_filter['type'] = 'pmf'
    pmf_jobs = [job for job in project.find_jobs(umb_filter)]
    print('Will compute these pmf jobs:')
    for job in pmf_jobs:
        if 'N' in job.statepoint():
            print('{0}: {surfactant} {N}'.format(job.get_id(), **job.statepoint()))
        else:
            print('{0}: {surfactant}'.format(job.get_id(), **job.statepoint()))
    for job in pmf_jobs:
        print('Computing pmf and bootstrap uncertainty for job id {}.'.format(
            job.get_id()))
        compute_pmf(job)
