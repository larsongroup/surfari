"""
    Gate metering:

    Statepoint quantities:
    - Harmonic bias center and spring constant
    - Kill points 1 and 2

    Need to measure:
    - Orientational autocorrelation time of free surfactant
    - Relative z position autocorrelation of adsorbed surfactant

    Record both in job document. Whichever is longer, draw surfactant
    configuration samples after that wait. Record hard positions of kill points.

    Load sampled surfactant configurations and run until surfactant passes
    a kill point, according to plumed.
"""
import signac
import resources as res
import time
import gmx_wrapper
import MDAnalysis as mda
import numpy as np
from scipy import signal
from identify_slab import identify_slab
from shutil import copyfile
from operator import itemgetter
from itertools import groupby
import os
import logging

logger = logging.getLogger(__name__)

project = signac.contrib.get_project()


def vector_autocorr(x):
    xd = np.array(x)-np.mean(x, axis=0)
    data_length = len(xd)
    data_shape = xd.shape
    b = np.zeros(np.array(data_shape)*[2, 1])
    b[data_length/2:data_length/2+data_length] = xd
    ac0 = signal.fftconvolve(b[:, 0], xd[::-1, 0], mode='valid')
    ac1 = signal.fftconvolve(b[:, 1], xd[::-1, 1], mode='valid')
    ac2 = signal.fftconvolve(b[:, 2], xd[::-1, 2], mode='valid')
    acn = ((ac0+ac1+ac2)/np.sum(xd**2.))[data_length/2:]
    if acn[1] < 0.135335:
        tau = 1
    else:
        tau = 2*int(np.arange(len(acn))[acn < 0.367879][0])
    return tau


def unnormalized_autocorrelation(x):
    """Compute the unnormalized autocorrelation as an integer index
    using scipy.signal.fftconvolve assuming a single exponential
    decay of the vector dot product.

    Parameters
    ----------
    x : array of floats
        eries of values equally spaced in time

    Returns
    -------
    tau : float
        first index at which the normalized autocorrelation drops below e^-1
    """
    xd = x - np.mean(x, axis=0)
    data_length = len(xd)
    b = np.zeros(data_length * 2)
    b[data_length/2:data_length/2+data_length] = xd
    ac = signal.fftconvolve(b, xd[::-1], mode='valid')
    return ac


def setup_ads(this_job,num_runs=10,num_threads=None,force=False,verbosity=0):
    """
        This sets up a configuration array if dependencies are available.
        This function should be called /after/ verifying that the job is actually requested.
    """
    from setup_base import match, subdict
    from setup_li import setup_li
    from key_sets import check_statepoint
    from PDBTools import pdbcat, NDX
    from topology import gmx_make_top
    from mdp import Gromacs_mdp

    print 'Setting up job %s'%(this_job.get_id())
    statepoint = this_job.statepoint()
    check_statepoint(statepoint,'ads')
    # Probably redundant check that job hasn't already been computed
    if not force and 'stage' in this_job.document and this_job.document['stage'] == 'finished':
        print 'job %s is already finished. skipping setup.'%(this_job.get_id())
        return False

    if 'solutes' in statepoint:
        solutes = statepoint['solutes']
        if statepoint['solutes'] is None or (solutes[0] is None and solutes[1]
                                             is None):
            print('Statepoint cannot have None solutes... leave solutes key '
                  'out entirely!')
            return False
    
    inherited_keys = ('N','xy_len','solvents','forcefield',\
                     'slab_thicknesses','surfactant','T','Pz')
    opt_keys = ('solutes',)

    # Check, create (if lacking), and load dependencies
    lis = [li for li in project.find_jobs()\
            if 'type' in li.statepoint()\
            and li.statepoint()['type'] == 'li'\
            and 'stage' in li.document\
            and li.document['stage'] == 'finished'\
            and match(li.statepoint(), statepoint, inherited_keys, opt_keys)]

    if len(lis) == 0:
        li_sp = subdict(statepoint, inherited_keys, opt_keys=opt_keys)
        li_sp['type'] = 'li'
        check_statepoint(li_sp,'li')
        with project.open_job(li_sp) as li_job:
            setup_li(li_job,num_threads=num_threads,verbosity=verbosity)
    elif len(lis) > 1:
        print('Multiple loaded interfaces are usable '
              'for job {}'.format(this_job.get_id()))
        print('Using the first returned by project.find_jobs()')
        li_job = lis[0]
    else:
        li_job = lis[0]

    ##########################################
    ## Dependencies exist, so set up the configuration array
    eq_conf = li_job.workspace()+'/'+'li_eq.gro'
    solvents = statepoint['solvents']
    solventA = solvents[0]
    surfactant = statepoint['surfactant']
    ff = statepoint['forcefield']

    A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
    surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]

    copyfile(eq_conf,'li_eq.gro')
    copyfile(eq_conf.replace('.gro','.tpr'),'li_eq.tpr')

    gmx = gmx_wrapper.Gromacs(verbosity=verbosity, ignore_pme_warning=True)

    # Get my bearings and translate slab to left end of box near z=0
    z1, z2, w, _ = identify_slab('li_eq.gro', 'li_eq.gro', A_sel_str)
    gmx.trjconv('-s li_eq.tpr -f li_eq.gro -o aligned.pdb -pbc atom '
                '-trans 0 0 {}'.format((-z1+3.*w)/10.), stdin_text='System\n')
    os.remove('li_eq.tpr')

    # Reload translated box and choose low z, hi z
    z1, z2, w, _ = identify_slab('aligned.pdb', 'aligned.pdb', A_sel_str)
    u = mda.Universe('aligned.pdb')
    u2 = mda.Universe(res.abspath+res.ff_molecule_pdb[ff]['dummy'])
    u2.dimensions = u.dimensions
    zghost = (z1+z2)/2.
    u2.atoms.positions = np.array([[0., 0., zghost]], dtype=np.float32)
    u3 = mda.Merge(u.atoms, u2.atoms)
    u3.dimensions = u.dimensions
    with mda.Writer('aligned+ghost.pdb') as W:
        W.write(u3.atoms)
    del(u)
    del(u2)
    del(u3)
    u = mda.Universe('aligned+ghost.pdb')

    surf_max_z = u.dimensions[2]*0.49
    start_z = statepoint['start_z']
    if start_z > surf_max_z:
        logger.warning('start_z > surf_max_z')
    A_z = u.select_atoms(A_sel_str).center_of_geometry()[2]
    surf = u.select_atoms(surf_sel_str)
    surf_z = surf.center_of_geometry()[2]
    hi_z = A_z + start_z - w

    # Create ndx file
    ndx = NDX('aligned+ghost.pdb','ca.ndx')
    ndx.AppendMDASelectionStr(A_sel_str,'pull_ref')
    resid = surf.resids[0]
    pull_surf_sel_str = surf_sel_str+' and resid %d'%resid
    pulled_surfactant = u.select_atoms(pull_surf_sel_str)
    ghost = u.select_atoms('resname DUM')
    ndx.AppendMDASelectionStr(pull_surf_sel_str,'pull_surf')
    ndx.AppendMDASelectionStr('resname DUM', 'ghost')
    with open('ca.top','w') as top:
        top.write(gmx_make_top(ff,'aligned+ghost.pdb'))
    pull_in_rate = -0.002
    pull_out_rate = 0.002
    pull_k = 6000

    # Write mdp file from template
    mdp = Gromacs_mdp(ff=ff,type='pull')
    mdp['freezegrps'] = 'ghost'
    mdp['freezedim'] = 'Y Y Y'
    mdp['pull_ngroups'] = 2
    mdp['pull_ncoords'] = 1
    mdp['pull_group1_name'] = 'pull_ref'
    mdp['pull_group2_name'] = 'pull_surf'
    mdp['pull_coord1_type'] = 'umbrella'
    mdp['pull_coord1_geometry'] = 'distance'
    mdp['pull_coord1_dim'] = 'N N Y'
    mdp['pull_coord1_start'] = 'yes'
    mdp['pull_coord1_groups'] = '1 2'
    mdp['pull_coord1_k'] = pull_k
    mdp['nsteps'] = abs((hi_z - surf_z)/10.)/abs(pull_in_rate*mdp['dt'])
    mdp['pull_coord1_rate'] = pull_out_rate
    mdp.write('pull_out.mdp')

    def format_indices_for_plumed(indices):
        data = sorted(indices)
        y = []
        for k, g in groupby(enumerate(data), lambda (i,x):i-x):
            x = map(itemgetter(1), g)
            if len(x) > 1:
                y.append('{}-{}'.format(min(x), max(x)))
            else:
                y.append(str(x[0]))
        return ','.join(y)


    with open('plumed_pull_out.dat', 'w') as f:
        plumed_text = ('c_surf: COM ATOMS={atoms}\n'
                       'ghost: COM ATOMS={ghost_atoms}\n'
                       'd: DISTANCE ATOMS=ghost,c_surf COMPONENTS\n'
                       'PRINT STRIDE=1 ARG=d.z FILE=COLVAR\n'
                       '#COMMITTOR ...\n'
                       '#  ARG=p.z\n'
                       '#  STRIDE=1\n'
                       '#  BASIN_A_LOWER=-999\n'
                       '#  BASIN_A_UPPER={{lower_bound}}\n'
                       '#  BASIN_B_LOWER={{upper_bound}}\n'
                       '#  BASIN_B_UPPER=999\n'
                       '#  FILE=commit0\n'
                       '#... COMMITTOR\n')
        plumed_text = plumed_text.format(atoms=format_indices_for_plumed(pulled_surfactant.indices+1),
                                         ghost_atoms=ghost.indices[0]+1)
        f.write(plumed_text)

    gmx.grompp('-f pull_out.mdp -c aligned+ghost.pdb -p ca.top'
               ' -n ca.ndx -o pull_out.tpr -maxwarn 1')

    if num_threads:
        gmx.mdrun_mpi(num_threads, '-deffnm pull_out -plumed plumed_pull_out.dat')
    else:
        gmx.mdrun_mpi(1, '-deffnm pull_out -plumed plumed_pull_out.dat')

    #1#u = mda.Universe('pull_out.tpr', 'pull_out.xtc')
    #1#pulled_surfactant = u.select_atoms(pull_surf_sel_str)
    #1#monolayer_surfactant = u.select_atoms(surf_sel_str+' and not resid {}'.format(resid))
    #1#monolayer_surfactant_rel_z = defaultdict(list)
    #1#pulled_surfactant_tip_to_tip = []
    #1#logger.info('Orientational autocorrelation uses first and last atom in'
    #1#            ' molecule as tips of tail and head. If this isn\'t right,'
    #1#            ' then change the code!')
    #1#for ts in u.trajectory:
    #1#    monolayer_com_z = monolayer_surfactant.center_of_mass()[2]
    #1#    for i in set(monolayer_surfactant.resids):
    #1#        each = monolayer_surfactant.select_atoms('resid {}'.format(i))
    #1#        monolayer_surfactant_rel_z[i].append(each.center_of_mass()[2]-monolayer_com_z)
    #1#    pulled_surfactant_tip_to_tip.append(pulled_surfactant.positions[-1] - 
    #1#                                        pulled_surfactant.positions[0])
    #1#job.document['monolayer_surfactant_rel_z'] = monolayer_surfactant_rel_z
    #1#variance_rel_z = np.vmeanmeanmeanar(sum(monolayer_surfactant_rel_z.values(), []))
    #1#ac_mean_rel_z = np.mean([unnormalized_autocorrelation(rel_z) for rel_z in monolayer_surfactant_rel_z.values()], axis=0)
    #1#acn_rel_z = ac_mean_rel_z/variance_rel_z
    #1##acn = ac[len(ac)/2::]/np.max(ac)
    #1#if acn_rel_z[1] < 0.135335:
    #1#    tau_rel_z = 1
    #1#else:
    #1#    tau_rel_z = 2*int(np.arange(len(acn_rel_z))[acn_rel_z < 0.367879][0])
    #1#dt_autocorr = mdp['nstxtcout']*mdp['dt']
    #1#tau_rel_z = dt_autocorr*tau_rel_z
    #1#job.document['tau_rel_z'] = tau_rel_z
    #1#tau_orient =  dt_autocorr*vector_autocorr(pulled_surfactant_tip_to_tip)
    #1#job.document['tau_orient'] = tau_orient
    #1#logger.info('tau_rel_z = {} ps'.format(tau_rel_z))
    #1#logger.info('tau_orient = {} ps'.format(tau_orient))
    #1#tau_sample = max(tau_rel_z, tau_orient)

    with open('plumed_config_sample.dat', 'w') as f:
        plumed_text = ('c_surf: COM ATOMS={atoms}\n'
                       'ghost: COM ATOMS={ghost_atoms}\n'
                       'd: DISTANCE ATOMS=ghost,c_surf COMPONENTS\n'
                       'PRINT STRIDE=1 ARG=d.z FILE=COLVAR\n'
                       'COMMITTOR ...\n'
                       '  ARG=d.z\n'
                       '  STRIDE=1\n'
                       '  BASIN_A_LOWER=-999\n'
                       '  BASIN_A_UPPER=6.0\n'
                       '  BASIN_B_LOWER=9.5\n'
                       '  BASIN_B_UPPER=999\n'
                       '  FILE=commit0\n'
                       '... COMMITTOR\n')
        plumed_text = plumed_text.format(atoms=format_indices_for_plumed(pulled_surfactant.indices+1),
                                         ghost_atoms=ghost.indices[0]+1)
        f.write(plumed_text)


    # Write mdp file from template
    tau_sample = 1000.
    mdp['nstxtcout'] = int(tau_sample/mdp['dt'])
    mdp['nsteps'] = mdp['nstxtcout']*num_runs+1
    mdp['pull_coord1_rate'] = 0.
    mdp.write('config_sample.mdp')

    gmx.grompp('-f config_sample.mdp -c myman.gro -p ca.top'+\
                ' -n ca.ndx -o config_sample.tpr')

    if num_threads:
        gmx.mdrun_mpi(num_threads, '-deffnm config_sample -plumed plumed_config_sample.dat')
    else:
        gmx.mdrun_mpi(1, '-deffnm config_sample -plumed plumed_config_sample.dat')

    #X# Execute pull out.
    #X# Run for a while.
    #X# Calculate autocorrelation times of relative monolayer positions and 
    #X# surfactant orientation.
    #X# Record in job document.
    #X# Give a shout of the numbers via debug logger.
    #X# Create mdp file and set up gromacs to spit out the desired configs,
    #   if possible
    #X# Run harmonic bias sample
    #X# Create plumed file
    # # Load and run each config with plumed.
    # # After each simulation, write to job doc whether it adsorbed and what
    #   time it escaped the run zone.

    #################################################

    # Clean up
    this_job.document['stage'] = 'ready'
    this_job.document['time'] = time.ctime()
    this_job.document['pull_out_rate'] = pull_out_rate
    this_job.document['pull_k'] = pull_k
    this_job.document['run_count'] = 0
    this_job.document['run_type'] = 'python'
    this_job.document['python_args'] = '-c "import signac; project = signac.contrib.get_project(); import sys; sys.path.append(\'../../\'); from setup_ads import run_ads; run_ads(project.open_job(id=\'%s\'), maxh=$MAXH)"'%(this_job.get_id())


def run_config_array(this_job, num_threads=None, verbosity=0,  maxh=None):
    statepoint = this_job.statepoint()
    surfactant = statepoint['surfactant']
    solventA = statepoint['solvents'][0]
    A_sel_str = 'resname '+res.resname_of_molecule[solventA]
    surf_sel_str = 'resname '+res.resname_of_molecule[surfactant]

    if this_job.document['stage'] != 'ready':
        print('Job is not in stage ready. Will not run.')
        return False

    gmx = gmx_wrapper.Gromacs(ignore_pme_warning=True, verbosity=verbosity)

    if num_threads is not None:
        nt = '-nt {:d}'.format(num_threads)
    else:
        nt = ''
    if maxh is not None:
        maxh = '-maxh {:f}'.format(0.98*maxh/2.)
    else:
        maxh = ''

    with this_job:
        gmx.mdrun('-deffnm pull_in {nt} {maxh} -cpi'.format(
                    nt=nt,
                    maxh=maxh))
        try:
            gmx.mdrun('-deffnm pull_out {nt} {maxh} -cpi'.format(
                        nt=nt,
                        maxh=maxh))
        except gmx_wrapper.GromacsError as gmx_error:
            if 'Distance between pull groups' in gmx_error.message:
                print 'Error occurred due to pulling too far. Trying to finish...'
            else:
                raise gmx_error

        gmx.trjcat('-f pull_in.xtc pull_out.xtc -o config_array.xtc -cat')

        u = mda.Universe('pull_in.tpr','config_array.xtc')
        z1,z2,w,_ = identify_slab('pull_in.tpr','config_array.xtc',A_sel_str)
        surf = u.select_atoms(surf_sel_str)
        resid = surf.resids[0]
        pull_surf_sel_str = surf_sel_str+' and resid %d'%resid
        pull_surf = u.select_atoms(pull_surf_sel_str)
        pull_ref = u.select_atoms(A_sel_str)
        zmin = 1e9
        zmax = -1e9
        for ts in u.trajectory:
            mda.lib.mdamath.make_whole(pull_surf)
            z = pull_surf.center_of_mass()[2] - z2
            if z < zmin:
                zmin = z
            elif z > zmax:
                zmax = z
    #
    #########################################

    # Clean up
    this_job.document['stage'] = 'finished'
    this_job.document['time'] = time.ctime()
    this_job.document['z_min'] = zmin/10.
    this_job.document['z_max'] = zmax/10.
    this_job.document['z_A_COM'] = (pull_ref.center_of_geometry()[2]-z2)/10.
    this_job.document['z_interface'] = 0.





####"""
####    Adsorption:
####    Surfactant is placed at the midplane between doubly-loaded interfaces, and
####    boundaries are defined which if crossed, will delete the surfactant and
####    replace another random configuration in the starting place.
####"""
####
####import signac
####import resources as res
####import time
####import gmx_wrapper
####import MDAnalysis as mda
####import numpy as np
####from identify_slab import identify_slab
####from shutil import copyfile
####
####project = signac.contrib.get_project()
####
####def setup_ads(this_job, n_ads, z_start=None, num_threads=None, force=False, verbosity=0):
####    """
####        This sets up a spontaneous adsorption id dependencies are available.
####    """
####    from setup_base import match, subdict
####    from setup_li import setup_li
####    from key_sets import check_statepoint
####    from PDBTools import pdbcat, NDX
####    from topology import gmx_make_top
####    from mdp import Gromacs_mdp
####
####    print 'Setting up job %s'%(this_job.get_id())
####    statepoint = this_job.statepoint()
####    check_statepoint(statepoint,'config array')
####    # Probably redundant check that job hasn't already been computed
####    if not force and 'stage' in this_job.document and this_job.document['stage'] == 'finished':
####        print 'job %s is already finished. skipping setup.'%(this_job.get_id())
####        return False
####
####    if 'solutes' in statepoint:
####        solutes = statepoint['solutes']
####        if statepoint['solutes'] is None or (solutes[0] is None and solutes[1] is None):
####            print 'Statepoint cannot have None solutes... leave solutes key out entirely!'
####            return False
####    
####    inherited_keys = ('N','xy_len','solvents','forcefield',\
####                     'slab_thicknesses','surfactant','T','Pz')
####    opt_keys = ('solutes',)
####
####    # Check, create (if lacking), and load dependencies
####    lis = [li for li in project.find_jobs()\
####            if 'type' in li.statepoint()\
####            and li.statepoint()['type'] == 'li'\
####            and 'stage' in li.document\
####            and li.document['stage'] == 'finished'\
####            and match(li.statepoint(), statepoint, inherited_keys, opt_keys)]
####
####    if len(lis) == 0:
####        li_sp = subdict(statepoint, inherited_keys, opt_keys=opt_keys)
####        li_sp['type'] = 'li'
####        check_statepoint(li_sp,'li')
####        with project.open_job(li_sp) as li_job:
####            setup_li(li_job,num_threads=num_threads,verbosity=verbosity)
####    elif len(lis) > 1:
####        print 'Multiple loaded interfaces are usable for job %s'%(this_job.get_id())
####        print 'Using the first returned by project.find_jobs()'
####        li_job = lis[0]
####    else:
####        li_job = lis[0]
####
####
##### parameters
##### same as dli
##### get singly-loaded interface
##### generate random surfactant configurations by putting it in a box of solvent A (or is it B?)
##### if the solvent is None, do NVT
##### otherwise, do NPT
##### -- start loop --
##### define inner boundary and outer boundary for cutoff
##### - if there is no adsorbed surfactant, use interface position
##### - if there is adsorbed surfactant, use arithmetic mean of
#####   average position and minimum position
##### - outer boundary should be other interface, minus some safety interval
##### add surfactant to box
##### create plumed script to stop simulation
##### run simulation
##### -- repeat loop until done --
