import signac
from setup_slab import setup_slab
from setup_surfactant_slab import setup_surfactant_slab
from setup_pair import setup_pair
from setup_li import setup_li
from setup_dli import setup_dli
from setup_config_array import setup_config_array
from umbrella import setup_umbrella
from tension import setup_tension
from solution import setup_solution

project = signac.contrib.get_project()

known_sp_types = ['slab', 'surfactant slab', 'solution', 'pair', 'li', 'dli',
                  'config array', 'tension', 'umbrella']

def smart_setup(statepoints, verbosity=0, runtime=None, resume=False):
    """Run jobs in an efficient order.
    
    For certain jobs (e.g. li or dli), the setup scripts can use a more
    efficient method if a similar higher-N run has been setup first
    """

    # Make sure our input is valid
    if type(statepoints) not in (tuple, list, set):
        if type(statepoints) == dict:
            statepoints = [statepoints]
        else:
            raise TypeError('Unrecognized input type: {}'.format(
                            type(statepoints)))

    sp_types = set([sp['type'] for sp in statepoints])

    if not all(t in known_sp_types for t in sp_types):
        raise TypeError('Unrecognized type(s):\n{}'.format('\n'.join(
                         [t for t in sp_types if t not in known_sp_types])))

    if ('tension' in sp_types or 'umbrella' in sp_types) and runtime is None:
        raise TypeError('Parameter runtime must be specified to setup tension'
                        ' or umbrella types')

    # Categorize statepoints
    sp_by_type = {t: [sp for sp in statepoints if sp['type'] == t]
                     for t in sp_types}

    # Run jobs
    if 'slab' in sp_by_type.keys():
        for sp in sp_by_type['slab']:
            with project.open_job(sp) as job:
                setup_slab(job, verbosity=verbosity)

    if 'surfactant slab' in sp_by_type.keys():
        for sp in sp_by_type['surfactant slab']:
            with project.open_job(sp) as job:
                setup_surfactant_slab(job, verbosity=verbosity)

    if 'solution' in sp_by_type.keys():
        for sp in sp_by_type['solution']:
            with project.open_job(sp) as job:
                setup_solution(job, runtime, verbosity=verbosity)

    if 'pair' in sp_by_type.keys():
        for sp in sp_by_type['pair']:
            with project.open_job(sp) as job:
                setup_pair(job, verbosity=verbosity, resume=resume)

    if 'li' in sp_by_type.keys():
        for sp in sorted(sp_by_type['li'], key=lambda x: x['N'],
                         reverse=True):
            with project.open_job(sp) as job:
                setup_li(job, verbosity=verbosity, resume=resume)

    if 'dli' in sp_by_type.keys():
        for sp in sorted(sp_by_type['dli'], key=lambda x: x['N'],
                         reverse=True):
            with project.open_job(sp) as job:
                setup_dli(job, verbosity=verbosity, resume=resume)

    if 'config array' in sp_by_type.keys():
        for sp in sorted(sp_by_type['config array'], key=lambda x: x['N'],
                         reverse=True):
            with project.open_job(sp) as job:
                setup_config_array(job, verbosity=verbosity, resume=resume)

    if 'umbrella' in sp_by_type.keys():
        for sp in sorted(sp_by_type['umbrella'], key=lambda x: x['N'],
                         reverse=True):
            with project.open_job(sp) as job:
                setup_umbrella(job, runtime, verbosity=verbosity, resume=resume)

    if 'tension' in sp_by_type.keys():
        for sp in sorted(sp_by_type['tension'], key=lambda x: x['N'],
                         reverse=True):
            with project.open_job(sp) as job:
                setup_tension(job, runtime, verbosity=verbosity, resume=resume)
