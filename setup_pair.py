"""
    Slab pairing:
    The unlucky slab which gets its atoms removed is the one with fewer
    atoms per molecule, or the second one if both have equal number of atoms.
    The reason for deleting atoms from the slab with fewer atoms per molecule
    is that hopefully less material will be deleted overall.

    The slabs must be squeezed together after they are placed next to each other.
    Slabs will be chceked so that tanh profiles will be best-fit to the slabs,
    and the edges of these slabs must be in contact (within some tolerance).
    The normalized, fitted slabs must have MSD per length below some threshold.
"""
import signac
import resources as res
import time
import gmx_wrapper
from setup_base import match, subdict
from setup_slab import setup_slab
from key_sets import check_statepoint
from PDBTools import pdbcat
import MDAnalysis as mda
import numpy as np
import resources as res
from topology import gmx_make_top
from mdp import Gromacs_mdp
from glob import glob
import os
from identify_slab import identify_slab
from squeezing import squeeze
from PackmolWrapper import PackmolInput
from solutes import add_solutes, process_statepoint_solutes
import time

project = signac.contrib.get_project()

def setup_pair(this_job,force=False,num_threads=None,verbosity=0):
    """
        This sets up a slab if dependencies are available.
        This function should be called /after/ verifying that the job is actually requested.
    """
    starttime = time.time()
    print 'Setting up job %s'%(this_job.get_id())
    statepoint = this_job.statepoint()
    check_statepoint(statepoint,'pair')
    # Probably redundant check that job hasn't already been computed
    if not force and 'stage' in this_job.document and this_job.document['stage'] == 'finished':
        print 'job %s is already finished. skipping setup.'%(this_job.get_id())
        return False

    # Check for adequate metadata and resources as best I can (misses existence of itp files,
    # but I'll take care of this when I make itp paths relative, by copying itp files to job dir)
    assert(len(statepoint['solvents']) == 2)
    solvents = statepoint['solvents']
    solventA = solvents[0]
    solventB = solvents[1]
    thicknessA = statepoint['slab_thicknesses'][0]
    thicknessB = statepoint['slab_thicknesses'][1]
    T0             = statepoint['T']
    Pz0            = statepoint['Pz']
    
    try:
        solutes = process_statepoint_solutes(statepoint)
    except AssertionError:
        print 'Invalid solutes definition'
        return False

    if solventA is None:
        raise Exception('Only solvent B can be None')

    A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
    if solventB is not None:
        B_sel_str = 'resname %s'%res.resname_of_molecule[solventB]

    ff = statepoint['forcefield']
    packmol_tol = res.packmol_tols[res.ff_scale[ff]]
    xy_len = statepoint['xy_len']
    T0             = statepoint['T']
    Pz0            = statepoint['Pz']

    assert(Gromacs_mdp(ff=ff,type='em'))
    if None in solvents:
        assert(Gromacs_mdp(ff=ff,type='nvt'))
    else:
        assert(Gromacs_mdp(ff=ff,type='squeeze'))
        assert(Gromacs_mdp(ff=ff,type='eq'))

    # Check, create (if lacking), and load dependencies
    inherited_keys = ('xy_len','forcefield','T','Pz')

    slabs = [slab_job for slab_job in project.find_jobs()\
            if 'type' in slab_job.statepoint()\
            and slab_job.statepoint()['type'] == 'slab'\
            and 'stage' in slab_job.document\
            and slab_job.document['stage'] == 'finished'
            and match(slab_job.statepoint(), statepoint, inherited_keys)]

    slabAs = [slab_job for slab_job in slabs\
            if 'solvent' in slab_job.statepoint() \
            and slab_job.statepoint()['solvent'] == solventA\
            and 'slab_thickness' in slab_job.statepoint()\
            and slab_job.statepoint()['slab_thickness'] == thicknessA]
    if len(slabAs) == 0:
        slabA_sp = subdict(statepoint, inherited_keys)
        slabA_sp['solvent'] = solventA
        slabA_sp['slab_thickness'] = thicknessA
        slabA_sp['type'] = 'slab'
        with project.open_job(slabA_sp) as slabA_job:
            setup_slab(slabA_job,num_threads=num_threads,verbosity=verbosity)
    elif len(slabAs) > 1:
        print 'Multiple slabs are usable for job %s'%(this_job.get_id())
        print 'Using the first returned by project.find_jobs()'
        slabA_job = slabAs[0]
    else:
        slabA_job = slabAs[0]

    if solventB is not None:
        slabBs = [slab_job for slab_job in slabs\
                if 'solvent' in slab_job.statepoint() \
                and slab_job.statepoint()['solvent'] == solventB\
                and 'slab_thickness' in slab_job.statepoint()\
                and slab_job.statepoint()['slab_thickness'] == thicknessB]
        if len(slabBs) == 0:
            slabB_sp = subdict(statepoint, inherited_keys)
            slabB_sp['solvent'] = solventB
            slabB_sp['slab_thickness'] = thicknessB
            slabB_sp['type'] = 'slab'
            with project.open_job(slabB_sp) as slabB_job:
                setup_slab(slabB_job,num_threads=num_threads,verbosity=verbosity)
        elif len(slabBs) > 1:
            print 'Multiple slabs are usable for job %s'%(this_job.get_id())
            print 'Using the first returned by project.find_jobs()'
            slabB_job = slabBs[0]
        else:
            slabB_job = slabBs[0]

    #########################################
    # Dependencies exist, so set up the pair
    this_job.document['start time'] = time.ctime()

    gmx = gmx_wrapper.Gromacs(verbosity=verbosity,
            ignore_pme_warning=True,
            ignore_cg_radii_warning=True)

    slabA_gro = '{0}/slab_eq.gro'.format(slabA_job.workspace())
    slabA_tpr = '{0}/slab_eq.tpr'.format(slabA_job.workspace())
    gmx.trjconv('-f {gro} -s {tpr} -pbc mol -o slabA.pdb'.format(gro=slabA_gro,
                                                                 tpr=slabA_tpr),
                stdin_text='System\n')
    uA = mda.Universe('slabA.pdb')
    slabA_Lz = uA.dimensions[2]

    # Add slab B (if not None) adjacent to slab A, making the box big enough for both
    if solventB is None:
        Lz = slabA_Lz+10.*thicknessB+10.*packmol_tol*2.
        gmx.editconf('-f slabA.pdb -o slabA.pdb -box %f %f %f -center %f %f %f'%(xy_len,xy_len,Lz/10.,xy_len/2.,xy_len/2.,Lz/40.))
        pdbcat('catted.pdb','slabA.pdb')

    elif solventB:
        slabB_gro = '%s/slab_eq.gro'%(slabB_job.workspace())
        slabB_tpr = '%s/slab_eq.tpr'%(slabB_job.workspace())

        B_atoms_per_mol = len(mda.Universe(slabB_tpr,slabB_gro).residues[0].atoms)
        gmx.trjconv('-f %s -s %s -pbc mol -o slabB.pdb'%(slabB_gro,slabB_tpr),stdin_text='System\n')
        uB = mda.Universe('slabB.pdb')
        slabB_Lz = np.max(uB.atoms.positions[:,2])-np.min(uB.atoms.positions[:,2])
        Lz = slabA_Lz+slabB_Lz+packmol_tol*2.

        gmx.editconf('-f slabA.pdb -o slabA.pdb -box %f %f %f -center %f %f %f'%(xy_len,xy_len,Lz/10.,xy_len/2.,xy_len/2.,Lz/40.))
        gmx.editconf('-f slabB.pdb -o slabB.pdb -box %f %f %f -center %f %f %f'%(xy_len,xy_len,Lz/10.,xy_len/2.,xy_len/2.,3*Lz/40.))
        pdbcat('catted.pdb','slabA.pdb','slabB.pdb')

    # Add solutes with packmol
    if solutes is not None:
        add_solutes('catted.pdb',ff,solvents,solutes)

    # Make topology and energy minimize
    u = mda.Universe('catted.pdb')
    with open('pair.top','w') as top:
        top.write(gmx_make_top(ff,'catted.pdb'))
    mdp = Gromacs_mdp(ff=ff,type='em')
    mdp.write('em.mdp')
    gmx.grompp('-f em.mdp -c catted.pdb -p pair.top -o pair_em.tpr')
    if num_threads:
        gmx.mdrun('-deffnm pair_em -c pair_squeeze0.gro -nt %d'%(num_threads))
    else:
        gmx.mdrun('-deffnm pair_em -c pair_squeeze0.gro')

    # Squeeze unless there's supposed to be empty space
    if not None in solvents:
        squeeze('pair', statepoint, verbosity=verbosity, num_threads=num_threads)

        # Combine squeezes into one trajectory
        gmx.trjcat('-f {} -cat -o full_squeeze.xtc'.format(' '.join(glob('pair_squeeze*xtc'))))

        # Run equilibration
        mdp = Gromacs_mdp(ff=ff,type='eq')
        mdp.add_params({'ref-p':(1.0,Pz0)})
        mdp.add_params({'ref-t':T0})
        mdp.add_params({'nsteps':50000})
        mdp.write('eq.mdp')

    elif None in solvents:
        mdp = Gromacs_mdp(ff=ff,type='nvt')
        mdp.add_params({'ref-t':T0})
        mdp.add_params({'nsteps':250000})
        mdp.write('eq.mdp')

    gmx.grompp('-f eq.mdp -c pair_squeeze{:d}.gro -p pair.top -o pair_eq.tpr'.format(
                                                    len(glob('pair_squeeze*gro'))-1))

    if num_threads:
        gmx.mdrun('-deffnm pair_eq -nt %d'%(num_threads))
    else:
        gmx.mdrun('-deffnm pair_eq')

    # Combine all setup into one trajectory
    if not None in solvents:
        gmx.trjcat('-f {} pair_eq.xtc -cat -o full.xtc'.format(' '.join(glob('pair_squeeze*.xtc'))))
    elif None in solvents:
        gmx.trjcat('-f pair_eq.xtc -cat -o full.xtc')
    # Discard extra trajectories
    for xtc in glob('slab_squeeze*xtc'):
        os.remove(xtc)
    os.remove('pair_eq.xtc')
    #
    #########################################


    # Clean up
    this_job.document['stage'] = 'finished'
    this_job.document['end time'] = time.ctime()
    this_job.document['wall time'] = time.time() - starttime
