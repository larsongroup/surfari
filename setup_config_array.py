"""
    Configuration array:
    Surfactant is pulled from a single-loaded interface both inward and outward with respect to solvent slab A. These pulling trajectories are then concatenatd into a single trajectory `config_array.xtc` which can be used for configurations of surfactant at different distances from the reference slab A. 
"""
import signac
import resources as res
import time
import gmx_wrapper
import MDAnalysis as mda
import numpy as np
from identify_slab import identify_slab
from shutil import copyfile

project = signac.contrib.get_project()

def setup_config_array(this_job,num_threads=None,force=False,verbosity=0):
    """
        This sets up a configuration array if dependencies are available.
        This function should be called /after/ verifying that the job is actually requested.
    """
    from setup_base import match, subdict
    from setup_li import setup_li
    from key_sets import check_statepoint
    from PDBTools import pdbcat, NDX
    from topology import gmx_make_top
    from mdp import Gromacs_mdp

    print 'Setting up job %s'%(this_job.get_id())
    statepoint = this_job.statepoint()
    check_statepoint(statepoint,'config array')
    # Probably redundant check that job hasn't already been computed
    if not force and 'stage' in this_job.document and this_job.document['stage'] == 'finished':
        print 'job %s is already finished. skipping setup.'%(this_job.get_id())
        return False

    if 'solutes' in statepoint:
        solutes = statepoint['solutes']
        if statepoint['solutes'] is None or (solutes[0] is None and solutes[1] is None):
            print 'Statepoint cannot have None solutes... leave solutes key out entirely!'
            return False
    
    inherited_keys = ('N','xy_len','solvents','forcefield',\
                     'slab_thicknesses','surfactant','T','Pz')
    opt_keys = ('solutes',)

    # Check, create (if lacking), and load dependencies
    lis = [li for li in project.find_jobs()\
            if 'type' in li.statepoint()\
            and li.statepoint()['type'] == 'li'\
            and 'stage' in li.document\
            and li.document['stage'] == 'finished'\
            and match(li.statepoint(), statepoint, inherited_keys, opt_keys)]

    if len(lis) == 0:
        li_sp = subdict(statepoint, inherited_keys, opt_keys=opt_keys)
        li_sp['type'] = 'li'
        check_statepoint(li_sp,'li')
        with project.open_job(li_sp) as li_job:
            setup_li(li_job,num_threads=num_threads,verbosity=verbosity)
    elif len(lis) > 1:
        print 'Multiple loaded interfaces are usable for job %s'%(this_job.get_id())
        print 'Using the first returned by project.find_jobs()'
        li_job = lis[0]
    else:
        li_job = lis[0]

    #########################################
    # Dependencies exist, so set up the configuration array
    eq_conf = li_job.workspace()+'/'+'li_eq.gro'
    solvents = statepoint['solvents']
    solventA = solvents[0]
    surfactant = statepoint['surfactant']
    ff = statepoint['forcefield']

    A_sel_str = 'resname %s'%res.resname_of_molecule[solventA]
    surf_sel_str = 'resname %s'%res.resname_of_molecule[surfactant]

    copyfile(eq_conf,'li_eq.gro')

    gmx = gmx_wrapper.Gromacs(verbosity=verbosity,ignore_pme_warning=True)

    # Get my bearings and translate slab to left end of box near z=0
    z1,z2,w,_ = identify_slab('li_eq.gro','li_eq.gro',A_sel_str)
    gmx.editconf('-f li_eq.gro -o aligned.pdb -pbc yes -translate 0 0 %f'%((-z1+3.*w)/10.))

    # Reload translated box and choose low z, hi z
    u = mda.Universe('aligned.pdb')
    surf_max_z = u.dimensions[2]*0.49
    A_z = u.select_atoms(A_sel_str).center_of_geometry()[2]
    surf = u.select_atoms(surf_sel_str)
    surf_z = surf.center_of_geometry()[2]
    low_z = A_z + w
    hi_z = A_z + surf_max_z - w

    # Create ndx file
    ndx = NDX('aligned.pdb','ca.ndx')
    ndx.AppendMDASelectionStr(A_sel_str,'pull_ref')
    resid = surf.resids[0]
    pull_surf_sel_str = surf_sel_str+' and resid %d'%resid
    ndx.AppendMDASelectionStr(pull_surf_sel_str,'pull_surf')
    with open('ca.top','w') as top:
        top.write(gmx_make_top(ff,'aligned.pdb'))
    pull_in_rate = -0.002
    pull_out_rate = 0.002
    pull_k = 6000

    # Write mdp file from template
    mdp = Gromacs_mdp(ff=ff,type='pull')
    mdp['nsteps'] = abs((low_z - surf_z)/10.)/abs(pull_in_rate*mdp['dt'])
    mdp['pull_ngroups'] = 2
    mdp['pull_ncoords'] = 1
    mdp['pull_group1_name'] = 'pull_ref'
    mdp['pull_group2_name'] = 'pull_surf'
    mdp['pull_coord1_type'] = 'umbrella'
    mdp['pull_coord1_geometry'] = 'distance'
    mdp['pull_coord1_dim'] = 'N N Y'
    mdp['pull_coord1_start'] = 'yes'
    mdp['pull_coord1_groups'] = '1 2'
    mdp['pull_coord1_rate'] = pull_in_rate
    mdp['pull_coord1_k'] = pull_k
    mdp.write('pull_in.mdp')
    mdp['nsteps'] = abs((hi_z - surf_z)/10.)/abs(pull_in_rate*mdp['dt'])
    mdp['pull_coord1_rate'] = pull_out_rate
    mdp.write('pull_out.mdp')

    gmx.grompp('-f pull_in.mdp -c aligned.pdb -p ca.top'+\
                ' -n ca.ndx -o pull_in.tpr')
    gmx.grompp('-f pull_out.mdp -c aligned.pdb -p ca.top'+\
                ' -n ca.ndx -o pull_out.tpr')
    #
    #################################################

    # Clean up
    this_job.document['stage'] = 'ready'
    this_job.document['time'] = time.ctime()
    this_job.document['pull_in_rate'] = pull_in_rate
    this_job.document['pull_out_rate'] = pull_out_rate
    this_job.document['pull_k'] = pull_k
    this_job.document['run_count'] = 0
    this_job.document['run_type'] = 'python'
    this_job.document['python_args'] = '-c "import signac; project = signac.contrib.get_project(); import sys; sys.path.append(\'../../\'); from setup_config_array import run_config_array; run_config_array(project.open_job(id=\'%s\'), maxh=$MAXH)"'%(this_job.get_id())

def run_config_array(this_job, num_threads=None, verbosity=0,  maxh=None):
    statepoint = this_job.statepoint()
    surfactant = statepoint['surfactant']
    solventA = statepoint['solvents'][0]
    A_sel_str = 'resname '+res.resname_of_molecule[solventA]
    surf_sel_str = 'resname '+res.resname_of_molecule[surfactant]

    if this_job.document['stage'] != 'ready':
        print('Job is not in stage ready. Will not run.')
        return False

    gmx = gmx_wrapper.Gromacs(ignore_pme_warning=True, verbosity=verbosity)

    if num_threads is not None:
        nt = '-nt {:d}'.format(num_threads)
    else:
        nt = ''
    if maxh is not None:
        maxh = '-maxh {:f}'.format(0.98*maxh/2.)
    else:
        maxh = ''

    with this_job:
        gmx.mdrun('-deffnm pull_in {nt} {maxh} -cpi'.format(
                    nt=nt,
                    maxh=maxh))
        try:
            gmx.mdrun('-deffnm pull_out {nt} {maxh} -cpi'.format(
                        nt=nt,
                        maxh=maxh))
        except gmx_wrapper.GromacsError as gmx_error:
            if 'Distance between pull groups' in gmx_error.message:
                print 'Error occurred due to pulling too far. Trying to finish...'
            else:
                raise gmx_error

        gmx.trjcat('-f pull_in.xtc pull_out.xtc -o config_array.xtc -cat')

        u = mda.Universe('pull_in.tpr','config_array.xtc')
        z1,z2,w,_ = identify_slab('pull_in.tpr','config_array.xtc',A_sel_str)
        surf = u.select_atoms(surf_sel_str)
        resid = surf.resids[0]
        pull_surf_sel_str = surf_sel_str+' and resid %d'%resid
        pull_surf = u.select_atoms(pull_surf_sel_str)
        pull_ref = u.select_atoms(A_sel_str)
        zmin = 1e9
        zmax = -1e9
        for ts in u.trajectory:
            mda.lib.mdamath.make_whole(pull_surf)
            z = pull_surf.center_of_mass()[2] - z2
            if z < zmin:
                zmin = z
            elif z > zmax:
                zmax = z
    #
    #########################################

    # Clean up
    this_job.document['stage'] = 'finished'
    this_job.document['time'] = time.ctime()
    this_job.document['z_min'] = zmin/10.
    this_job.document['z_max'] = zmax/10.
    this_job.document['z_A_COM'] = (pull_ref.center_of_geometry()[2]-z2)/10.
    this_job.document['z_interface'] = 0.
