import signac
project = signac.contrib.get_project()
abspath = project.root_directory()+'/resources/'

packmol_tols = {'cg':7.,'atomistic':2.}

ff_scale = {
        'gromos53a6oxy+D':'atomistic',
        'dry martini':'cg',
}

oils_last = ('water_SPC','butan-1-ol', 'hexan-1-ol', None,'octane','branched_c11','dodecane','hexadecane','squalane')
hydrophobicity = {oils_last[i]:i for i in range(len(oils_last))}

resname_of_molecule = {
    'NA':'NA',
    'CL':'CL',
    'squalane':'SQU',
    'hexadecane':'50',
    'branched_c11':'PDB',
    'c16e8':'58',
    'c16e8_alt':'58',
    'c12e8':'CE8',
    'c14e8':'DE8',
    'dodecane':'DOD',
    'octane':'OCT',
    'water_SPC':'SOL',
    'AOT':'AOT',
    'hexan-1-ol':'hnl',
    'butan-1-ol':'bnl',
}

molecule_of_resname = {value:key for key,value in resname_of_molecule.items()}

ff_itp = {
    'gromos53a6oxy+D':'gromos53a6oxy+D.ff/forcefield.itp',
    'dry martini':'ShihuDM.ff/ff.itp',
}

ff_molecule_pdb = {
    'gromos53a6oxy+D':
    {
        'squalane':'gromos53a6oxy+D/squalane/squalane.pdb',
        'dodecane':'gromos53a6oxy+D/dodecane/dodecane.pdb',
        'water_SPC':'gromos53a6oxy+D/water/spc.pdb',
        'c12e8':'gromos53a6oxy+D/c12e8/c12e8.pdb',
        'c14e8':'gromos53a6oxy+D/c14e8/c14e8.pdb',
        'NA':'gromos53a6oxy+D/NA/NA.pdb',
        'CL':'gromos53a6oxy+D/CL/CL.pdb',
        'AOT':'gromos53a6oxy+D/AOT/AOT.pdb',
        'branched_c11':'gromos53a6oxy+D/branched_c11/branched_c11.pdb',
        'hexan-1-ol':'gromos53a6oxy+D/alcohols/hexan-1-ol.pdb',
        'butan-1-ol':'gromos53a6oxy+D/alcohols/butan-1-ol.pdb',
    },
    'dry martini':
    {
        'hexadecane':'ShihuDM/hexadecane/hexadecane.pdb',
        'dodecane':'ShihuDM/dodecane/dodecane.pdb',
        'octane':'ShihuDM/octane/octane.pdb',
        'water':None,
        'c12e8':'ShihuDM/c12e8/c12e8.pdb',
        'c16e8':'ShihuDM/c16e8/c16e8.pdb',
        'c16e8_alt':'ShihuDM/c16e8/c16e8.pdb',
    },
}

ff_molecule_itp = {
    'gromos53a6oxy+D':
    {
        'squalane':'gromos53a6oxy+D/squalane/squalane.itp',
        'dodecane':'gromos53a6oxy+D/dodecane/dodecane.itp',
        'water_SPC':'gromos53a6oxy+D.ff/spc.itp',
        'c12e8':'gromos53a6oxy+D/c12e8/c12e8.itp',
        'c14e8':'gromos53a6oxy+D/c14e8/c14e8.itp',
        'NA':'gromos53a6oxy+D.ff/ions.itp',
        'CL':'gromos53a6oxy+D.ff/ions.itp',
        'AOT':'gromos53a6oxy+D/AOT/AOT_53a6oxy.itp',
        'branched_c11':'gromos53a6oxy+D/branched_c11/branched_c11.itp',
        'hexan-1-ol':'gromos53a6oxy+D/alcohols/hexan-1-ol.itp',
        'butan-1-ol':'gromos53a6oxy+D/alcohols/butan-1-ol.itp',
    },
    'dry martini':
    {
        'hexadecane':'ShihuDM/hexadecane/hexadecane.itp',
        'dodecane':'ShihuDM/dodecane/dodecane.itp',
        'octane':'ShihuDM/octane/octane.itp',
        'water':None,
        'c12e8':'ShihuDM/c12e8/c12e8.itp',
        'c16e8':'ShihuDM/c16e8/c16e8.itp',
        'c16e8_alt':'ShihuDM/c16e8/c16e8_alt.itp',
    },
}

ff_molecule_interface_align_group = {
    'gromos53a6oxy+D':
    {
        'c12e8':'name C10 or name C11 or name C12',
        'c14e8':'name C12 or name C13 or name C14',
    },
    'dry martini':
    {
        'c16e8':'name C3 or name C4',
        'c12e8':'name C2 or name C3',
    },
}

density = {
    'gromos53a6oxy+D':
    {
        'squalane':1.154,
        'dodecane':2.652,
        'branched_c11':2.851,
        'water_SPC':33.43,
    },
    'dry martini':
    {
        'hexadecane':2.0,
        'dodecane':2.652,
        'octane':3.7,
    },
    'testff':
    {
        'testmol':0.9,
    },
}

default_mdp_params = {'nstxout':0,'nstvout':0,'nstfout':0,'nstxtcout':500,
        'xtcprecision':100,'nstcomm':100,'cutoff_scheme':'group','nstenergy':100}

ff_mdp_params = {
    'testff':
    {
        'key1':'val1',
        'key2':'val2',
    },
    'gromos53a6oxy+D':
    {
        'ns_type':'grid',
        'coulombtype':'PME',
        'rlist':'0.9',
        'rcoulomb':'0.9',
        'rvdw':'1.4',
        'dt':0.002,
    },
    'dry martini':
    {
        'rlist':'0.9',
        'rcoulomb':'0.9',
        'rvdw':'1.4',
        'dt':0.04,
        'rlist':1.4,
        'coulombtype':'shift',
        'rcoulomb':1.2,
        'epsilon_r':15,
        'vdw-type':'shift',
        'rvdw-switch':0.9,
        'rvdw':1.2,
    },
}

type_mdp_params = {
    'em':
    {
        'constraints':'none',
         'integrator':'steep',
         'nstcgsteep':100,
         'nsteps':1000,
         'emtol':100,
         'emstep':0.001,
         'Tcoupl':'no',
         'Pcoupl':'no',
         'gen_vel':'no',
         'cutoff_scheme':'group',
     },
    'squeeze':
    {
         'constraints':'all-bonds',
         'constraint-algorithm':'LINCS',
         'tc-grps':'system',
         'tcoupl':'v-rescale',
         'tau-t':4.0,
         'ref-t':300,
         'pcoupl':'berendsen',
         'pcoupltype':'semiisotropic',
         'tau-p':4.0,
         'ref-p':(1.0,1.0),
         'nsteps':2000,
         'gen_vel':'no',
         'compressibility':(0.0,3e-4),
    },
    'eq':
    {
         'constraints':'all-bonds',
         'constraint-algorithm':'LINCS',
         'tc-grps':'system',
         'tcoupl':'v-rescale',
         'tau-t':4.0,
         'ref-t':300,
         'pcoupl':'berendsen',
         'pcoupltype':'semiisotropic',
         'tau-p':4.0,
         'ref-p':(1.0,1.0),
         'nsteps':50000,
         'gen_vel':'no',
    },
    'NAPzT':
    {
         'nstxtcout':2500,
         'constraints':'all-bonds',
         'constraint-algorithm':'LINCS',
         'tc-grps':'system',
         'tcoupl':'v-rescale',
         'tau-t':4.0,
         'ref-t':300,
         'pcoupl':'parrinello-rahman',
         'pcoupltype':'semiisotropic',
         'tau-p':8.0,
         'ref-p':(1.0,1.0),
         'nsteps':50000,
         'gen_vel':'no',
    },
    'NPT':
    {
         'nstxtcout':2500,
         'constraints':'all-bonds',
         'constraint-algorithm':'LINCS',
         'tc-grps':'system',
         'tcoupl':'v-rescale',
         'tau-t':4.0,
         'ref-t':300,
         'pcoupl':'parrinello-rahman',
         'pcoupltype':'isotropic',
         'tau-p':8.0,
         'ref-p':(1.0),
         'nsteps':50000,
         'gen_vel':'no',
    },
    'nvt':
    {
         'nstxtcout':2500,
         'constraints':'all-bonds',
         'constraint-algorithm':'LINCS',
         'tc-grps':'system',
         'tcoupl':'v-rescale',
         'tau-t':4.0,
         'ref-t':300,
         'pcoupl':'no',
         'nsteps':2000,
         'gen_vel':'no',
    },
    'pull':
    {
         'constraints':'all-bonds',
         'constraint-algorithm':'LINCS',
         'tc-grps':'system',
         'tcoupl':'v-rescale',
         'tau-t':4.0,
         'ref-t':300,
         'pcoupl':'no',
         'nsteps':2000,
         'gen_vel':'no',
         'pull':'yes',
         'pull-nstfout':0,
         'pull-nstxout':0,
    },
    'umbrella':
    {
         'nstxtcout':2500,
         'constraints':'all-bonds',
         'constraint-algorithm':'LINCS',
         'tc-grps':'system',
         'tcoupl':'v-rescale',
         'tau-t':4.0,
         'ref-t':300,
         'pcoupl':'no',
         'nsteps':2000,
         'gen_vel':'yes',
         'gen_temp':300.0,
         'pull':'yes',
         'pull-nstfout':0,
         'pull-nstxout':100,
         'pcoupl':'parrinello-rahman',
         'pcoupltype':'semiisotropic',
         'tau-p':8.0,
         'ref-p':(1.0,1.0),
    },
    'test':
    {
        'key3':(3,5),
    },
}

ff_type_mdp_params = {
    'testff':
    {
        'test':
        {
            'key1':'val4',
        },
    },
    'dry martini':
    {
        'squeeze':
        {
            'dt':0.004,
        },
        'eq':
        {
            'pcoupl':'no',
            'compressibility':(0.0,3e-4), #TODO verify this
        },
        'NAPzT':
        {
            'pcoupl':'no',
            'compressibility':(0.0,3e-4),
        },
        'NPT':
        {
            'pcoupl':'no',
            'compressibility':(0.0,3e-4),
        },
        'umbrella':
        {
            'pcoupl':'no',
            'compressibility':(0.0,3e-4),
        },
    },
    'gromos53a6oxy+D':
    {
        'eq':
        {
            'compressibility':(0.0,4.5e-5),
        },
        'NAPzT':
        {
            'compressibility':(0.0,4.5e-5),
        },
        'NPT':
        {
            'compressibility':4.5e-5,
        },
        'umbrella':
        {
            'compressibility':(0.0,4.5e-5),
        },


    },
}

