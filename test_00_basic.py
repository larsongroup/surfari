# coding: utf-8
import MDAnalysis as mda
import os
import signac
import resources as res
from smart_setup import smart_setup
from numpy.testing import assert_raises, assert_almost_equal, assert_equal
from compute import outer_slab_discrepancy, solvent_is_leaking
from identify_slab import identify_slab

project = signac.contrib.get_project()

def test_project_config():
    # user and email should be set in signac.rc in root_directory
    assert(project.config['user'])
    assert(project.config['email'])

def test_match():
    from setup_base import match
    d1 = {'a':1,'b':2}
    d2 = {'a':1}
    d3 = {'b':2}
    d4 = {'a':3,'b':2}
    assert(match(d2,d1,('a')))
    assert(not match(d3,d1,('a')))
    assert(not match(d4,d1,('a')))
    d5 = {'a':(1,9),'b':2}
    d6 = {'a':[1,9],'b':4}
    assert(match(d5,d6,('a')))
    assert(not match(d5,d6,('b')))

def test_subdict():
    from setup_base import subdict
    d1 = {'a':1,'b':2}
    d2 = {'a':1}
    d3 = {'b':2}
    assert(subdict(d1,('a',)) == d2)
    assert(not subdict(d1,('a',)) == d3)
    assert(not subdict(d1,('a',)) == d1)

def test_resources():
    assert(type(res.packmol_tols) == dict)
    assert(type(res.ff_molecule_pdb) == dict)
    assert(type(res.ff_molecule_itp) == dict)
    assert(type(res.density) == dict)
    assert(type(res.ff_mdp_params) == dict)
    assert(type(res.type_mdp_params) == dict)
    assert(type(res.resname_of_molecule) == dict)
    assert(type(res.molecule_of_resname) == dict)

    res_path = project.root_directory()+'/resources/'
    assert(os.path.isdir(res_path))
    for ff in res.ff_molecule_pdb:
        for molecule in res.ff_molecule_pdb[ff]:
            pdb = res.ff_molecule_pdb[ff][molecule]
            if pdb is not None:
                assert(os.path.isfile(res_path+pdb)), '%s does not exist'%pdb
    for ff in res.ff_molecule_itp:
        for molecule in res.ff_molecule_itp[ff]:
            itp = res.ff_molecule_itp[ff][molecule]
            if itp is not None:
                assert(os.path.isfile(res_path+itp)), '%s does not exist'%itp

def test_resources_path():
    res_path = project.root_directory()+'/resources/'
    assert(os.path.samefile(os.path.dirname(res.__file__),res_path))

def test_gmx_make_top():
    pass

def test_mda_get_resname():
    from topology import mda_get_resname
    path1='testfiles/one_water.pdb'
    path2='testfiles/two_water.pdb'
    path3='testfiles/one_water_one_oil.pdb'
    path4='testfiles/nothing.pdb'
    assert(mda_get_resname(path1) == 'SOL')
    assert(mda_get_resname(path2) == 'SOL')
    try:
        mda_get_resname(path3)
        raise Exception('This should have failed because %s has two molecules of different type'%path3)
    except:
        pass
    try:
        mda_get_resname(path4)
        raise Exception('This should have failed because %s has no molecules.'%path4)
    except:
        pass
def test_Gromacs():
    from gmx_wrapper import Gromacs
    gmx = Gromacs()
    gmx.editconf('-f testfiles/one_water.pdb -o testfiles/one_water.pdb')

def test_Gromacs_mdp():
    from mdp import Gromacs_mdp_base
    ff = 'testff'
    type = 'test'
    mdp = Gromacs_mdp_base()
    mdp.add_params(res.ff_mdp_params[ff])
    mdp.add_params(res.type_mdp_params[type])
    mdp.add_params(res.ff_type_mdp_params[ff][type])
    mdp.write('test.mdp')
    with open('test.mdp') as file:
        assert('key3 = 3 5' in file.readline())
        assert('key2 = val2' in file.readline())
        assert('key1 = val4' in file.readline())
        assert(not file.readline())
    mdp.add_params({'another_key':4})
    mdp['Another_key'] = 5
    mdp.write('test.mdp')
    with open('test.mdp') as file:
        assert('key3        = 3 5' in file.readline())
        assert('key2        = val2' in file.readline())
        assert('key1        = val4' in file.readline())
        assert('another-key = 5' in file.readline())
        assert(not file.readline())
    os.remove('test.mdp')

def test_squeezing_funcs():
    from squeezing import empty_xyz_bins, eliminate_empty_z_bins, density_correlation
    assert(not empty_xyz_bins('testfiles/spc216.gro','testfiles/spc216.gro'))
    assert(not eliminate_empty_z_bins('testfiles/spc216.gro','testfiles/spc216.gro',2.))
    assert(abs(density_correlation('testfiles/spc216.gro','testfiles/spc216.gro')+0.33898)<1e-5)

def test_empty_statepoint():
    sp = {}
    assert_raises(KeyError, smart_setup, sp, verbosity=1)

def test_unknown_type_statepoint():
    sp ={'type':'unknown','xy_len':6,'solvent':'hexadecane',
            'forcefield':'dry martini',
            'slab_thickness':10,
            'T':300.0,'Pz':1.0,}
    assert_raises(TypeError, smart_setup, sp, verbosity=1)

def test_missing_runtime_statepoint():
    sp = {'type':'umbrella','N':25,'Pz':1.0,'T':300.0,'forcefield':'dry martini',\
            'slab_thicknesses':[10,10],'solvents':['hexadecane',None],\
            'surfactant':'c16e8','xy_len':6,'z':-2.,'k':6000}
    assert_raises(TypeError, smart_setup, sp, verbosity=1)


#def test_surf_is_adsorbed_true():
#
#def test_surf_is_adsorbed_false():

def test_outer_slab_discrepancy():
    conf1 = 'testfiles/leaking1.gro' # z1 < z2 and (z1 + z2)/2. > Lz/2.
    conf2 = 'testfiles/leaking2.gro' # z2 < z1
    conf3 = 'testfiles/leaking3.gro' # z1 < z2 and (z1 + z2)/2. <= Lz/2.
    d1 = outer_slab_discrepancy(conf1, conf1, 'resname SOL')
    d2 = outer_slab_discrepancy(conf2, conf2, 'resname SOL')
    d3 = outer_slab_discrepancy(conf3, conf3, 'resname SOL')
    assert(abs(d1-d2) < 0.4)
    assert(abs(d2-d3) < 0.4)

def test_solvent_is_leaking():
    """Test that solvent leaks are detected same way regardless of slab position.
    """
    conf1 = 'testfiles/leaking1.gro' # z1 < z2 and (z1 + z2)/2. > Lz/2.
    assert(solvent_is_leaking(conf1, conf1, 'resname SOL'))
    # TODO need some not-leaking tests

def test_identify_slab():
    conf1 = 'testfiles/leaking1.gro'
    z1, z2, w, rho = identify_slab(conf1, conf1, 'resname SOL')
    assert_almost_equal(z1, 86.517145109617672, decimal=2)
    assert_almost_equal(z2, 181.18901851807522, decimal=2)
    assert_almost_equal(w, 6.2881510498436199, decimal=2)
    assert_almost_equal(rho, 0.57584375878870275, decimal=2)

def test_mda_atom_order():
    conf1 = 'testfiles/leaking1.gro'
    u = mda.Universe(conf1)
    assert_equal(u.atoms.indices, u.residues.atoms.indices)


