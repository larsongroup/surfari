from gmx_wrapper import Gromacs_mdp_base
import resources as res

class Gromacs_mdp(Gromacs_mdp_base):
    def __init__(self,default=True,ff=None,type=None,params=None):
        """
            Params are applied in this order (if specified):
            1. default
            2. ff
            3. type
            4. ff_type
            5. params (keyword to Gromacs_mdp.__init__)
        """
        self.params = {}
        if default:
            self.add_params(res.default_mdp_params)
        if ff:
            self.add_params(res.ff_mdp_params[ff])
        if type:
            self.add_params(res.type_mdp_params[type])
        if ff and type:
            try:
                self.add_params(res.ff_type_mdp_params[ff][type])
            except:
                pass
        if params:
            self.add_params(params)
        if params is not None:
            self.add_params(params)
