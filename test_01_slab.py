# coding: utf-8
import signac
import resources as res
import os
from smart_setup import smart_setup

project = signac.contrib.get_project()

def test_smart_setup_DM_hexadecane():
    sp ={'type':'slab','xy_len':6,'solvent':'hexadecane',
            'forcefield':'dry martini',
            'slab_thickness':10,
            'T':300.0,'Pz':1.0,}
    smart_setup(sp,verbosity=1)
    with project.open_job(sp) as job:
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_smart_setup_GRO_dodecane():
    sp ={'type':'slab','xy_len':6,'solvent':'dodecane',
            'forcefield':'gromos53a6oxy+D',
            'slab_thickness':10,
            'T':300.0,'Pz':1.0,}
    smart_setup(sp,verbosity=1)
    with project.open_job(sp) as job:
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_smart_setup_water_SPC():
    sp ={'type':'slab','xy_len':6,'solvent':'water_SPC',
            'forcefield':'gromos53a6oxy+D',
            'slab_thickness':10,
            'T':300.0,'Pz':1.0,}
    smart_setup(sp,verbosity=1)
    with project.open_job(sp) as job:
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')
