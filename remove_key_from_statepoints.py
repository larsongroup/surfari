import signac
project = signac.contrib.get_project()

key_to_remove = 'scale'

outdated_jobs = [job for job in project.find_jobs() if key_to_remove in job.statepoint()]

for job in outdated_jobs:
    sp = job.statepoint()
    del sp[key_to_remove]
    try:
        project.reset_statepoint(job,sp)
    except RuntimeError as reset_error:
        print reset_error
