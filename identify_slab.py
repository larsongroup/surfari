import MDAnalysis as mda
import numpy as np
np.seterr(divide='ignore')
from numpy import abs, tanh
from scipy.optimize import differential_evolution

def TanhProfile(z1,dz,w,rho1,x_edges):
    Lx = x_edges[-1]-x_edges[0]
    dx = x_edges[1]-x_edges[0]
    x = (x_edges[:-1]+x_edges[1:])/2.
    x_ext = np.linspace(x[0]-Lx,x[-1]+Lx,3*len(x))
    profile = rho1/2.*(tanh(2*(x_ext-z1)/w)-tanh(2*(x_ext-z1-abs(dz))/w))
    stack = np.vstack((profile[:len(x)],profile[len(x):-len(x)],profile[-len(x):]))
    return np.amax(stack,axis=0)

def TanhProfileResidual(z1_dz_w_rho1,x,rho_to_fit):
    z1   = z1_dz_w_rho1[0]
    dz   = z1_dz_w_rho1[1]
    w    = z1_dz_w_rho1[2]
    rho1 = z1_dz_w_rho1[3]
    return np.sum((TanhProfile(z1,dz,w,rho1,x)-rho_to_fit)**2.)

def identify_slab(tpr, traj, slab_sel_str, frame=None):
    """Return slab edge positions, edge width, and bulk density assuming
    symmetric, intact slab (not pbc-broken). Performs this just for the first frame.

    Parameters
    ----------
    tpr : topology file readable by MDAnalysis
    traj : coordinate file readable by MDAnalysis
    slab_sel_str : selection string for atoms in the slab of interest

    Returns
    -------
    z1 : left edge of slab
    z2 : right edge of slab
    w : interface width
    rho : bulk density of slab
    frame : optional, frame to use (default to first frame)
    """
    # Use first frame of trajectory to find interface
    u = mda.Universe(tpr,traj)
    if frame is not None:
        u.trajectory[frame]
    slab = u.select_atoms(slab_sel_str)
    slab.pack_into_box()

    bins = 200
    weights = np.ones(len(slab.atoms))/(u.dimensions[0]*u.dimensions[1]*u.dimensions[2]/bins)
    if 'masses' in dir(slab):
        weights *= slab.masses
    hist, z_edges = np.histogram(slab.positions[:,2],
                                 weights=weights,
                                 bins=bins,
                                 range=(0,u.dimensions[2]))

    sol = differential_evolution(TanhProfileResidual,bounds=((0,u.dimensions[2]),(0,u.dimensions[2]),(0,u.dimensions[2]/2.),(0.1,4.9)),args=(z_edges,hist),popsize=30,tol=0.001)
    z1f   = sol.values()[3][0]
    dzf   = sol.values()[3][1]
    wf    = sol.values()[3][2]
    rho1f = sol.values()[3][3]
    z2f = z1f + dzf
    return z1f, z2f, wf, rho1f
