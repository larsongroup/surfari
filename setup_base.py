def match(sp_check, sp_ref, match_keys, opt_keys=None):
    for key in match_keys:
        try:
            if key not in sp_check or tuple(sp_check[key]) != tuple(sp_ref[key]):
                return False
        except TypeError:
            if key not in sp_check or sp_check[key] != sp_ref[key]:
                return False
    if opt_keys is not None:
        for key in opt_keys:
            if key in sp_ref:
                try:
                    if key not in sp_check or tuple(sp_check[key]) != tuple(sp_ref[key]):
                        return False
                except TypeError:
                    if key not in sp_check or sp_check[key] != sp_ref[key]:
                        return False
    return True

def subdict(sp_ref, keys, opt_keys=None):
    subdict = {}
    for key in keys:
        subdict[key] = sp_ref[key]
    if opt_keys is not None:
        for key in opt_keys:
            if key in sp_ref:
                subdict[key] = sp_ref[key]
    return subdict

