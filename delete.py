from glob import glob
import signac
import os

project = signac.contrib.get_project()

def delete(job):
    with job:
        # Make sure I'm in a subdirectory of the signac project workspace
        # before I start deleting things
        assert(os.path.samefile(project.workspace(),
            os.path.sep.join(os.getcwd().split(os.path.sep)[:-1])))
        for path in glob('*'):
            os.remove(path)
        if os.path.isfile('.stderr'):
            os.remove('.stderr')
        if os.path.isfile('.stdout'):
            os.remove('.stdout')
        if os.path.isfile('.packmol.inp'):
            os.remove('.packmol.inp')
        if os.path.isfile('.packmol.stdout'):
            os.remove('.packmol.stdout')
    os.removedirs(job.workspace())

def delete_id(*job_ids):
    """
        Before deleteting a job, you should make absolutely sure you want
        to delete the job document and all files computed.
        The result of this job may also be a dependency for some other job,
        so tracability may be hindered.
    """
    for id in job_ids:
        try:
            delete([i for i in project.find_jobs() if i.get_id() == id][0])
        except IndexError:
            print 'Job not found (perhaps it was already delete?)'
