# coding: utf-8
import signac
import resources as res
import os
from smart_setup import smart_setup
import MDAnalysis as mda

project = signac.contrib.get_project()

def test_setup_pair_DM_hexadecane_water():
    # 3afe15ee739fa9efa92263c358e0fc9c
    sp ={'type':'pair','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,}
    smart_setup(sp,verbosity=1)
    with project.open_job(sp) as job:
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_pair_DM_hexadecane_water_solutes():
    # 3afe15ee739fa9efa92263c358e0fc9c
    sp ={'type':'pair','xy_len':6,'solvents':['hexadecane',None],
            'forcefield':'dry martini','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,'solutes':[[['dodecane'],[2]],None]}
    smart_setup(sp,verbosity=1)
    with project.open_job(sp) as job:
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')


def test_setup_pair_dodecane_water_SPC():
    # 8e137097cd93f95dbfa8c686d09c3b57
    sp ={'type':'pair','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'T':300.0,'Pz':1.0,}
    smart_setup(sp,verbosity=1)
    with project.open_job(sp) as job:
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')

def test_setup_pair_dodecane_water_SPC_NACL():
    sp ={'type':'pair','xy_len':6,'solvents':['dodecane','water_SPC'],
            'forcefield':'gromos53a6oxy+D','slab_thicknesses':[10,10],
            'solutes':[None,[['NA','CL'],[2,2]]],
            'T':300.0,'Pz':1.0,}
    smart_setup(sp,verbosity=1)
    with project.open_job(sp) as job:
        assert(os.path.isfile('signac_job_document.json'))
        assert(job.document['stage'] == 'finished')
        assert(len(mda.Universe('pair_eq.gro').select_atoms('name NA')) == 2), 'Expected 2 NA atoms'
        assert(len(mda.Universe('pair_eq.gro').select_atoms('name CL')) == 2), 'Expected 2 CL atoms'
